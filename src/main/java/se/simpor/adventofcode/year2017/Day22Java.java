package se.simpor.adventofcode.year2017;

import java.util.HashMap;
import java.util.Map;

import static se.simpor.adventofcode.year2017.Day22Java.Dir.*;
import static se.simpor.adventofcode.year2017.Day22Java.Status.*;

/**
 *
 *
 */
public class Day22Java {
    class Coord {
        int x;
        int y;

        public Coord(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public String toString() {
            return "Coord{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Coord coord = (Coord) o;

            if (x != coord.x) return false;
            return y == coord.y;
        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            return result;
        }

        public String key() {
            return x + "#" + y;
        }

        public Coord copy() {
            return new Coord(x, y);
        }
    }

    enum Dir {up, right, down, left}

    public long solve1(String problem) {
        String[] split = problem.split("\n");
        Map<Coord, String> map = new HashMap<>();
        int xMin = Integer.MAX_VALUE;
        int xMax = Integer.MIN_VALUE;
        int yMin = Integer.MAX_VALUE;
        int yMax = Integer.MIN_VALUE;
        for (int i = 0; i < split.length; i++) {
            String row = split[i];
            for (int j = 0; j < row.length(); j++) {
                char c = row.charAt(j);
                Coord pos = new Coord(j, i);
                map.put(pos.copy(), "" + c);
                if (pos.x < xMin) xMin = pos.x;
                if (pos.x > xMax) xMax = pos.x;
                if (pos.y < yMin) yMin = pos.y;
                if (pos.y > yMax) yMax = pos.y;
            }
        }
//        printMap(map, xMin, xMax, yMin, yMax);
        Coord pos = new Coord(split.length / 2, split[0].length() / 2);
        Dir dir = up;
        long infections = 0;
        for (int i = 0; i < 10000; i++) {
            if (pos.x < xMin) xMin = pos.x;
            if (pos.x > xMax) xMax = pos.x;
            if (pos.y < yMin) yMin = pos.y;
            if (pos.y > yMax) yMax = pos.y;
            map.putIfAbsent(pos.copy(), ".");
            String s = map.get(pos.copy());
            if (s.equals("#")) {
                dir = getNextDir(dir, 0);
                map.put(pos.copy(), ".");
            }
            if (s.equals(".")) {
                dir = getNextDir(dir, 1);
                map.put(pos.copy(), "#");
                infections++;
            }
            switch (dir) {
                case up:
                    pos.y--;
                    break;
                case right:
                    pos.x++;
                    break;
                case down:
                    pos.y++;
                    break;
                case left:
                    pos.x--;
                    break;
            }
        }

        //       printMap(map, xMin, xMax, yMin, yMax);

        return infections;
    }

    private void printMap(Map<Coord, String> map, int xMin, int xMax, int yMin, int yMax) {
        for (int y = yMin; y <= yMax; y++) {
            for (int x = xMin; x <= xMax; x++) {
                String orDefault = map.getOrDefault(new Coord(x, y), ".");
                System.out.print(orDefault);
            }
            System.out.println();
        }
    }

    private Dir getNextDir(Dir dir, int i) {
        if (i == 0) { // infected, turn right
            if (dir == up) return right;
            if (dir == right) return down;
            if (dir == down) return left;
            if (dir == left) return up;
        }
        if (i == 1) { // clean, turn left
            if (dir == up) return left;
            if (dir == left) return down;
            if (dir == down) return right;
            if (dir == right) return up;
        }
        throw new RuntimeException("Nope");
    }

    enum Status {clean, weakened, infected, flagged}

    /**
     * @param problem
     * @return
     */
    public Long solve2(String problem) {
        String[] split = problem.split("\n");
        Map<Coord, String> map = new HashMap<>();
        int xMin = Integer.MAX_VALUE;
        int xMax = Integer.MIN_VALUE;
        int yMin = Integer.MAX_VALUE;
        int yMax = Integer.MIN_VALUE;
        for (int i = 0; i < split.length; i++) {
            String row = split[i];
            for (int j = 0; j < row.length(); j++) {
                char c = row.charAt(j);
                Coord pos = new Coord(j, i);
                map.put(pos.copy(), "" + c);
                if (pos.x < xMin) xMin = pos.x;
                if (pos.x > xMax) xMax = pos.x;
                if (pos.y < yMin) yMin = pos.y;
                if (pos.y > yMax) yMax = pos.y;
            }
        }
//        printMap(map, xMin, xMax, yMin, yMax);
        Coord pos = new Coord(split.length / 2, split[0].length() / 2);
        Dir dir = up;
        long infections = 0;
        for (int i = 0; i < 10000000; i++) {
            if (pos.x < xMin) xMin = pos.x;
            if (pos.x > xMax) xMax = pos.x;
            if (pos.y < yMin) yMin = pos.y;
            if (pos.y > yMax) yMax = pos.y;
            map.putIfAbsent(pos.copy(), ".");
            String status = map.get(pos.copy());
            if (status.equals(".")) {
                dir = getNextDir(dir, clean);
                map.put(pos.copy(), "W");
            }
            if (status.equals("W")) {
                dir = getNextDir(dir, weakened);
                map.put(pos.copy(), "#");
                infections++;
            }
            if (status.equals("#")) {
                dir = getNextDir(dir, infected);
                map.put(pos.copy(), "F");
            }
            if (status.equals("F")) {
                dir = getNextDir(dir, flagged);
                map.put(pos.copy(), ".");
            }
            switch (dir) {
                case up:
                    pos.y--;
                    break;
                case right:
                    pos.x++;
                    break;
                case down:
                    pos.y++;
                    break;
                case left:
                    pos.x--;
                    break;
            }
        }

        //       printMap(map, xMin, xMax, yMin, yMax);

        return infections;
    }

    private Dir getNextDir(Dir dir, Status status) {
        if (status == infected) { // infected, turn right
            if (dir == up) return right;
            if (dir == right) return down;
            if (dir == down) return left;
            if (dir == left) return up;
        }
        if (status == Status.clean) { // clean, turn left
            if (dir == up) return left;
            if (dir == left) return down;
            if (dir == down) return right;
            if (dir == right) return up;
        }
        if (status == Status.weakened) return dir;
        if (status == Status.flagged) {
            if (dir == up) return down;
            if (dir == left) return right;
            if (dir == down) return up;
            if (dir == right) return left;

        }
        throw new RuntimeException("Nope");
    }

    public static void main(String[] args) {
        Day22Java solver = new Day22Java();

        System.out.println("---- 1 example calc ------");
        Object solve1 = solver.solve1(example);
        System.out.println("---- 1 example solution ------");
        System.out.println(solve1);

        System.out.println("---- 1 real calc ------");
        Object solve1Real = solver.solve1(realProblem);
        System.out.println("---- 1 real solution ------");
        System.out.println(solve1Real);

        System.out.println("------------------------------------");

        System.out.println("---- 2 example calc ------");
        Object solve2 = solver.solve2(example);
        System.out.println("---- 2 example solution ------");
        System.out.println(solve2);

        System.out.println("---- 2 real calc ------");
        Object solve2Real = solver.solve2(realProblem);
        System.out.println("---- 2 real solution ------");
        System.out.println(solve2Real);
    }

    static String example = "..#\n" +
            "#..\n" +
            "...\n";
    static String realProblem = ".#.##..##...#...#.....##.\n" +
            "#.......###..#...#.#.....\n" +
            "##.###..#....#.##.###.##.\n" +
            "##...#.#.##..#.#.###.....\n" +
            ".#....#..#..#..#..#...###\n" +
            "##.####....#...#...###...\n" +
            "#.#########.####...##..##\n" +
            "...###....#.##..##.#...##\n" +
            "##.###....#...#.##.######\n" +
            ".#.##.###.#.#..####..#..#\n" +
            "###.....##..##.##.#.#...#\n" +
            "....#.##.#.#.####.#...#..\n" +
            "....#...#..#...######.##.\n" +
            "##........###.###..#####.\n" +
            "....#.#.#..#######...##..\n" +
            "###.....####..#..##..####\n" +
            "#...##.#....####..##.#...\n" +
            ".##.#.#.....#.#.#..##..##\n" +
            ".#.#.#.##...##.###...####\n" +
            ".#..#.#...#......#...#..#\n" +
            "##.....##...#..####...###\n" +
            "..#####.#..###...#.#.#..#\n" +
            ".####.#....##..##...##..#\n" +
            "#.##..#.##..#.#.##..#...#\n" +
            "##.###.#.##########.#####";
}
