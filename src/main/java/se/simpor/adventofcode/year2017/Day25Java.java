package se.simpor.adventofcode.year2017;

import java.util.HashMap;
import java.util.Map;

import static se.simpor.adventofcode.year2017.Day25Java.State.A;
import static se.simpor.adventofcode.year2017.Day25Java.State.B;
import static se.simpor.adventofcode.year2017.Day25Java.State.C;
import static se.simpor.adventofcode.year2017.Day25Java.State.D;
import static se.simpor.adventofcode.year2017.Day25Java.State.E;
import static se.simpor.adventofcode.year2017.Day25Java.State.F;

/**
 *
 *
 */
public class Day25Java {
    enum State {A, B, C, D, E, F}

    public long solve1(String problem) {
        Map<Long, Long> vec = new HashMap<>();
        Long pos = 0L;
        vec.put(0L, 0L);
        State state = A;
        long right = 1;
        long left = -1;
        for (long i = 0; i < 12134527; i++) {
            Long currentValue = vec.getOrDefault(pos, 0L);
            switch (state) {
                case A:
                    if (currentValue == 0) {
                        vec.put(pos, 1L);
                        pos = pos + right;
                        state = B;
                    } else {
                        vec.put(pos, 0L);
                        pos = pos + left;
                        state = C;
                    }
                    break;
                case B:
                    if (currentValue == 0) {
                        vec.put(pos, 1L);
                        pos = pos + left;
                        state = A;
                    } else {
                        vec.put(pos, 1L);
                        pos = pos + right;
                        state = C;
                    }
                    break;
                case C:
                    if (currentValue == 0) {
                        vec.put(pos, 1L);
                        pos = pos + right;
                        state = A;
                    } else {
                        vec.put(pos, 0L);
                        pos = pos + left;
                        state = D;
                    }
                    break;
                case D:
                    if (currentValue == 0) {
                        vec.put(pos, 1L);
                        pos = pos + left;
                        state = E;
                    } else {
                        vec.put(pos, 1L);
                        pos = pos + left;
                        state = C;
                    }
                    break;
                case E:
                    if (currentValue == 0) {
                        vec.put(pos, 1L);
                        pos = pos + right;
                        state = F;
                    } else {
                        vec.put(pos, 1L);
                        pos = pos + left;
                        state = A;
                    }
                    break;
                case F:
                    if (currentValue == 0) {
                        vec.put(pos, 1L);
                        pos = pos + right;
                        state = A;
                    } else {
                        vec.put(pos, 1L);
                        pos = pos + right;
                        state = E;
                    }
                    break;
            }

        }
        return vec.values().stream().mapToLong(l -> l).sum();
    }


    /**
     * @param problem
     * @return
     */
    public Integer solve2(String problem) {

        return null;
    }

    public static void main(String[] args) {
        Day25Java solver = new Day25Java();

        System.out.println("---- 1 example calc ------");
        Object solve1 = solver.solve1(example);
        System.out.println("---- 1 example solution ------");
        System.out.println(solve1);

        System.out.println("---- 1 real calc ------");
        Object solve1Real = solver.solve1(realProblem);
        System.out.println("---- 1 real solution ------");
        System.out.println(solve1Real);

        System.out.println("------------------------------------");

        System.out.println("---- 2 example calc ------");
        Object solve2 = solver.solve2(example);
        System.out.println("---- 2 example solution ------");
        System.out.println(solve2);

        System.out.println("---- 2 real calc ------");
        Object solve2Real = solver.solve2(realProblem);
        System.out.println("---- 2 real solution ------");
        System.out.println(solve2Real);

    }

    static String example = "";
    static String realProblem = "Begin in state A.\n" +
            "Perform a diagnostic checksum after 12134527 steps.\n" +
            "\n" +
            "In state A:\n" +
            "  If the current value is 0:\n" +
            "    - Write the value 1.\n" +
            "    - Move one slot to the right.\n" +
            "    - Continue with state B.\n" +
            "  If the current value is 1:\n" +
            "    - Write the value 0.\n" +
            "    - Move one slot to the left.\n" +
            "    - Continue with state C.\n" +
            "\n" +
            "In state B:\n" +
            "  If the current value is 0:\n" +
            "    - Write the value 1.\n" +
            "    - Move one slot to the left.\n" +
            "    - Continue with state A.\n" +
            "  If the current value is 1:\n" +
            "    - Write the value 1.\n" +
            "    - Move one slot to the right.\n" +
            "    - Continue with state C.\n" +
            "\n" +
            "In state C:\n" +
            "  If the current value is 0:\n" +
            "    - Write the value 1.\n" +
            "    - Move one slot to the right.\n" +
            "    - Continue with state A.\n" +
            "  If the current value is 1:\n" +
            "    - Write the value 0.\n" +
            "    - Move one slot to the left.\n" +
            "    - Continue with state D.\n" +
            "\n" +
            "In state D:\n" +
            "  If the current value is 0:\n" +
            "    - Write the value 1.\n" +
            "    - Move one slot to the left.\n" +
            "    - Continue with state E.\n" +
            "  If the current value is 1:\n" +
            "    - Write the value 1.\n" +
            "    - Move one slot to the left.\n" +
            "    - Continue with state C.\n" +
            "\n" +
            "In state E:\n" +
            "  If the current value is 0:\n" +
            "    - Write the value 1.\n" +
            "    - Move one slot to the right.\n" +
            "    - Continue with state F.\n" +
            "  If the current value is 1:\n" +
            "    - Write the value 1.\n" +
            "    - Move one slot to the right.\n" +
            "    - Continue with state A.\n" +
            "\n" +
            "In state F:\n" +
            "  If the current value is 0:\n" +
            "    - Write the value 1.\n" +
            "    - Move one slot to the right.\n" +
            "    - Continue with state A.\n" +
            "  If the current value is 1:\n" +
            "    - Write the value 1.\n" +
            "    - Move one slot to the right.\n" +
            "    - Continue with state E.";
}
