package se.simpor.adventofcode.year2017;

import java.util.Arrays;
import java.util.OptionalInt;

/**
 * --- Day 2: Corruption Checksum ---

 As you walk through the door, a glowing humanoid shape yells in your direction. "You there! Your state appears to be idle. Come help us repair the corruption in this spreadsheet - if we take another millisecond, we'll have to display an hourglass cursor!"

 The spreadsheet consists of rows of apparently-random numbers. To make sure the recovery process is on the right track, they need you to calculate the spreadsheet's checksum. For each row, determine the difference between the largest value and the smallest value; the checksum is the sum of all of these differences.

 For example, given the following spreadsheet:

 5 1 9 5
 7 5 3
 2 4 6 8
 The first row's largest and smallest values are 9 and 1, and their difference is 8.
 The second row's largest and smallest values are 7 and 3, and their difference is 4.
 The third row's difference is 6.
 In this example, the spreadsheet's checksum would be 8 + 4 + 6 = 18.

 What is the checksum for the spreadsheet in your puzzle input?

 Your puzzle answer was 36174.

 --- Part Two ---

 "Great work; looks like we're on the right track after all. Here's a star for your effort." However, the program seems a little worried. Can programs be worried?

 "Based on what we're seeing, it looks like all the User wanted is some information about the evenly divisible values in the spreadsheet. Unfortunately, none of us are equipped for that kind of calculation - most of us specialize in bitwise operations."

 It sounds like the goal is to find the only two numbers in each row where one evenly divides the other - that is, where the result of the division operation is a whole number. They would like you to find those numbers on each line, divide them, and add up each line's result.

 For example, given the following spreadsheet:

 5 9 2 8
 9 4 7 3
 3 8 6 5
 In the first row, the only two numbers that evenly divide are 8 and 2; the result of this division is 4.
 In the second row, the two numbers are 9 and 3; the result is 3.
 In the third row, the result is 2.
 In this example, the sum of the results would be 4 + 3 + 2 = 9.

 What is the sum of each row's result in your puzzle input?


 */
public class Day2Java {

    public int solve1(String problem) {
        return Arrays.stream(problem.split("\n"))
                .mapToInt(this::calc)
                .sum();
    }

    public int solve2(String problem) {
        return Arrays.stream(problem.split("\n"))
                .mapToInt(this::calc2)
                .sum();
    }

    private int calc(String subProblem) {
        OptionalInt min = Arrays.stream(subProblem.split("( )|\t"))
                .mapToInt(Integer::parseInt)
                .min();

        OptionalInt max = Arrays.stream(subProblem.split("( )|\t"))
                .mapToInt(Integer::parseInt)
                .max();

        return max.getAsInt() - min.getAsInt();
    }
    private int calc2(String subProblem) {
        String[] split = subProblem.split("( )|\t");

        for (int i = 0; i < split.length; i++) {
            int s1 = Integer.parseInt(split[i]);
            for (int j = 0; j < split.length; j++) {
                int s2 = Integer.parseInt(split[j]);
                if (i != j && s1 > s2 && s1 % s2 == 0){
                    return s1 / s2;
                }
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        String problem = "5 1 9 5\n" +
                "7 5 3\n" +
                "2 4 6 8\n";
        System.out.println(new Day2Java().solve1(problem));

        String problem1 = "3751\t3769\t2769\t2039\t2794\t240\t3579\t1228\t4291\t220\t324\t3960\t211\t1346\t237\t1586\n" +
                "550\t589\t538\t110\t167\t567\t99\t203\t524\t288\t500\t111\t118\t185\t505\t74\n" +
                "2127\t1904\t199\t221\t1201\t250\t1119\t377\t1633\t1801\t2011\t1794\t394\t238\t206\t680\n" +
                "435\t1703\t1385\t1461\t213\t1211\t192\t1553\t1580\t197\t571\t195\t326\t1491\t869\t1282\n" +
                "109\t104\t3033\t120\t652\t2752\t1822\t2518\t1289\t1053\t1397\t951\t3015\t3016\t125\t1782\n" +
                "2025\t1920\t1891\t99\t1057\t1909\t2237\t106\t97\t920\t603\t1841\t2150\t1980\t1970\t88\n" +
                "1870\t170\t167\t176\t306\t1909\t1825\t1709\t168\t1400\t359\t817\t1678\t1718\t1594\t1552\n" +
                "98\t81\t216\t677\t572\t295\t38\t574\t403\t74\t91\t534\t662\t588\t511\t51\n" +
                "453\t1153\t666\t695\t63\t69\t68\t58\t524\t1088\t75\t1117\t1192\t1232\t1046\t443\n" +
                "3893\t441\t1825\t3730\t3660\t115\t4503\t4105\t3495\t4092\t48\t3852\t132\t156\t150\t4229\n" +
                "867\t44\t571\t40\t884\t922\t418\t328\t901\t845\t42\t860\t932\t53\t432\t569\n" +
                "905\t717\t162\t4536\t4219\t179\t990\t374\t4409\t4821\t393\t4181\t4054\t4958\t186\t193\n" +
                "2610\t2936\t218\t2552\t3281\t761\t204\t3433\t3699\t2727\t3065\t3624\t193\t926\t1866\t236\n" +
                "2602\t216\t495\t3733\t183\t4688\t2893\t4042\t3066\t3810\t189\t4392\t3900\t4321\t2814\t159\n" +
                "166\t136\t80\t185\t135\t78\t177\t123\t82\t150\t121\t145\t115\t63\t68\t24\n" +
                "214\t221\t265\t766\t959\t1038\t226\t1188\t1122\t117\t458\t1105\t1285\t1017\t274\t281";

        System.out.println(new Day2Java().solve1(problem1));
        System.out.println(new Day2Java().solve2(problem1));
    }
}
