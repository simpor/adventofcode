package se.simpor.adventofcode.year2017;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * --- Day 13: Packet Scanners ---
 * <p>
 * You need to cross a vast firewall. The firewall consists of several layers,
 * each with a security scanner that moves back and forth across the layer.
 * To succeed, you must not be detected by a scanner.
 * <p>
 * By studying the firewall briefly, you are able to record (in your puzzle input)
 * the depth of each layer and the range of the scanning area for the scanner within it,
 * written as depth: range. Each layer has a thickness of exactly 1.
 * A layer at depth 0 begins immediately inside the firewall; a layer at depth 1 would start immediately after that.
 * <p>
 * For example, suppose you've recorded the following:
 * <p>
 * 0: 3
 * 1: 2
 * 4: 4
 * 6: 4
 * This means that there is a layer immediately inside the firewall (with range 3),
 * a second layer immediately after that (with range 2),
 * a third layer which begins at depth 4 (with range 4),
 * and a fourth layer which begins at depth 6 (also with range 4). Visually,
 * it might look like this:
 * <p>
 * 0   1   2   3   4   5   6
 * [ ] [ ] ... ... [ ] ... [ ]
 * [ ] [ ]         [ ]     [ ]
 * [ ]             [ ]     [ ]
 * [ ]     [ ]
 * Within each layer, a security scanner moves back and forth within its range.
 * Each security scanner starts at the top and moves down until it reaches the bottom,
 * then moves up until it reaches the top, and repeats.
 * A security scanner takes one picosecond to move one step.
 * Drawing scanners as S, the first few picoseconds look like this:
 * <p>
 * <p>
 * Picosecond 0:
 * 0   1   2   3   4   5   6
 * [S] [S] ... ... [S] ... [S]
 * [ ] [ ]         [ ]     [ ]
 * [ ]             [ ]     [ ]
 * [ ]     [ ]
 * <p>
 * Picosecond 1:
 * 0   1   2   3   4   5   6
 * [ ] [ ] ... ... [ ] ... [ ]
 * [S] [S]         [S]     [S]
 * [ ]             [ ]     [ ]
 * [ ]     [ ]
 * <p>
 * Picosecond 2:
 * 0   1   2   3   4   5   6
 * [ ] [S] ... ... [ ] ... [ ]
 * [ ] [ ]         [ ]     [ ]
 * [S]             [S]     [S]
 * [ ]     [ ]
 * <p>
 * Picosecond 3:
 * 0   1   2   3   4   5   6
 * [ ] [ ] ... ... [ ] ... [ ]
 * [S] [S]         [ ]     [ ]
 * [ ]             [ ]     [ ]
 * [S]     [S]
 * Your plan is to hitch a ride on a packet about to move through the firewall.
 * The packet will travel along the top of each layer,
 * and it moves at one layer per picosecond. Each picosecond, t
 * he packet moves one layer forward (its first move takes it into layer 0),
 * and then the scanners move one step.
 * If there is a scanner at the top of the layer as your packet enters it, you are caught.
 * (If a scanner moves into the top of its layer while you are there, you are not caught:
 * it doesn't have time to notice you before you leave.) If you were to do this in the configuration above,
 * marking your current position with parentheses, your passage through the firewall would look like this:
 * <p>
 * Initial state:
 * 0   1   2   3   4   5   6
 * [S] [S] ... ... [S] ... [S]
 * [ ] [ ]         [ ]     [ ]
 * [ ]             [ ]     [ ]
 * [ ]     [ ]
 * <p>
 * Picosecond 0:
 * 0   1   2   3   4   5   6
 * (S) [S] ... ... [S] ... [S]
 * [ ] [ ]         [ ]     [ ]
 * [ ]             [ ]     [ ]
 * [ ]     [ ]
 * <p>
 * 0   1   2   3   4   5   6
 * ( ) [ ] ... ... [ ] ... [ ]
 * [S] [S]         [S]     [S]
 * [ ]             [ ]     [ ]
 * [ ]     [ ]
 * <p>
 * <p>
 * Picosecond 1:
 * 0   1   2   3   4   5   6
 * [ ] ( ) ... ... [ ] ... [ ]
 * [S] [S]         [S]     [S]
 * [ ]             [ ]     [ ]
 * [ ]     [ ]
 * <p>
 * 0   1   2   3   4   5   6
 * [ ] (S) ... ... [ ] ... [ ]
 * [ ] [ ]         [ ]     [ ]
 * [S]             [S]     [S]
 * [ ]     [ ]
 * <p>
 * <p>
 * Picosecond 2:
 * 0   1   2   3   4   5   6
 * [ ] [S] (.) ... [ ] ... [ ]
 * [ ] [ ]         [ ]     [ ]
 * [S]             [S]     [S]
 * [ ]     [ ]
 * <p>
 * 0   1   2   3   4   5   6
 * [ ] [ ] (.) ... [ ] ... [ ]
 * [S] [S]         [ ]     [ ]
 * [ ]             [ ]     [ ]
 * [S]     [S]
 * <p>
 * <p>
 * Picosecond 3:
 * 0   1   2   3   4   5   6
 * [ ] [ ] ... (.) [ ] ... [ ]
 * [S] [S]         [ ]     [ ]
 * [ ]             [ ]     [ ]
 * [S]     [S]
 * <p>
 * 0   1   2   3   4   5   6
 * [S] [S] ... (.) [ ] ... [ ]
 * [ ] [ ]         [ ]     [ ]
 * [ ]             [S]     [S]
 * [ ]     [ ]
 * <p>
 * *
 * Picosecond 4:
 * 0   1   2   3   4   5   6
 * [S] [S] ... ... ( ) ... [ ]
 * [ ] [ ]         [ ]     [ ]
 * [ ]             [S]     [S]
 * [ ]     [ ]
 * <p>
 * 0   1   2   3   4   5   6
 * [ ] [ ] ... ... ( ) ... [ ]
 * [S] [S]         [S]     [S]
 * [ ]             [ ]     [ ]
 * [ ]     [ ]
 * <p>
 * <p>
 * Picosecond 5:
 * 0   1   2   3   4   5   6
 * [ ] [ ] ... ... [ ] (.) [ ]
 * [S] [S]         [S]     [S]
 * [ ]             [ ]     [ ]
 * [ ]     [ ]
 * <p>
 * 0   1   2   3   4   5   6
 * [ ] [S] ... ... [S] (.) [S]
 * [ ] [ ]         [ ]     [ ]
 * [S]             [ ]     [ ]
 * [ ]     [ ]
 * <p>
 * <p>
 * Picosecond 6:
 * 0   1   2   3   4   5   6
 * [ ] [S] ... ... [S] ... (S)
 * [ ] [ ]         [ ]     [ ]
 * [S]             [ ]     [ ]
 * [ ]     [ ]
 * <p>
 * 0   1   2   3   4   5   6
 * [ ] [ ] ... ... [ ] ... ( )
 * [S] [S]         [S]     [S]
 * [ ]             [ ]     [ ]
 * [ ]     [ ]
 * In this situation, you are caught in layers 0 and 6,
 * because your packet entered the layer when its scanner was at the top when you entered it.
 * You are not caught in layer 1, since the scanner moved into the top of the layer once you were already there.
 * <p>
 * The severity of getting caught on a layer is equal to its depth multiplied by its range.
 * (Ignore layers in which you do not get caught.)
 * The severity of the whole trip is the sum of these values.
 * In the example above, the trip severity is 0*3 + 6*4 = 24.
 * <p>
 * Given the details of the firewall you've recorded,
 * if you leave immediately, what is the severity of your whole trip?
 */
public class Day13Java {
    class Firewall {
        int pos;
        int depth;
        int scanner = 0;
        int dir = 1;

        public Firewall(int pos, int depth) {
            this.pos = pos;
            this.depth = depth;
        }

        public void reset() {
            scanner = 0;
            dir = 1;
        }

        public void move() {
            scanner += dir;
            if (dir == 1 && scanner == depth) {
                dir = -1;
                scanner = depth - 2;
            } else if (dir == -1 && scanner == -1) {
                dir = 1;
                scanner = 1;
            }
        }

        public boolean present(int pos) {
            return this.pos == pos && scanner == 0;
        }

        public int penalty(int pos) {
            if (present(pos)) {
                return depth * pos;
            }
            return 0;
        }

        public Firewall copy(){
            Firewall firewall = new Firewall(this.pos, this.depth);
            firewall.dir = this.dir;
            firewall.scanner = this.scanner;
            return firewall;
        }

        @Override
        public String toString() {
            return "Firewall{" +
                    "pos=" + pos +
                    ", depth=" + depth +
                    ", scanner=" + scanner +
                    ", dir=" + dir +
                    '}';
        }
    }

    public Integer solve1(String problem) {
        String[] rows = problem.split("\n");

        List<Firewall> firewalls = Stream.of(rows)
                .map(r -> r.split(": "))
                .map(s -> new Firewall(Integer.parseInt(s[0]), Integer.parseInt(s[1])))
                .collect(Collectors.toList());

        int max = firewalls.stream().mapToInt(f -> f.pos).max().getAsInt();

        int time = 0;
        int movePos = 0;
        int severity = 0;
        for (int i = 0; i <= max; i++) {
            final int currentPos = i;
            int sum = firewalls.stream().mapToInt(f -> f.penalty(currentPos)).sum();
            System.out.println(String.format("%s %s -> %s", i, sum, firewalls.stream().map(f -> "" + f.pos + ":" + f.scanner).collect(Collectors.joining(" "))));
            severity += sum;
            firewalls.forEach(Firewall::move);

        }

        return severity;
    }


    /**
     * --- Part Two ---
     * <p>
     * Now, you need to pass through the firewall without being caught - easier said than done.
     * <p>
     * You can't control the speed of the packet, but you can delay it any number of picoseconds.
     * For each picosecond you delay the packet before beginning your trip,
     * all security scanners move one step.
     * You're not in the firewall during this time; you don't enter layer 0 until you stop delaying the packet.
     * <p>
     * In the example above, if you delay 10 picoseconds (picoseconds 0 - 9), you won't get caught:
     * <p>
     * State after delaying:
     * 0   1   2   3   4   5   6
     * [ ] [S] ... ... [ ] ... [ ]
     * [ ] [ ]         [ ]     [ ]
     * [S]             [S]     [S]
     * [ ]     [ ]
     * <p>
     * Picosecond 10:
     * 0   1   2   3   4   5   6
     * ( ) [S] ... ... [ ] ... [ ]
     * [ ] [ ]         [ ]     [ ]
     * [S]             [S]     [S]
     * [ ]     [ ]
     * <p>
     * 0   1   2   3   4   5   6
     * ( ) [ ] ... ... [ ] ... [ ]
     * [S] [S]         [S]     [S]
     * [ ]             [ ]     [ ]
     * [ ]     [ ]
     * <p>
     * <p>
     * Picosecond 11:
     * 0   1   2   3   4   5   6
     * [ ] ( ) ... ... [ ] ... [ ]
     * [S] [S]         [S]     [S]
     * [ ]             [ ]     [ ]
     * [ ]     [ ]
     * <p>
     * 0   1   2   3   4   5   6
     * [S] (S) ... ... [S] ... [S]
     * [ ] [ ]         [ ]     [ ]
     * [ ]             [ ]     [ ]
     * [ ]     [ ]
     * <p>
     * <p>
     * Picosecond 12:
     * 0   1   2   3   4   5   6
     * [S] [S] (.) ... [S] ... [S]
     * [ ] [ ]         [ ]     [ ]
     * [ ]             [ ]     [ ]
     * [ ]     [ ]
     * <p>
     * 0   1   2   3   4   5   6
     * [ ] [ ] (.) ... [ ] ... [ ]
     * [S] [S]         [S]     [S]
     * [ ]             [ ]     [ ]
     * [ ]     [ ]
     * <p>
     * <p>
     * Picosecond 13:
     * 0   1   2   3   4   5   6
     * [ ] [ ] ... (.) [ ] ... [ ]
     * [S] [S]         [S]     [S]
     * [ ]             [ ]     [ ]
     * [ ]     [ ]
     * <p>
     * 0   1   2   3   4   5   6
     * [ ] [S] ... (.) [ ] ... [ ]
     * [ ] [ ]         [ ]     [ ]
     * [S]             [S]     [S]
     * [ ]     [ ]
     * <p>
     * <p>
     * Picosecond 14:
     * 0   1   2   3   4   5   6
     * [ ] [S] ... ... ( ) ... [ ]
     * [ ] [ ]         [ ]     [ ]
     * [S]             [S]     [S]
     * [ ]     [ ]
     * <p>
     * 0   1   2   3   4   5   6
     * [ ] [ ] ... ... ( ) ... [ ]
     * [S] [S]         [ ]     [ ]
     * [ ]             [ ]     [ ]
     * [S]     [S]
     * <p>
     * <p>
     * Picosecond 15:
     * 0   1   2   3   4   5   6
     * [ ] [ ] ... ... [ ] (.) [ ]
     * [S] [S]         [ ]     [ ]
     * [ ]             [ ]     [ ]
     * [S]     [S]
     * <p>
     * 0   1   2   3   4   5   6
     * [S] [S] ... ... [ ] (.) [ ]
     * [ ] [ ]         [ ]     [ ]
     * [ ]             [S]     [S]
     * [ ]     [ ]
     * <p>
     * <p>
     * Picosecond 16:
     * 0   1   2   3   4   5   6
     * [S] [S] ... ... [ ] ... ( )
     * [ ] [ ]         [ ]     [ ]
     * [ ]             [S]     [S]
     * [ ]     [ ]
     * <p>
     * 0   1   2   3   4   5   6
     * [ ] [ ] ... ... [ ] ... ( )
     * [S] [S]         [S]     [S]
     * [ ]             [ ]     [ ]
     * [ ]     [ ]
     * Because all smaller delays would get you caught, the fewest number of picoseconds you would need to delay to get through safely is 10.
     * <p>
     * What is the fewest number of picoseconds that you need to delay the packet to pass through the firewall without being caught?
     *
     * @param problem
     * @return
     */
    public Integer solve2(String problem) {
        String[] rows = problem.split("\n");

        List<Firewall> firewalls = Stream.of(rows)
                .map(r -> r.split(": "))
                .map(s -> new Firewall(Integer.parseInt(s[0]), Integer.parseInt(s[1])))
                .collect(Collectors.toList());

        int max = firewalls.stream().mapToInt(f -> f.pos).max().getAsInt();

        int delayed = 0;
        int movePos = 0;
        int severity = 1;
        while (severity != 0) {
            severity = 0;
            if (delayed % 10000 == 0) {
                System.out.println("---------------" + delayed);
            }
            List<Firewall> workingList = firewalls.stream().map(Firewall::copy).collect(Collectors.toList());
            Map<Integer, Firewall> firewallMap = workingList.stream().collect(Collectors.toMap(f -> f.pos, f -> f));
            for (int i = 0; i <= max; i++) {
                Firewall firewall = firewallMap.get(i);
                if (firewall != null && firewall.present(i)) {
                    //System.out.println("Caught: " + firewall);
                    severity = 1;
                    break;
                }
                // System.out.println(String.format("%s %s -> %s", i, hit.isPresent(), firewalls.stream().map(f -> "" + f.pos + ":" + f.scanner).collect(Collectors.joining(" "))));
                workingList.forEach(Firewall::move);
            }

            firewalls.forEach(Firewall::move);
            delayed++;
        }

        return delayed - 1;
    }

    public static void main(String[] args) {
        Day13Java solver = new Day13Java();
        System.out.println("---- 1 example calc ------");
        Object solve1 = solver.solve1(example);
        System.out.println("---- 1 real calc ------");
        Object solve1Real = solver.solve1(realProblem);
        System.out.println("---- 1 example solution ------");
        System.out.println(solve1);
        System.out.println("---- 1 real solution ------");
        System.out.println(solve1Real);
        System.out.println("------------------------------------");
        System.out.println("---- 2 example calc ------");
        Object solve2 = solver.solve2(example);
        System.out.println("---- 2 real calc ------");
        Object solve2Real = solver.solve2(realProblem);
        System.out.println("---- 2 example solution ------");
        System.out.println(solve2);
        System.out.println("---- 2 real solution ------");
        System.out.println(solve2Real);
    }

    static String example = "0: 3\n" +
            "1: 2\n" +
            "4: 4\n" +
            "6: 4\n";
    static String realProblem = "0: 4\n" +
            "1: 2\n" +
            "2: 3\n" +
            "4: 5\n" +
            "6: 6\n" +
            "8: 6\n" +
            "10: 4\n" +
            "12: 8\n" +
            "14: 8\n" +
            "16: 9\n" +
            "18: 8\n" +
            "20: 6\n" +
            "22: 6\n" +
            "24: 8\n" +
            "26: 12\n" +
            "28: 12\n" +
            "30: 12\n" +
            "32: 10\n" +
            "34: 8\n" +
            "36: 8\n" +
            "38: 10\n" +
            "40: 12\n" +
            "42: 12\n" +
            "44: 12\n" +
            "46: 14\n" +
            "48: 14\n" +
            "50: 14\n" +
            "52: 14\n" +
            "54: 12\n" +
            "56: 12\n" +
            "58: 12\n" +
            "60: 12\n" +
            "62: 14\n" +
            "64: 14\n" +
            "66: 14\n" +
            "68: 14\n" +
            "70: 14\n" +
            "80: 14\n" +
            "82: 14\n" +
            "86: 14\n" +
            "88: 17\n" +
            "94: 30\n" +
            "98: 18";
}
