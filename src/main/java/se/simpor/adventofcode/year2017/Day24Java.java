package se.simpor.adventofcode.year2017;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 *
 */
public class Day24Java {
    class Component {
        int a;
        int b;
        int index = 0;

        public Component(int a, int b) {
            this.a = a;
            this.b = b;
        }

        @Override
        public String toString() {
            return "Component{" +
                    "a=" + a +
                    ", b=" + b +
                    ", index=" + index +
                    '}';
        }
    }

    public Integer solve1(String problem) {
        String[] rows = problem.split("\n");
        List<Component> components = new ArrayList<>();
        for (int i = 0; i < rows.length; i++) {
            String row = rows[i];
            String[] connectors = row.split("/");
            Component component = new Component(
                    Integer.parseInt(connectors[0]),
                    Integer.parseInt(connectors[1])
            );
            component.index = i;
            components.add(component);
        }

        List<List<Component>> resultList = new ArrayList<>();
        List<Component> start = components.stream().filter(c -> c.a == 0 || c.b == 0).collect(Collectors.toList());

        for (int i = 0; i < start.size(); i++) {
            Component component = start.get(i);
            List<Component> workList = new ArrayList<>();
            workList.add(component);
            List<Component> left = new ArrayList<>();
            left.addAll(components);
            left.removeAll(start);
            recursive(workList, left, resultList);
        }

        int max = Integer.MIN_VALUE;
        List<Component> res = new ArrayList<>();
        for (List<Component> componentList : resultList) {
            int c = 0;
            for (Component component : componentList) {
                c += component.a + component.b;
            }
            if (c > max) {
                max = c;
                res = componentList;
            }
        }
        System.out.println(res);
        return max;
    }

    private void recursive(List<Component> workList, List<Component> left, List<List<Component>> resultList) {
        boolean done = true;
        Component latestComponent = workList.get(workList.size() - 1);
        Component prev = workList.size() > 1 ? workList.get(workList.size() - 2) : null;

        boolean connectedA = false;
        boolean connectedB = false;
        if (latestComponent.a == 0) connectedA = true;
        else if (latestComponent.b == 0) connectedB = true;
        else if (latestComponent.a == prev.a || latestComponent.a == prev.b) connectedA = true;
        else if (latestComponent.b == prev.a || latestComponent.b == prev.b) connectedB = true;

        for (Component component : left) {
            if (connectedB && (latestComponent.a == component.b || latestComponent.a == component.a)) {
                List<Component> newLeft = new ArrayList<>();
                newLeft.addAll(left);
                newLeft.remove(component);
                List<Component> newWorkList = new ArrayList<>();
                newWorkList.addAll(workList);
                newWorkList.add(component);
                recursive(newWorkList, newLeft, resultList);
                done = false;
            } else if (connectedA && (latestComponent.b == component.b || latestComponent.b == component.a)) {
                List<Component> newLeft = new ArrayList<>();
                newLeft.addAll(left);
                newLeft.remove(component);
                List<Component> newWorkList = new ArrayList<>();
                newWorkList.addAll(workList);
                newWorkList.add(component);
                recursive(newWorkList, newLeft, resultList);
                done = false;
            }
        }
        if (done) {
            resultList.add(workList);
        }
    }


    /**
     * @param problem
     * @return
     */
    public Integer solve2(String problem) {

        String[] rows = problem.split("\n");
        List<Component> components = new ArrayList<>();
        for (int i = 0; i < rows.length; i++) {
            String row = rows[i];
            String[] connectors = row.split("/");
            Component component = new Component(
                    Integer.parseInt(connectors[0]),
                    Integer.parseInt(connectors[1])
            );
            component.index = i;
            components.add(component);
        }

        List<List<Component>> resultList = new ArrayList<>();
        List<Component> start = components.stream().filter(c -> c.a == 0 || c.b == 0).collect(Collectors.toList());

        for (int i = 0; i < start.size(); i++) {
            Component component = start.get(i);
            List<Component> workList = new ArrayList<>();
            workList.add(component);
            List<Component> left = new ArrayList<>();
            left.addAll(components);
            left.removeAll(start);
            recursive(workList, left, resultList);
        }

        int max = Integer.MIN_VALUE;
        int longest = Integer.MIN_VALUE;
        List<Component> res = new ArrayList<>();
        for (List<Component> componentList : resultList) {
            int c = 0;
            for (Component component : componentList) {
                c += component.a + component.b;
            }
            if (longest <= componentList.size()) {
                longest = componentList.size();
                if (c > max) {
                    max = c;
                    res = componentList;
                }
            }
        }
        System.out.println(res);
        return max;
    }

    public static void main(String[] args) {
        Day24Java solver = new Day24Java();

        System.out.println("---- 1 example calc ------");
        Object solve1 = solver.solve1(example);
        System.out.println("---- 1 example solution ------");
        System.out.println(solve1);

        System.out.println("---- 1 real calc ------");
        Object solve1Real = solver.solve1(realProblem);
        System.out.println("---- 1 real solution ------");
        System.out.println(solve1Real);

        System.out.println("------------------------------------");

        System.out.println("---- 2 example calc ------");
        Object solve2 = solver.solve2(example);
        System.out.println("---- 2 example solution ------");
        System.out.println(solve2);

        System.out.println("---- 2 real calc ------");
        Object solve2Real = solver.solve2(realProblem);
        System.out.println("---- 2 real solution ------");
        System.out.println(solve2Real);

    }

    static String example = "0/2\n" +
            "2/2\n" +
            "2/3\n" +
            "3/4\n" +
            "3/5\n" +
            "0/1\n" +
            "10/1\n" +
            "9/10";
    static String realProblem = "48/5\n" +
            "25/10\n" +
            "35/49\n" +
            "34/41\n" +
            "35/35\n" +
            "47/35\n" +
            "34/46\n" +
            "47/23\n" +
            "28/8\n" +
            "27/21\n" +
            "40/11\n" +
            "22/50\n" +
            "48/42\n" +
            "38/17\n" +
            "50/33\n" +
            "13/13\n" +
            "22/33\n" +
            "17/29\n" +
            "50/0\n" +
            "20/47\n" +
            "28/0\n" +
            "42/4\n" +
            "46/22\n" +
            "19/35\n" +
            "17/22\n" +
            "33/37\n" +
            "47/7\n" +
            "35/20\n" +
            "8/36\n" +
            "24/34\n" +
            "6/7\n" +
            "7/43\n" +
            "45/37\n" +
            "21/31\n" +
            "37/26\n" +
            "16/5\n" +
            "11/14\n" +
            "7/23\n" +
            "2/23\n" +
            "3/25\n" +
            "20/20\n" +
            "18/20\n" +
            "19/34\n" +
            "25/46\n" +
            "41/24\n" +
            "0/33\n" +
            "3/7\n" +
            "49/38\n" +
            "47/22\n" +
            "44/15\n" +
            "24/21\n" +
            "10/35\n" +
            "6/21\n" +
            "14/50";
}
