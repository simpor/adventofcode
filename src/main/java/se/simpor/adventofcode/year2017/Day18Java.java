package se.simpor.adventofcode.year2017;

import java.util.*;

/**
 * --- Day 18: Duet ---
 * You discover a tablet containing some strange assembly code labeled simply "Duet". Rather than bother the sound card with it, you decide to run the code yourself. Unfortunately, you don't see any documentation, so you're left to figure out what the instructions mean on your own.
 * <p>
 * It seems like the assembly is meant to operate on a set of registers that are each named with a single letter and that can each hold a single integer. You suppose each register should start with a value of 0.
 * <p>
 * There aren't that many instructions, so it shouldn't be hard to figure out what they do. Here's what you determine:
 * <p>
 * snd X plays a sound with a frequency equal to the value of X.
 * set X Y sets register X to the value of Y.
 * add X Y increases register X by the value of Y.
 * mul X Y sets register X to the result of multiplying the value contained in register X by the value of Y.
 * mod X Y sets register X to the remainder of dividing the value contained in register X by the value of Y (that is, it sets X to the result of X modulo Y).
 * rcv X recovers the frequency of the last sound played, but only when the value of X is not zero. (If it is zero, the command does nothing.)
 * jgz X Y jumps with an offset of the value of Y, but only if the value of X is greater than zero. (An offset of 2 skips the next instruction, an offset of -1 jumps to the previous instruction, and so on.)
 * Many of the instructions can take either a register (a single letter) or a number. The value of a register is the integer it contains; the value of a number is that number.
 * <p>
 * After each jump instruction, the program continues with the instruction to which the jump jumped. After any other instruction, the program continues with the next instruction. Continuing (or jumping) off either end of the program terminates it.
 * <p>
 * For example:
 * <p>
 * set a 1
 * add a 2
 * mul a a
 * mod a 5
 * snd a
 * set a 0
 * rcv a
 * jgz a -1
 * set a 1
 * jgz a -2
 * The first four instructions set a to 1, add 2 to it, square it, and then set it to itself modulo 5, resulting in a value of 4.
 * Then, a sound with frequency 4 (the value of a) is played.
 * After that, a is set to 0, causing the subsequent rcv and jgz instructions to both be skipped (rcv because a is 0, and jgz because a is not greater than 0).
 * Finally, a is set to 1, causing the next jgz instruction to activate, jumping back two instructions to another jump, which jumps again to the rcv, which ultimately triggers the recover operation.
 * At the time the recover operation is executed, the frequency of the last sound played is 4.
 * <p>
 * What is the value of the recovered frequency (the value of the most recently played sound) the first time a rcv instruction is executed with a non-zero value?
 */
public class Day18Java {

    public Long solve1(String problem) {

        String[] rows = problem.split("\n");
        long latestSound = -1;
        long latestRec = -1;
        Map<String, Long> registers = new HashMap<>();
        registers.put("a", 0L);
        registers.put("b", 0L);
        registers.put("i", 0L);
        registers.put("p", 0L);
        registers.put("f", 0L);
        Map<String, Long> sounds = new HashMap<>();
        Set<String> comSet = new HashSet<>();
        for (int i = 0; i < rows.length; i++) {
            String[] commands = rows[i].split(" ");
            comSet.add(rows[i]);
            String register = commands[1];
            Long registerValue = 0L;
            try {
                registerValue = Long.parseLong(register);
                int a = 0;
            } catch (NumberFormatException e) {
                registerValue = registers.get(register);
            }

            long newValue = 0;
            if (commands.length == 3) {
                String secondParam = commands[2];
                if (registers.containsKey(secondParam)) {
                    newValue = registers.get(secondParam);
                } else {
                    try {
                        newValue = Long.parseLong(secondParam);
                    } catch (NumberFormatException e) {
                        newValue = 0;
                    }
                }
            }

            switch (commands[0]) {
                case "set":
                    registers.put(register, newValue);
                    break;
                case "add":
                    registers.put(register, registerValue + newValue);
                    break;
                case "mul":
                    registers.put(register, registerValue * newValue);
                    break;
                case "snd":
                    sounds.put(register, registerValue);
                    latestSound = registerValue;
                    break;
                case "mod":
                    registers.put(register, registerValue % newValue);
                    break;
                case "rcv":
                    if (registerValue != 0 && latestSound > 0) {
                        latestRec = latestSound;
                        i = rows.length + 1;
                    }
                    break;
                case "jgz":
                    if (newValue == 11) {
                        int a = 0;
                    }
                    if (registerValue > 0) {
                        i--;
                        i += newValue;
                    }
                    ;
                    break;
            }

            if (i < 0) {
                break;
            }

        }

        return latestRec;
    }

    class Player {
        long soundTimes = 0;
        long receivers = 0;
        Map<String, Long> registers = new HashMap<>();
        List<Long> queue = new ArrayList<>();
        Player other;
        int i=0;
    }

    /**
     * @param problem
     * @return
     */
    public Long solve2(String problem) {
        String[] rows = problem.split("\n");
        final Player p1 = new Player();
        final Player p2 = new Player();
        Player activePlayer = p1;
        p2.registers.put("p", 1L);
        p1.other = p2;
        p2.other = p1;
        for (int i = 0; i < rows.length; i++) {
            String[] commands = rows[i].split(" ");
            String register = commands[1];
            Long registerValue = 0L;
            try {
                registerValue = Long.parseLong(register);
                int a = 0;
            } catch (NumberFormatException e) {
                registerValue = activePlayer.registers.getOrDefault(register, 0L);
            }
            activePlayer.i = i;
            long newValue = 0;
            if (commands.length == 3) {
                String secondParam = commands[2];
                if (activePlayer.registers.containsKey(secondParam)) {
                    newValue = activePlayer.registers.get(secondParam);
                } else {
                    try {
                        newValue = Long.parseLong(secondParam);
                    } catch (NumberFormatException e) {
                        newValue = 0;
                    }
                }
            }

            switch (commands[0]) {
                case "set":
                    activePlayer.registers.put(register, newValue);
                    break;
                case "add":
                    activePlayer.registers.put(register, registerValue + newValue);
                    break;
                case "mul":
                    activePlayer.registers.put(register, registerValue * newValue);
                    break;
                case "mod":
                    activePlayer.registers.put(register, registerValue % newValue);
                    break;

                case "snd":
                    activePlayer.soundTimes++;
                    activePlayer.queue.add(registerValue);
                    break;
                case "rcv":
                    if (activePlayer.other.queue.size() == 0 && activePlayer.queue.size() == 0) {
                        i = -100;
                    } else if (activePlayer.other.queue.size() == 0) {
                        activePlayer = activePlayer.other;
                        i = activePlayer.i -1;
                        continue;
                    } else {
                        activePlayer.registers.put(register, activePlayer.other.queue.remove(0));
                        activePlayer.receivers++;
                    }
                    break;
                case "jgz":
                    if (registerValue > 0) {
                        i--;
                        i += newValue;
                    }
                    break;
            }

            if (i < 0) {
                break;
            }

        }

        return p2.soundTimes;
    }

    public static void main(String[] args) {
        Day18Java solver = new Day18Java();
        System.out.println("---- 1 example calc ------");
        Object solve1 = solver.solve1(example);
        System.out.println("---- 1 real calc ------");
        Object solve1Real = solver.solve1(realProblem);
        System.out.println("---- 1 example solution ------");
        System.out.println(solve1);
        System.out.println("---- 1 real solution ------");
        System.out.println(solve1Real);
        System.out.println("------------------------------------");
        System.out.println("---- 2 example calc ------");
        Object solve2 = solver.solve2("snd 1\n" +
                "snd 2\n" +
                "snd p\n" +
                "rcv a\n" +
                "rcv b\n" +
                "rcv c\n" +
                "rcv d");
        System.out.println("---- 2 example solution ------");
        System.out.println(solve2);
        System.out.println("---- 2 real calc ------");
        Object solve2Real = solver.solve2(realProblem);
        System.out.println("---- 2 real solution ------");
        System.out.println(solve2Real);
    }

    static String example = "set a 1\n" +
            "add a 2\n" +
            "mul a a\n" +
            "mod a 5\n" +
            "snd a\n" +
            "set a 0\n" +
            "rcv a\n" +
            "jgz a -1\n" +
            "set a 1\n" +
            "jgz a -2";
    static String realProblem = "set i 31\n" +
            "set a 1\n" +
            "mul p 17\n" +
            "jgz p p\n" +
            "mul a 2\n" +
            "add i -1\n" +
            "jgz i -2\n" +
            "add a -1\n" +
            "set i 127\n" +
            "set p 952\n" +
            "mul p 8505\n" +
            "mod p a\n" +
            "mul p 129749\n" +
            "add p 12345\n" +
            "mod p a\n" +
            "set b p\n" +
            "mod b 10000\n" +
            "snd b\n" +
            "add i -1\n" +
            "jgz i -9\n" +
            "jgz a 3\n" +
            "rcv b\n" +
            "jgz b -1\n" +
            "set f 0\n" +
            "set i 126\n" +
            "rcv a\n" +
            "rcv b\n" +
            "set p a\n" +
            "mul p -1\n" +
            "add p b\n" +
            "jgz p 4\n" +
            "snd a\n" +
            "set a b\n" +
            "jgz 1 3\n" +
            "snd b\n" +
            "set f 1\n" +
            "add i -1\n" +
            "jgz i -11\n" +
            "snd a\n" +
            "jgz f -16\n" +
            "jgz a -19";
}
