package se.simpor.adventofcode.year2017;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *
 */
public class Day23Java {

    public Long solve1(String problem) {

        String[] rows = problem.split("\n");
        Map<String, Long> registers = new HashMap<>();
        registers.put("a", 0L);
        registers.put("b", 0L);
        registers.put("c", 0L);
        registers.put("d", 0L);
        registers.put("e", 0L);
        registers.put("f", 0L);
        registers.put("g", 0L);
        registers.put("h", 0L);
        long countMul = 0;
        long loopCount = 0;
        for (int i = 0; i < rows.length; i++) {
            loopCount++;
            String[] commands = rows[i].split(" ");
            String register = commands[1];
            Long registerValue = 0L;
            try {
                registerValue = Long.parseLong(register);
                int a = 0;
            } catch (NumberFormatException e) {
                registerValue = registers.get(register);
            }

            long newValue = 0;
            if (commands.length == 3) {
                String secondParam = commands[2];
                if (registers.containsKey(secondParam)) {
                    newValue = registers.get(secondParam);
                } else {
                    try {
                        newValue = Long.parseLong(secondParam);
                    } catch (NumberFormatException e) {
                        newValue = 0;
                    }
                }
            }

            switch (commands[0]) {
                case "set":
                    registers.put(register, newValue);
                    break;
                case "sub":
                    registers.put(register, registerValue - newValue);
                    break;
                case "mul":
                    registers.put(register, registerValue * newValue);
                    countMul++;
                    break;
                case "jnz":
                    if (newValue == 11) {
                        int a = 0;
                    }
                    if (registerValue != 0) {
                        i--;
                        i += newValue;
                    }
                    ;
                    break;
                default:
                    int a = 0;
                    break;
            }

            if (i < 0) {
                break;
            }

        }
        System.out.println("Done after: " + loopCount);
        return countMul;
    }

    private static long mulCount;

    abstract class Instruction {
        protected String type;
        protected int order;
        protected long x = Long.MAX_VALUE;
        protected long y = Long.MAX_VALUE;
        protected String a;
        protected String b;
        protected Map<String, Long> registers;

        public abstract void doit();

        @Override
        public String toString() {
            return order + " -> " + type + " " + a + " " + b;
//                    + " (" +
//                    (x == Long.MAX_VALUE ? " " : x)
//                    + "," +
//                    (y == Long.MAX_VALUE ? " " : y)
//                    + ")";
        }
    }

    class Set extends Instruction {
        public Set() {
            type = "Set";
        }

        @Override
        public void doit() {
            if (y == Long.MAX_VALUE) {
                registers.put(a, registers.get(b));
            } else {
                registers.put(a, y);
            }
        }
    }

    class Sub extends Instruction {
        public Sub() {
            type = "Sub";
        }

        @Override
        public void doit() {
            if (y == Long.MAX_VALUE) {
                registers.put(a, registers.get(a) - registers.get(b));
            } else {
                registers.put(a, registers.get(a) - y);
            }
        }
    }

    class Mul extends Instruction {
        public Mul() {
            type = "Mul";
        }

        @Override
        public void doit() {
            if (y == Long.MAX_VALUE) {
                registers.put(a, registers.get(a) * registers.get(b));
            } else {
                registers.put(a, registers.get(a) * y);
            }
            mulCount++;
        }
    }

    class Jnz extends Instruction {
        public Jnz() {
            type = "Jnz";
        }

        @Override
        public void doit() {
            long test = x == Long.MAX_VALUE ? registers.get(a) : x;
            if (test != 0) {
                registers.put("loop", registers.get("loop") + y - 1);
            }
        }
    }

    public Long solve2(String problem) {
        mulCount = 0;
        String[] rows = problem.split("\n");
        Map<String, Long> registers = new HashMap<>();
        registers.put("a", 1L);
        registers.put("b", 0L);
        registers.put("c", 0L);
        registers.put("d", 0L);
        registers.put("e", 0L);
        registers.put("f", 0L);
        registers.put("g", 0L);
        registers.put("h", 0L);
        registers.put("loop", 0L);
        List<Instruction> instructions = new ArrayList<>();

        for (int i = 0; i < rows.length; i++) {
            String[] commands = rows[i].split(" ");
            String command = commands[0];
            boolean xNumber = false;
            boolean yNumber = false;
            String x = commands[1];
            String y = commands[2];
            try {
                Long.parseLong(x);
                xNumber = true;
            } catch (NumberFormatException e) {
            }

            try {
                Long.parseLong(y);
                yNumber = true;
            } catch (NumberFormatException e) {
            }

            Instruction next;
            switch (command) {
                case "set":
                    next = new Set();
                    break;
                case "sub":
                    next = new Sub();
                    break;
                case "mul":
                    next = new Mul();
                    break;
                case "jnz":
                    next = new Jnz();
                    break;
                default:
                    throw new RuntimeException("Nope");
            }
            next.registers = registers;
            if (xNumber) next.x = Long.parseLong(x);
            if (yNumber) next.y = Long.parseLong(y);
            next.a = x;
            next.b = y;
            next.order = i;
            instructions.add(next);
        }

        long loopCount = 0;
        for (int i = 0; i < instructions.size(); i++) {
            loopCount++;
            registers.put("loop", (long) i);
            Instruction instruction = instructions.get(i);
            instruction.doit();
//            if (loopCount % 10000 == 0) {

            i = registers.get("loop").intValue();
            String format = String.format("%s \t-> a: %s\tb: %s\tc: %s\td: %s\te: %s\tf: %s\tg: %s\th: %s",
                    instruction.toString()
                    , registers.get("a")
                    , registers.get("b")
                    , registers.get("c")
                    , registers.get("d")
                    , registers.get("e")
                    , registers.get("f")
                    , registers.get("g")
                    , registers.get("h")
            );
            System.out.println(format);
//            }

            if (instruction.order == 18) {
                Long g = registers.get("g");
                Long e = registers.get("e");
                registers.put("g", 0L);
                registers.put("f", 0L);
                registers.put("e", e - g);
            }

//            if (instruction.order == 22) {
//                Long g = registers.get("g");
//                Long e = registers.get("e");
//                registers.put("g", 0L);
//
//            }
//
//            if (instruction.order == 30) {
//                Long g = registers.get("g");
//                Long e = registers.get("e");
//
//            }

            if (i < 0) {
                break;
            }
            if (loopCount == 300) {
                break;
            }
        }

        return registers.get("h");
    }

    public static void main(String[] args) {
        Day23Java solver = new Day23Java();

//        System.out.println("---- 1 example calc ------");
//        Object solve1 = solver.solve1(example);
//        System.out.println("---- 1 example solution ------");
//        System.out.println(solve1);

        System.out.println("---- 1 real calc ------");
        Object solve1Real = solver.solve1(realProblem);
        System.out.println("---- 1 real solution ------");
        System.out.println(solve1Real);

        System.out.println("------------------------------------");

//        System.out.println("---- 2 example calc ------");
//        Object solve2 = solver.solve2(example);
//        System.out.println("---- 2 example solution ------");
//        System.out.println(solve2);

        System.out.println("---- 2 real calc ------");
        Object solve2Real = solver.solve2(realProblem);
        System.out.println("---- 2 real solution ------");
        System.out.println(solve2Real);

    }

    static String example = "";
    static String realProblem = "set b 84\n" +
            "set c b\n" +
            "jnz a 2\n" +
            "jnz 1 5\n" +
            "mul b 100\n" +
            "sub b -100000\n" +
            "set c b\n" +
            "sub c -17000\n" +
            "set f 1\n" +
            "set d 2\n" +
            "set e 2\n" +
            "set g d\n" +
            "mul g e\n" +
            "sub g b\n" +
            "jnz g 2\n" +
            "set f 0\n" +
            "sub e -1\n" +
            "set g e\n" +
            "sub g b\n" +
            "jnz g -8\n" +
            "sub d -1\n" +
            "set g d\n" +
            "sub g b\n" +
            "jnz g -13\n" +
            "jnz f 2\n" +
            "sub h -1\n" +
            "set g b\n" +
            "sub g c\n" +
            "jnz g 2\n" +
            "jnz 1 3\n" +
            "sub b -17\n" +
            "jnz 1 -23";
}
