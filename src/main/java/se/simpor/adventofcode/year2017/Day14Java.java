package se.simpor.adventofcode.year2017;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * --- Day 14: Disk Defragmentation ---
 * <p>
 * Suddenly, a scheduled job activates the system's disk defragmenter. Were the situation different, you might sit and watch it for a while, but today, you just don't have that kind of time. It's soaking up valuable system resources that are needed elsewhere, and so the only option is to help it finish its task as soon as possible.
 * <p>
 * The disk in question consists of a 128x128 grid; each square of the grid is either free or used. On this disk, the state of the grid is tracked by the bits in a sequence of knot hashes.
 * <p>
 * A total of 128 knot hashes are calculated, each corresponding to a single row in the grid; each hash contains 128 bits which correspond to individual grid squares. Each bit of a hash indicates whether that square is free (0) or used (1).
 * <p>
 * The hash inputs are a key string (your puzzle input), a dash, and a number from 0 to 127 corresponding to the row. For example, if your key string were flqrgnkx, then the first row would be given by the bits of the knot hash of flqrgnkx-0, the second row from the bits of the knot hash of flqrgnkx-1, and so on until the last row, flqrgnkx-127.
 * <p>
 * The output of a knot hash is traditionally represented by 32 hexadecimal digits; each of these digits correspond to 4 bits, for a total of 4 * 32 = 128 bits. To convert to bits, turn each hexadecimal digit to its equivalent binary value, high-bit first: 0 becomes 0000, 1 becomes 0001, e becomes 1110, f becomes 1111, and so on; a hash that begins with a0c2017... in hexadecimal would begin with 10100000110000100000000101110000... in binary.
 * <p>
 * Continuing this process, the first 8 rows and columns for key flqrgnkx appear as follows, using # to denote used squares, and . to denote free ones:
 * <p>
 * ##.#.#..-->
 * .#.#.#.#
 * ....#.#.
 * #.#.##.#
 * .##.#...
 * ##..#..#
 * .#...#..
 * ##.#.##.-->
 * |      |
 * V      V
 * In this example, 8108 squares are used across the entire 128x128 grid.
 * <p>
 * Given your actual key string, how many squares are used?
 */
public class Day14Java {

    public Integer solve1(String problem) {
        Day10Java day10Java = new Day10Java();

        List<List<String>> allStrings = new ArrayList<>();
        int countOnes = 0;
        for (int i = 0; i < 128; i++) {
            String hash = day10Java.solve2(problem + "-" + i);
            String binaryString = hash.chars()
                    .mapToObj(c -> Character.toString((char) c))
                    .map(s -> {
                        String string = Integer.toBinaryString(Integer.parseInt(s, 16));
                        while (string.length() < 4) {
                            string = "0" + string;
                        }
                        return string;
                    })
                    .collect(Collectors.joining());
            int sum = binaryString.chars()
                    .mapToObj(c -> Character.toString((char) c))
                    .mapToInt(Integer::parseInt)
                    .sum();
            countOnes += sum;
        }


        return countOnes;
    }


    /**
     * --- Part Two ---
     * <p>
     * Now, all the defragmenter needs to know is the number of regions. A region is a group of used squares that are all adjacent, not including diagonals. Every used square is in exactly one region: lone used squares form their own isolated regions, while several adjacent squares all count as a single region.
     * <p>
     * In the example above, the following nine regions are visible, each marked with a distinct digit:
     * <p>
     * 11.2.3..-->
     * .1.2.3.4
     * ....5.6.
     * 7.8.55.9
     * .88.5...
     * 88..5..8
     * .8...8..
     * 88.8.88.-->
     * |      |
     * V      V
     * Of particular interest is the region marked 8; while it does not appear contiguous in this small view, all of the squares marked 8 are connected when considering the whole 128x128 grid. In total, in this example, 1242 regions are present.
     * <p>
     * How many regions are present given your key string?
     *
     * @param problem
     * @return
     */
    public Integer solve2(String problem) {

        Day10Java day10Java = new Day10Java();

        List<String> allStrings = new ArrayList<>();
        for (int i = 0; i < 128; i++) {
            String hash = day10Java.solve2(problem + "-" + i);
            String binaryString = hash.chars()
                    .mapToObj(c -> Character.toString((char) c))
                    .map(s -> {
                        String string = Integer.toBinaryString(Integer.parseInt(s, 16));
                        while (string.length() < 4) {
                            string = "0" + string;
                        }
                        return string;
                    })
                    .collect(Collectors.joining());
            allStrings.add(binaryString);
        }
        int regions = 0;

        Map<String, String> map = new HashMap<>();
        for (int i = 0; i < allStrings.size(); i++) {
            String allString = allStrings.get(i);
            for (int j = 0; j < allString.length(); j++) {
                char c = allString.charAt(j);
                map.put(i + "-" + j, "" + c);
            }
        }

        for (int x = 0; x < 128; x++) {
            for (int y = 0; y < 128; y++) {
                String s = map.get(x + "-" + y);
                if (s.equals("1")) {
                    regions++;
                    markRegion(x, y, map, regions);
                }
            }
        }

        return regions;
    }

    private void markRegion(int x, int y, Map<String, String> map, int r) {
        String key = x + "-" + y;
        if (map.getOrDefault(key, "").equals("1")) {
            map.put(key, "#" + r);
            markRegion(x + 1, y, map, r);
            markRegion(x - 1, y, map, r);
            markRegion(x, y + 1, map, r);
            markRegion(x, y - 1, map, r);
        }
    }

    public static void main(String[] args) {
        Day14Java solver = new Day14Java();
        System.out.println("---- 1 example calc ------");
        Object solve1 = solver.solve1(example);
        System.out.println("---- 1 real calc ------");
        Object solve1Real = solver.solve1(realProblem);
        System.out.println("---- 1 example solution ------");
        System.out.println(solve1);
        System.out.println("---- 1 real solution ------");
        System.out.println(solve1Real);
        System.out.println("------------------------------------");
        System.out.println("---- 2 example calc ------");
        Object solve2 = solver.solve2(example);
        System.out.println("---- 2 real calc ------");
        Object solve2Real = solver.solve2(realProblem);
        System.out.println("---- 2 example solution ------");
        System.out.println(solve2);
        System.out.println("---- 2 real solution ------");
        System.out.println(solve2Real);
    }

    static String example = "flqrgnkx";
    static String realProblem = "uugsqrei";
}
