package se.simpor.adventofcode.year2017;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * --- Day 17: Spinlock ---
 * <p>
 * Suddenly, whirling in the distance, you notice what looks like a massive, pixelated hurricane: a deadly spinlock. This spinlock isn't just consuming computing power, but memory, too; vast, digital mountains are being ripped from the ground and consumed by the vortex.
 * <p>
 * If you don't move quickly, fixing that printer will be the least of your problems.
 * <p>
 * This spinlock's algorithm is simple but efficient, quickly consuming everything in its path. It starts with a circular buffer containing only the value 0, which it marks as the current position. It then steps forward through the circular buffer some number of steps (your puzzle input) before inserting the first new value, 1, after the value it stopped on. The inserted value becomes the current position. Then, it steps forward from there the same number of steps, and wherever it stops, inserts after it the second new value, 2, and uses that as the new current position again.
 * <p>
 * It repeats this process of stepping forward, inserting a new value, and using the location of the inserted value as the new current position a total of 2017 times, inserting 2017 as its final operation, and ending with a total of 2018 values (including 0) in the circular buffer.
 * <p>
 * For example, if the spinlock were to step 3 times per insert, the circular buffer would begin to evolve like this (using parentheses to mark the current position after each iteration of the algorithm):
 * <p>
 * (0), the initial state before any insertions.
 * 0 (1): the spinlock steps forward three times (0, 0, 0), and then inserts the first value, 1, after it. 1 becomes the current position.
 * 0 (2) 1: the spinlock steps forward three times (0, 1, 0), and then inserts the second value, 2, after it. 2 becomes the current position.
 * 0  2 (3) 1: the spinlock steps forward three times (1, 0, 2), and then inserts the third value, 3, after it. 3 becomes the current position.
 * And so on:
 * <p>
 * 0  2 (4) 3  1
 * 0 (5) 2  4  3  1
 * 0  5  2  4  3 (6) 1
 * 0  5 (7) 2  4  3  6  1
 * 0  5  7  2  4  3 (8) 6  1
 * 0 (9) 5  7  2  4  3  8  6  1
 * Eventually, after 2017 insertions, the section of the circular buffer near the last insertion looks like this:
 * <p>
 * 1512  1134  151 (2017) 638  1513  851
 * Perhaps, if you can identify the value that will ultimately be after the last value written (2017), you can short-circuit the spinlock. In this example, that would be 638.
 * <p>
 * What is the value after 2017 in your completed circular buffer?
 * <p>
 * Your puzzle input is 328.
 */
public class Day17Java {

    public Integer solve1(String problem) {
        int length = Integer.parseInt(problem);

        int[] arr = new int[2017];
        arr[0]=0;
        arr[1]=1;
        int lastPos = 1;
        for (int i = 2; i < 2018; i++) {
            int newPos = length + lastPos + 1;
            while (newPos > i) newPos = newPos - i;
            arr[newPos] = i;
//            System.out.println(list.stream().map(t -> "" + t).collect(Collectors.joining(", ")));
            lastPos = newPos;
        }


        return arr[lastPos+1];
    }


    /**
     * --- Part Two ---
     * <p>
     * The spinlock does not short-circuit. Instead, it gets more angry. At least, you assume that's what happened; it's spinning significantly faster than it was a moment ago.
     * <p>
     * You have good news and bad news.
     * <p>
     * The good news is that you have improved calculations for how to stop the spinlock. They indicate that you actually need to identify the value after 0 in the current state of the circular buffer.
     * <p>
     * The bad news is that while you were determining this, the spinlock has just finished inserting its fifty millionth value (50000000).
     * <p>
     * What is the value after 0 the moment 50000000 is inserted?
     *
     * @param problem
     * @return
     */
    public Integer solve2(String problem) {
        int length = Integer.parseInt(problem);

        int lastPos = 1;
        int result = 1;
        for (int i = 2; i < 50000000; i++) {
            int newPos = length + lastPos + 1;
            while (newPos > i) newPos = newPos - i;
            if (newPos == 1){
                result = i;
            }
            lastPos = newPos;
        }


        return result;
    }

    public static void main(String[] args) {
        Day17Java solver = new Day17Java();
        System.out.println("---- 1 example calc ------");
        Object solve1 = solver.solve1(example);
        System.out.println("---- 1 real calc ------");
        Object solve1Real = solver.solve1(realProblem);
        System.out.println("---- 1 example solution ------");
        System.out.println(solve1);
        System.out.println("---- 1 real solution ------");
        System.out.println(solve1Real);
        System.out.println("------------------------------------");
        System.out.println("---- 2 example calc ------");
        Object solve2 = solver.solve2(example);
        System.out.println("---- 2 real calc ------");
        Object solve2Real = solver.solve2(realProblem);
        System.out.println("---- 2 example solution ------");
        System.out.println(solve2);
        System.out.println("---- 2 real solution ------");
        System.out.println(solve2Real);
    }

    static String example = "3";
    static String realProblem = "328";
}
