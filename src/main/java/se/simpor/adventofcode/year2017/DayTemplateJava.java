package se.simpor.adventofcode.year2017;

/**
 *
 *
 */
public class DayTemplateJava {

    public String solve1(String problem) {

        return null;
    }


    /**
     * @param problem
     * @return
     */
    public Integer solve2(String problem) {

        return null;
    }

    public static void main(String[] args) {
        DayTemplateJava solver = new DayTemplateJava();

        System.out.println("---- 1 example calc ------");
        Object solve1 = solver.solve1(example);
        System.out.println("---- 1 example solution ------");
        System.out.println(solve1);

        System.out.println("---- 1 real calc ------");
        Object solve1Real = solver.solve1(realProblem);
        System.out.println("---- 1 real solution ------");
        System.out.println(solve1Real);

        System.out.println("------------------------------------");

        System.out.println("---- 2 example calc ------");
        Object solve2 = solver.solve2(example);
        System.out.println("---- 2 example solution ------");
        System.out.println(solve2);

        System.out.println("---- 2 real calc ------");
        Object solve2Real = solver.solve2(realProblem);
        System.out.println("---- 2 real solution ------");
        System.out.println(solve2Real);

    }

    static String example = "";
    static String realProblem = "";
}
