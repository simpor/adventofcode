package se.simpor.adventofcode.year2017;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

import static se.simpor.adventofcode.year2017.Day19Java.Dir.*;

/**
 *
 *
 */
public class Day19Java {

    class Coord {
        int x;
        int y;
        char c;

        @Override
        public String toString() {
            return "Coord{" +
                    "x=" + x +
                    ", y=" + y +
                    ", c=" + c +
                    '}';
        }
    }

    enum Dir {up, right, down, left, plus}

    public String solve1(String problem) {
        String[] rows = problem.split("\n");
        int startPos = -1;
        for (int i = 0; i < rows[0].length(); i++) {
            char c = rows[0].charAt(i);
            if (c == '|') {
                startPos = i;
                break;
            }
        }

        Map<String, Coord> map = new HashMap<>();
        for (int i = 0; i < rows.length; i++) {
            String row = rows[i];
            for (int j = 0; j < row.length(); j++) {
                Coord coord = new Coord();
                coord.x = j;
                coord.y = i;
                coord.c = row.charAt(j);
                map.put(key(coord.x, coord.y), coord);
            }
        }

        // walk the maze...
        Dir dir = down; // 0=up, 1=right, 2=down, 3=left, 4 = plusSign
        int currentX = startPos, currentY = 0;
        String letters = "";
        try {
            while (true) {
                Coord currentCoord = map.get(key(currentX, currentY));
                System.out.println(String.format("At [%s, %s] with dir: %s and char: %s", currentX, currentY, dir, currentCoord.c));
                map.put(key(currentX, currentY), null);
                Coord nextCoord = null;
                List<Coord> surronding1 = getSurronding(map, 1, dir, currentX, currentY);
                List<Coord> surronding2 = getSurronding(map, 2, dir, currentX, currentY);
                if (surronding1.size() == 0 && surronding2.size() == 0) {
                    break;
                }
                if (dir == up || dir == down) {
                    Optional<Coord> first = surronding1.stream().filter(c -> (c.c != '-')).findFirst();
                    if (first.isPresent()) {
                        nextCoord = first.get();
                    } else {
                        Optional<Coord> second = surronding2.stream().filter(c -> (c.c != '-')).findFirst();
                        nextCoord = second.get();
                    }
                    if (nextCoord.c != '|' && nextCoord.c != '+') {
                        letters = letters + nextCoord.c;
                    } else if (nextCoord.c == '+') {
                        dir = plus;
                    }
                } else if (dir == left || dir == right) {
                    Optional<Coord> first = surronding1.stream().filter(c -> (c.c != '|')).findFirst();
                    if (first.isPresent()) {
                        nextCoord = first.get();
                    } else {
                        Optional<Coord> second = surronding2.stream().filter(c -> (c.c != '|')).findFirst();
                        nextCoord = second.get();
                    }
                    if (nextCoord.c != '-' && nextCoord.c != '+') {
                        letters = letters + nextCoord.c;
                    } else if (nextCoord.c == '+') {
                        dir = plus;
                    }
                } else if (dir == plus) {
                    if (surronding1.size() == 0) {
                        int a = 0;
                    }
                    nextCoord = surronding1.get(0);
                    if (nextCoord.x < currentX) dir = left;
                    else if (nextCoord.x > currentX) dir = right;
                    else if (nextCoord.y > currentY) dir = down;
                    else if (nextCoord.y < currentY) dir = up;
                    else {
                        int b = 0;
                    }
                    if (nextCoord.c != '-' && nextCoord.c != '|' && nextCoord.c != '+') {
                        letters = letters + nextCoord.c;
                    }
                }
                currentX = nextCoord.x;
                currentY = nextCoord.y;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(letters);
        }

        return letters;
    }

    // 0=up, 1=right, 2=down, 3=left, 4 = plusSign
    private List<Coord> getSurronding(Map<String, Coord> map, int step, Dir dir, int currentX, int currentY) {
        List<Coord> coords = new ArrayList<>();
        if (dir == up) {
            coords.add(map.get(key(currentX, currentY - step)));
        } else if (dir == right) {
            coords.add(map.get(key(currentX + step, currentY)));
        } else if (dir == down) {
            coords.add(map.get(key(currentX, currentY + step)));
        } else if (dir == left) {
            coords.add(map.get(key(currentX - step, currentY)));
        } else if (dir == plus) {
            coords.add(map.get(key(currentX, currentY - step)));
            coords.add(map.get(key(currentX + step, currentY)));
            coords.add(map.get(key(currentX, currentY + step)));
            coords.add(map.get(key(currentX - step, currentY)));
        }
        return coords.stream().filter(Objects::nonNull).filter(t -> t.c != ' ').collect(Collectors.toList());
    }

    private String key(int x, int y) {
        return x + "-" + y;
    }


    /**
     * @param problem
     * @return
     */
    public Integer solve2(String problem) {
        String[] rows = problem.split("\n");
        int startPos = -1;
        for (int i = 0; i < rows[0].length(); i++) {
            char c = rows[0].charAt(i);
            if (c == '|') {
                startPos = i;
                break;
            }
        }

        Map<String, Coord> map = new HashMap<>();
        for (int i = 0; i < rows.length; i++) {
            String row = rows[i];
            for (int j = 0; j < row.length(); j++) {
                Coord coord = new Coord();
                coord.x = j;
                coord.y = i;
                coord.c = row.charAt(j);
                map.put(key(coord.x, coord.y), coord);
            }
        }

        // walk the maze...
        Dir dir = down; // 0=up, 1=right, 2=down, 3=left, 4 = plusSign
        int currentX = startPos, currentY = 0;
        String letters = "";
        int steps = 0;
        try {
            while (true) {
                steps++;
                Coord currentCoord = map.get(key(currentX, currentY));
 //               System.out.println(String.format("At [%s, %s] with dir: %s and char: %s", currentX, currentY, dir, currentCoord.c));
                map.put(key(currentX, currentY), null);
                Coord nextCoord = null;
                List<Coord> surronding1 = getSurronding(map, 1, dir, currentX, currentY);
                List<Coord> surronding2 = getSurronding(map, 2, dir, currentX, currentY);
                if (surronding1.size() == 0 && surronding2.size() == 0) {
                    break;
                }
                if (dir == up || dir == down) {
                    Optional<Coord> first = surronding1.stream().filter(c -> (c.c != '-')).findFirst();
                    if (first.isPresent()) {
                        nextCoord = first.get();
                    } else {
                        Optional<Coord> second = surronding2.stream().filter(c -> (c.c != '-')).findFirst();
                        nextCoord = second.get();
                        steps++;
                    }
                    if (nextCoord.c != '|' && nextCoord.c != '+') {
                        letters = letters + nextCoord.c;
                    } else if (nextCoord.c == '+') {
                        dir = plus;
                    }
                } else if (dir == left || dir == right) {
                    Optional<Coord> first = surronding1.stream().filter(c -> (c.c != '|')).findFirst();
                    if (first.isPresent()) {
                        nextCoord = first.get();
                    } else {
                        Optional<Coord> second = surronding2.stream().filter(c -> (c.c != '|')).findFirst();
                        nextCoord = second.get();
                        steps++;
                    }
                    if (nextCoord.c != '-' && nextCoord.c != '+') {
                        letters = letters + nextCoord.c;
                    } else if (nextCoord.c == '+') {
                        dir = plus;
                    }
                } else if (dir == plus) {
                    if (surronding1.size() == 0) {
                        int a = 0;
                    }
                    nextCoord = surronding1.get(0);
                    if (nextCoord.x < currentX) dir = left;
                    else if (nextCoord.x > currentX) dir = right;
                    else if (nextCoord.y > currentY) dir = down;
                    else if (nextCoord.y < currentY) dir = up;
                    else {
                        int b = 0;
                    }
                    if (nextCoord.c != '-' && nextCoord.c != '|' && nextCoord.c != '+') {
                        letters = letters + nextCoord.c;
                    }
                }
                currentX = nextCoord.x;
                currentY = nextCoord.y;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(letters);
        }

        return steps;
    }

    public static void main(String[] args) throws URISyntaxException, IOException {
        Day19Java solver = new Day19Java();
        System.out.println("---- 1 example calc ------");
        Object solve1 = solver.solve1(example);
        System.out.println("---- 1 example solution ------");
        System.out.println(solve1);
        System.out.println("---- 1 real calc ------");

        URI uri = solver.getClass().getClassLoader().getResource("day19.txt").toURI();
        Path path = Paths.get(uri);

        realProblem = java.nio.file.Files.lines(path).collect(
                Collectors.joining("\n"));

        Object solve1Real = solver.solve1(realProblem);
        System.out.println("---- 1 real solution ------");
        System.out.println(solve1Real);
        System.out.println("------------------------------------");
        System.out.println("---- 2 example calc ------");
        Object solve2 = solver.solve2(example);
        System.out.println("---- 2 example solution ------");
        System.out.println(solve2);
        System.out.println("---- 2 real calc ------");
        Object solve2Real = solver.solve2(realProblem);
        System.out.println("---- 2 real solution ------");
        System.out.println(solve2Real);
    }

    static String example =
            "     |          \n" +
                    "     |  +--+    \n" +
                    "     A  |  C    \n" +
                    " F---|----E|--+ \n" +
                    "     |  |  |  D \n" +
                    "     +B-+  +--+ \n";
    static String realProblem = "";
}
