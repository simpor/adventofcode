package se.simpor.adventofcode.year2017;

import java.util.*;
import java.util.stream.Collectors;

/**
 *
 *
 */
public class Day21Java {

    static class PixelPattern {

        int size;
        List<String> rows = new ArrayList<>();

        public boolean match(PixelPattern pattern) {
            if (size != pattern.size)
                return false;
            if (equal(pattern))
                return true;
            PixelPattern rotate1 = pattern.rotate();
            if (equal(rotate1))
                return true;
            if (equal(rotate1.flip()))
                return true;
            PixelPattern rotat2 = rotate1.rotate();
            if (equal(rotat2))
                return true;
            if (equal(rotat2.flip()))
                return true;
            PixelPattern rotate3 = rotat2.rotate();
            if (equal(rotate3))
                return true;
            if (equal(rotate3.flip()))
                return true;
            PixelPattern rotate4 = rotate3.rotate();
            if (equal(rotate4))
                return true;
            if (equal(rotate4.flip()))
                return true;

            return false;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            PixelPattern that = (PixelPattern) o;

            if (size != that.size) return false;
            return rows != null ? rows.equals(that.rows) : that.rows == null;
        }

        @Override
        public int hashCode() {
            int result = size;
            result = 31 * result + (rows != null ? rows.hashCode() : 0);
            return result;
        }

        public boolean equal(PixelPattern pattern) {
            boolean equals = true;
            for (int i = 0; i < rows.size(); i++) {
                String row = rows.get(i);
                String s = pattern.rows.get(i);
                if (!row.equals(s))
                    equals = false;
            }
            if (equals)
                return true;
            return false;
        }

        public PixelPattern flip() {
            PixelPattern pixelPattern = new PixelPattern();
            pixelPattern.size = this.size;
            for (int i = 0; i < rows.size(); i++) {
                pixelPattern.rows.add(new StringBuffer(rows.get(i)).reverse().toString());
            }
            return pixelPattern;
        }

        public PixelPattern rotate() {
            PixelPattern pixelPattern = new PixelPattern();
            pixelPattern.size = this.size;
            for (int i = 0; i < size; i++) {
                String row = "";
                for (int j = 0; j < size; j++) {
                    row += rows.get(size - 1 - j).charAt(i);
                }
                pixelPattern.rows.add(row);
            }
            return pixelPattern;
        }

        String string() {
            return rows.stream().collect(Collectors.joining(""));
        }

        /**
         * ../.#
         * ##./#../...
         * .#./..#/###
         * #..#/..../..../#..#
         */
        static PixelPattern parseRule(String pattern) {
            PixelPattern pixelPattern = new PixelPattern();
            String[] split = pattern.split("/");
            pixelPattern.size = split.length;
            pixelPattern.rows.addAll(Arrays.asList(split));
            return pixelPattern;
        }
    }


    public String solve1(String problem) {
        PixelPattern startPattern = new PixelPattern();
        startPattern.size = 3;
        startPattern.rows.add(".#.");
        startPattern.rows.add("..#");
        startPattern.rows.add("###");

        String[] patternRules = problem.split("\n");
        Map<PixelPattern, PixelPattern> ruleMap = new HashMap<>();
        for (int i = 0; i < patternRules.length; i++) {
            String patternRule = patternRules[i];
            String[] rules = patternRule.split(" => ");

            PixelPattern fromPattern = PixelPattern.parseRule(rules[0]);
            PixelPattern toPattern = PixelPattern.parseRule(rules[1]);
            ruleMap.put(fromPattern, toPattern);
        }

        PixelPattern init = patternFromRule(ruleMap, startPattern);

        List<List<PixelPattern>> patterns = new ArrayList<>();
        patterns.add(Arrays.asList(startPattern));

        for (int i = 0; i < 5; i++) {
            log("Map: %s", i);
            List<String> screen = new ArrayList<>();
            for (List<PixelPattern> pattern : patterns) {
                List<PixelPattern> list = new ArrayList<>();
                int size = 0;
                for (PixelPattern pixelPattern : pattern) {
                    PixelPattern e = patternFromRule(ruleMap, pixelPattern);
                    list.add(e);
                    size = e.size;
                }
                for (int x = 0; x < size; x++) {
                    String row = "";
                    for (PixelPattern pixelPattern : list) {
                        row += pixelPattern.rows.get(x);
                    }
                    screen.add(row);
                    log(row);
                }
            }

            // identify split size of screen
            int length = screen.size();
            int splitSize = length % 2 == 0 ? 2 : 3;
            patterns = new ArrayList<>();
            log("Patterns:");
            for (int j = 0; j < length; j += splitSize) {
                List<PixelPattern> newRow = new ArrayList<>();
                patterns.add(newRow);

                for (int k = 0; k < length / splitSize; k++) {
                    PixelPattern newPattern = new PixelPattern();
                    newPattern.size = splitSize;
                    log("----");
                    for (int l = 0; l < splitSize; l++) {
                        int beginIndex = k * splitSize;
                        String substring = screen.get(l+j).substring(beginIndex, splitSize + beginIndex);
                        newPattern.rows.add(substring);
                    }
                    newRow.add(newPattern);
                    newPattern.rows.forEach(r -> log(r));
                }
            }
            log("----");
            log("Loop: %s", i);
            log("");

            long countHashes = 0;
            String collect = patterns.stream()
                    .flatMap(Collection::stream)
                    .flatMap(p -> p.rows.stream())
                    .collect(Collectors.joining());
            log("Number of hashes: " + collect.replace(".", "").length());


        }
        return null;
    }

    private PixelPattern patternFromRule(Map<PixelPattern, PixelPattern> ruleMap, PixelPattern pixelPattern) {
        for (PixelPattern pattern : ruleMap.keySet()) {
            if (pattern.match(pixelPattern)) {
                return ruleMap.get(pattern);
            }
        }
        throw new RuntimeException("Can't find pattern for: " + pixelPattern.rows.stream().collect(Collectors.joining("/")));
    }

    private void log(String message, Object... arg) {
        System.out.println(String.format(message, arg));
    }

    /**
     * @param problem
     * @return
     */
    public Integer solve2(String problem) {
        PixelPattern startPattern = new PixelPattern();
        startPattern.size = 3;
        startPattern.rows.add(".#.");
        startPattern.rows.add("..#");
        startPattern.rows.add("###");

        String[] patternRules = problem.split("\n");
        Map<PixelPattern, PixelPattern> ruleMap = new HashMap<>();
        for (int i = 0; i < patternRules.length; i++) {
            String patternRule = patternRules[i];
            String[] rules = patternRule.split(" => ");

            PixelPattern fromPattern = PixelPattern.parseRule(rules[0]);
            PixelPattern toPattern = PixelPattern.parseRule(rules[1]);
            ruleMap.put(fromPattern, toPattern);
        }

        PixelPattern init = patternFromRule(ruleMap, startPattern);

        List<List<PixelPattern>> patterns = new ArrayList<>();
        patterns.add(Arrays.asList(startPattern));

        for (int i = 0; i < 18; i++) {
            log("Map: %s", i);
            List<String> screen = new ArrayList<>();
            for (List<PixelPattern> pattern : patterns) {
                List<PixelPattern> list = new ArrayList<>();
                int size = 0;
                for (PixelPattern pixelPattern : pattern) {
                    PixelPattern e = patternFromRule(ruleMap, pixelPattern);
                    list.add(e);
                    size = e.size;
                }
                for (int x = 0; x < size; x++) {
                    String row = "";
                    for (PixelPattern pixelPattern : list) {
                        row += pixelPattern.rows.get(x);
                    }
                    screen.add(row);
                    log(row);
                }
            }

            // identify split size of screen
            int length = screen.size();
            int splitSize = length % 2 == 0 ? 2 : 3;
            patterns = new ArrayList<>();
            log("Patterns:");
            for (int j = 0; j < length; j += splitSize) {
                List<PixelPattern> newRow = new ArrayList<>();
                patterns.add(newRow);

                for (int k = 0; k < length / splitSize; k++) {
                    PixelPattern newPattern = new PixelPattern();
                    newPattern.size = splitSize;
                    log("----");
                    for (int l = 0; l < splitSize; l++) {
                        int beginIndex = k * splitSize;
                        String substring = screen.get(l+j).substring(beginIndex, splitSize + beginIndex);
                        newPattern.rows.add(substring);
                    }
                    newRow.add(newPattern);
                    newPattern.rows.forEach(r -> log(r));
                }
            }
            log("----");
            log("Loop: %s", i);
            log("");

            long countHashes = 0;
            String collect = patterns.stream()
                    .flatMap(Collection::stream)
                    .flatMap(p -> p.rows.stream())
                    .collect(Collectors.joining());
            log("Number of hashes: " + collect.replace(".", "").length());


        }
        return null;
    }

    public static void main(String[] args) {
        Day21Java solver = new Day21Java();
        System.out.println("---- 1 example calc ------");
        Object solve1 = solver.solve1(example);
        System.out.println("---- 1 example solution ------");
        System.out.println(solve1);
        System.out.println("---- 1 real calc ------");
        Object solve1Real = solver.solve1(realProblem);
        System.out.println("---- 1 real solution ------");
        System.out.println(solve1Real);
        System.out.println("------------------------------------");
//        System.out.println("---- 2 example calc ------");
//        Object solve2 = solver.solve2(example);
//        System.out.println("---- 2 example solution ------");
//        System.out.println(solve2);
        System.out.println("---- 2 real calc ------");
        Object solve2Real = solver.solve2(realProblem);
        System.out.println("---- 2 real solution ------");
        System.out.println(solve2Real);
    }

    static String example = "../.# => ##./#../...\n" +
            ".#./..#/### => #..#/..../..../#..#\n";
    static String realProblem = "../.. => ###/.##/#..\n" +
            "#./.. => #.#/..#/#..\n" +
            "##/.. => ###/.#./###\n" +
            ".#/#. => ##./###/...\n" +
            "##/#. => ##./###/#.#\n" +
            "##/## => #.#/#.#/###\n" +
            ".../.../... => #.#./#..#/#.##/#.#.\n" +
            "#../.../... => #.##/.##./#..#/.###\n" +
            ".#./.../... => #..#/#.#./.#../#.##\n" +
            "##./.../... => .#../...#/####/...#\n" +
            "#.#/.../... => ##.#/..#./...#/###.\n" +
            "###/.../... => #.#./#..#/####/##..\n" +
            ".#./#../... => ..##/..##/####/##.#\n" +
            "##./#../... => #.#./#.../..../##..\n" +
            "..#/#../... => .#.#/##.#/...#/####\n" +
            "#.#/#../... => .#../.###/.##./##.#\n" +
            ".##/#../... => ##../.#.#/#.../..##\n" +
            "###/#../... => ##.#/##.#/.###/##..\n" +
            ".../.#./... => ..#./..../##.#/#.#.\n" +
            "#../.#./... => ..##/.#.#/..#./###.\n" +
            ".#./.#./... => ...#/.#../.#.#/##..\n" +
            "##./.#./... => #..#/.###/##../#.#.\n" +
            "#.#/.#./... => ##.#/..#./.#../#..#\n" +
            "###/.#./... => #.#./####/#..#/#...\n" +
            ".#./##./... => ##../##.#/.###/##..\n" +
            "##./##./... => .#../####/.##./.#..\n" +
            "..#/##./... => ####/##.#/##.#/###.\n" +
            "#.#/##./... => .##./#.##/##.#/#...\n" +
            ".##/##./... => ..../#.##/##.#/##..\n" +
            "###/##./... => #.../.##./#.#./#...\n" +
            ".../#.#/... => #..#/..##/#.../#.##\n" +
            "#../#.#/... => ..##/..#./..#./..##\n" +
            ".#./#.#/... => ..##/####/####/....\n" +
            "##./#.#/... => ###./.#../##.#/#.#.\n" +
            "#.#/#.#/... => .##./#..#/..#./##..\n" +
            "###/#.#/... => ##.#/..#./#..#/...#\n" +
            ".../###/... => ..##/.#.#/#.../...#\n" +
            "#../###/... => ..##/#.##/#.#./..#.\n" +
            ".#./###/... => ##../..##/.##./...#\n" +
            "##./###/... => #.#./#.../#.../.##.\n" +
            "#.#/###/... => ##.#/..##/..##/.###\n" +
            "###/###/... => ..#./#..#/.#../.##.\n" +
            "..#/.../#.. => ###./#.#./#.##/#.##\n" +
            "#.#/.../#.. => ####/.##./#..#/.###\n" +
            ".##/.../#.. => #.#./..../..../##.#\n" +
            "###/.../#.. => .#.#/..../.#.#/###.\n" +
            ".##/#../#.. => ####/#..#/.##./####\n" +
            "###/#../#.. => ##../.#../..../###.\n" +
            "..#/.#./#.. => .###/##../#.##/...#\n" +
            "#.#/.#./#.. => ...#/####/#.../..#.\n" +
            ".##/.#./#.. => #.../####/.#.#/###.\n" +
            "###/.#./#.. => ####/..../.#../##..\n" +
            ".##/##./#.. => ..../###./##../.###\n" +
            "###/##./#.. => #.../#..#/#..#/###.\n" +
            "#../..#/#.. => ..../.###/.#../.#.#\n" +
            ".#./..#/#.. => .#.#/...#/#.#./##..\n" +
            "##./..#/#.. => .#../##.#/.#../.##.\n" +
            "#.#/..#/#.. => ##../#.##/.###/#.#.\n" +
            ".##/..#/#.. => #.#./..../.#.#/..##\n" +
            "###/..#/#.. => ...#/#.../...#/..#.\n" +
            "#../#.#/#.. => ##.#/..#./###./.###\n" +
            ".#./#.#/#.. => ..../##../.#.#/.###\n" +
            "##./#.#/#.. => ###./#.#./.#../.#.#\n" +
            "..#/#.#/#.. => ###./..../##.#/#..#\n" +
            "#.#/#.#/#.. => .#.#/#.##/#.../..#.\n" +
            ".##/#.#/#.. => .#.#/#.../##../####\n" +
            "###/#.#/#.. => #.##/..#./..##/....\n" +
            "#../.##/#.. => .#../.#../...#/#...\n" +
            ".#./.##/#.. => ##../#..#/###./##.#\n" +
            "##./.##/#.. => .#.#/#..#/..../#..#\n" +
            "#.#/.##/#.. => ##.#/..../##../##..\n" +
            ".##/.##/#.. => #.#./..#./#.../.#..\n" +
            "###/.##/#.. => #.#./##.#/####/....\n" +
            "#../###/#.. => ##../#.##/.#../.###\n" +
            ".#./###/#.. => #.../#.##/..../.#.#\n" +
            "##./###/#.. => ###./##../.#../..##\n" +
            "..#/###/#.. => ..#./.#../####/#..#\n" +
            "#.#/###/#.. => #.##/..#./..#./#.##\n" +
            ".##/###/#.. => .#../#.../####/#...\n" +
            "###/###/#.. => #.../..#./..../.##.\n" +
            ".#./#.#/.#. => .#.#/####/###./....\n" +
            "##./#.#/.#. => ##.#/###./#.##/#..#\n" +
            "#.#/#.#/.#. => ####/#.#./..../##..\n" +
            "###/#.#/.#. => ##.#/##../.###/..##\n" +
            ".#./###/.#. => .##./...#/##.#/.###\n" +
            "##./###/.#. => ..##/.#../..#./#...\n" +
            "#.#/###/.#. => ####/#.../..#./#.#.\n" +
            "###/###/.#. => #.../.###/..##/.#.#\n" +
            "#.#/..#/##. => ...#/###./#.#./#.##\n" +
            "###/..#/##. => ##../..#./###./##..\n" +
            ".##/#.#/##. => #.../#.#./#.#./#..#\n" +
            "###/#.#/##. => #.../##.#/#.#./....\n" +
            "#.#/.##/##. => ...#/#.#./...#/#...\n" +
            "###/.##/##. => .###/...#/#..#/###.\n" +
            ".##/###/##. => ###./.##./##.#/#..#\n" +
            "###/###/##. => #.../##../.###/.#..\n" +
            "#.#/.../#.# => #.../#..#/...#/#..#\n" +
            "###/.../#.# => .#../..##/.##./.#.#\n" +
            "###/#../#.# => ..../.#.#/###./#...\n" +
            "#.#/.#./#.# => ##.#/.#.#/#.##/...#\n" +
            "###/.#./#.# => ..../#.../#.../.###\n" +
            "###/##./#.# => ..##/.##./##.#/##.#\n" +
            "#.#/#.#/#.# => ..##/.#../..##/..#.\n" +
            "###/#.#/#.# => ..##/.###/...#/##..\n" +
            "#.#/###/#.# => ..#./.#.#/.###/####\n" +
            "###/###/#.# => #.#./##../#.#./##.#\n" +
            "###/#.#/### => #.#./..##/#.#./#...\n" +
            "###/###/### => ##../.###/###./#..#";
}
