package se.simpor.adventofcode.year2017;

import java.util.*;
import java.util.stream.Collectors;

/**
 * --- Day 6: Memory Reallocation ---
 * <p>
 * A debugger program here is having an issue: it is trying to repair a memory reallocation routine, but it keeps getting stuck in an infinite loop.
 * <p>
 * In this area, there are sixteen memory banks; each memory bank can hold any number of blocks. The goal of the reallocation routine is to balance the blocks between the memory banks.
 * <p>
 * The reallocation routine operates in cycles. In each cycle, it finds the memory bank with the most blocks (ties won by the lowest-numbered memory bank) and redistributes those blocks among the banks. To do this, it removes all of the blocks from the selected bank, then moves to the next (by index) memory bank and inserts one of the blocks. It continues doing this until it runs out of blocks; if it reaches the last memory bank, it wraps around to the first one.
 * <p>
 * The debugger would like to know how many redistributions can be done before a blocks-in-banks configuration is produced that has been seen before.
 * <p>
 * For example, imagine a scenario with only four memory banks:
 * <p>
 * The banks start with 0, 2, 7, and 0 blocks. The third bank has the most blocks, so it is chosen for redistribution.
 * Starting with the next bank (the fourth bank) and then continuing to the first bank, the second bank, and so on, the 7 blocks are spread out over the memory banks. The fourth, first, and second banks get two blocks each, and the third bank gets one back. The final result looks like this: 2 4 1 2.
 * Next, the second bank is chosen because it contains the most blocks (four). Because there are four memory banks, each gets one block. The result is: 3 1 2 3.
 * Now, there is a tie between the first and fourth memory banks, both of which have three blocks. The first bank wins the tie, and its three blocks are distributed evenly over the other three banks, leaving it with none: 0 2 3 4.
 * The fourth bank is chosen, and its four blocks are distributed such that each of the four banks receives one: 1 3 4 1.
 * The third bank is chosen, and the same thing happens: 2 4 1 2.
 * At this point, we've reached a state we've seen before: 2 4 1 2 was already seen. The infinite loop is detected after the fifth block redistribution cycle, and so the answer in this example is 5.
 * <p>
 * Given the initial block counts in your puzzle input, how many redistribution cycles must be completed before a configuration is produced that has been seen before?
 */
public class Day6Java {

    public int solve1(String problem) {
        String[] split = problem.split("\t");
        int[] nums = new int[split.length];
        for (int i = 0; i < split.length; i++) {
            nums[i] = Integer.parseInt(split[i]);
        }

        Set<String> combo = new HashSet<>();
        int counter = 0;

        while (true) {
            combo.add(key(nums));

            int max = 0;
            int index = 0;
            for (int i = 0; i < nums.length; i++) {
                int num = nums[i];
                if (num > max) {
                    max = num;
                    index = i;
                }
            }
            nums[index] = 0;

            // spread out..
            while (max > 0) {
                index++;
                if (index > nums.length - 1) index = 0;
                nums[index] += 1;
                max--;
            }

            counter++;
            if (combo.contains(key(nums))) {
                return counter;
            }
        }

    }

    private String key(int[] nums) {
        return Arrays.stream(nums).mapToObj(Integer::toString).collect(Collectors.joining("-"));
    }


    /**
     * Out of curiosity, the debugger would also like to know the size of the loop: starting from a state that has already been seen,
     * how many block redistribution cycles must be performed before that same state is seen again?
     * <p>
     * In the example above, 2 4 1 2 is seen again after four cycles, and so the answer in that example would be 4.
     * <p>
     * How many cycles are in the infinite loop that arises from the configuration in your puzzle input?
     *
     * @param problem
     * @return
     */
    public int solve2(String problem) {
        String[] split = problem.split("\t");
        int[] nums = new int[split.length];
        for (int i = 0; i < split.length; i++) {
            nums[i] = Integer.parseInt(split[i]);
        }

        Map<String, Integer> combo = new HashMap<>();
        int counter = 0;

        while (true) {
            combo.put(key(nums), counter);

            int max = 0;
            int index = 0;
            for (int i = 0; i < nums.length; i++) {
                int num = nums[i];
                if (num > max) {
                    max = num;
                    index = i;
                }
            }
            nums[index] = 0;

            // spread out..
            while (max > 0) {
                index++;
                if (index > nums.length - 1) index = 0;
                nums[index] += 1;
                max--;
            }

            counter++;
            if (combo.containsKey(key(nums))) {
                return counter - combo.get(key(nums));
            }
        }
    }


    public static void main(String[] args) {
        Day6Java day1Java = new Day6Java();
        int solve1 = day1Java.solve1(example);
        int solve1Real = day1Java.solve1(realProblem);
        int solve2 = day1Java.solve2(example);
        int solve2Real = day1Java.solve2(realProblem);
        System.out.println(solve1);
        System.out.println(solve1Real);
        System.out.println(solve2);
        System.out.println(solve2Real);
    }

    static String example = "0\t2\t7\t0";
    static String realProblem = "4\t1\t15\t12\t0\t9\t9\t5\t5\t8\t7\t3\t14\t5\t12\t3";
}
