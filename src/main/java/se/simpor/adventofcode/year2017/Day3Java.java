package se.simpor.adventofcode.year2017;

import java.util.HashMap;
import java.util.Map;

/**
 * You come across an experimental new kind of memory stored on an infinite two-dimensional grid.
 * <p>
 * Each square on the grid is allocated in a spiral pattern starting at a location marked 1 and then counting up while spiraling outward. For example, the first few squares are allocated like this:
 * <p>
 * 17  16  15  14  13
 * 18   5   4   3  12
 * 19   6   1   2  11
 * 20   7   8   9  10
 * 21  22  23---> ...
 * While this is very space-efficient (no squares are skipped), requested data must be carried back to square 1 (the location of the only access port for this memory system) by programs that can only move up, down, left, or right. They always take the shortest path: the Manhattan Distance between the location of the data and square 1.
 * <p>
 * For example:
 * <p>
 * Data from square 1 is carried 0 steps, since it's at the access port.
 * Data from square 12 is carried 3 steps, such as: down, left, left.
 * Data from square 23 is carried only 2 steps: up twice.
 * Data from square 1024 must be carried 31 steps.
 */
public class Day3Java {

    public int solve1(int number) {
        int sum = 0;
        Map<Tuple, Integer> board = new HashMap<>();

        board.put(t(0, 0), 1);
        board.put(t(1, 0), 2);
        board.put(t(1, 1), 3);
        Tuple prev = t(1, 1);
        Tuple next = null;
        for (int i = 4; i <= number; i++) {
            int defaultValue = -1;
            int up = board.getOrDefault(t(prev.x, prev.y + 1), defaultValue);
            int down = board.getOrDefault(t(prev.x, prev.y - 1), defaultValue);
            int left = board.getOrDefault(t(prev.x - 1, prev.y), defaultValue);
            int right = board.getOrDefault(t(prev.x + 1, prev.y), defaultValue);
            int lastNum = board.get(prev);
            next = getNext(prev, next, defaultValue, up, down, left, right, lastNum);
//            System.out.println(String.format("%s -> u: %s d: %s r: %s l: %s", lastNum, up, down, right, left));
//            System.out.println(String.format("%s -> x: %s y: %s", i, next.x, next.y));

            board.put(next, i);
            prev = next;
        }

        return Math.abs(next.x) + Math.abs(next.y);
    }

    private Tuple getNext(Tuple prev, Tuple next, int defaultValue, int up, int down, int left, int right, int lastNum) {
        if (up == lastNum - 1) {
            if (right == defaultValue) {
                next = t(prev.x + 1, prev.y);
            } else {
                next = t(prev.x, prev.y - 1);
            }
        } else if (down == lastNum - 1) {
            if (left == defaultValue) {
                next = t(prev.x - 1, prev.y);
            } else {
                next = t(prev.x, prev.y + 1);
            }
        } else if (right == lastNum - 1) {
            if (down == defaultValue) {
                next = t(prev.x, prev.y - 1);
            } else {
                next = t(prev.x - 1, prev.y);
            }
        } else if (left == lastNum - 1) {
            if (up == defaultValue) {
                next = t(prev.x, prev.y + 1);
            } else {
                next = t(prev.x + 1, prev.y);
            }
        }
        return next;
    }


    public int solve2(int number) {
        Map<Tuple, Integer> board = new HashMap<>();
        Map<Tuple, Integer> sumBoard = new HashMap<>();

        board.put(t(0, 0), 1);
        board.put(t(1, 0), 2);
        board.put(t(1, 1), 3);
        sumBoard.put(t(0, 0), 1);
        sumBoard.put(t(1, 0), 1);
        sumBoard.put(t(1, 1), 2);
        Tuple prev = t(1, 1);
        Tuple next = null;
        for (int i = 4; i <= number; i++) {
            int defaultValue = -100;
            int up = board.getOrDefault(t(prev.x, prev.y + 1), defaultValue);
            int upLeft = board.getOrDefault(t(prev.x - 1, prev.y + 1), defaultValue);
            int upRight = board.getOrDefault(t(prev.x + 1, prev.y + 1), defaultValue);
            int down = board.getOrDefault(t(prev.x, prev.y - 1), defaultValue);
            int downLeft = board.getOrDefault(t(prev.x - 1, prev.y - 1), defaultValue);
            int downRight = board.getOrDefault(t(prev.x + 1, prev.y - 1), defaultValue);
            int left = board.getOrDefault(t(prev.x - 1, prev.y), defaultValue);
            int right = board.getOrDefault(t(prev.x + 1, prev.y), defaultValue);
            int lastNum = board.get(prev);
            next = getNext(prev, next, defaultValue, up, down, left, right, lastNum);
//            System.out.println(String.format("%s -> u: %s d: %s r: %s l: %s", lastNum, up, down, right, left));
//            System.out.println(String.format("%s -> x: %s y: %s", i, next.x, next.y));

            board.put(next, i);
            int sum = sumBoard.getOrDefault(t(next.x, next.y + 1), 0) +
                    sumBoard.getOrDefault(t(next.x - 1, next.y + 1), 0) +
                    sumBoard.getOrDefault(t(next.x + 1, next.y + 1), 0) +
                    sumBoard.getOrDefault(t(next.x, next.y - 1), 0) +
                    sumBoard.getOrDefault(t(next.x - 1, next.y - 1), 0) +
                    sumBoard.getOrDefault(t(next.x + 1, next.y - 1), 0) +
                    sumBoard.getOrDefault(t(next.x - 1, next.y), 0) +
                    sumBoard.getOrDefault(t(next.x + 1, next.y), 0);
            sumBoard.put(next, sum);
            prev = next;
            if (sum > number) {
                return sumBoard.get(next);
            }
        }

        return -1;
    }

    private Tuple t(int x, int y) {
        Tuple tuple = new Tuple();
        tuple.x = x;
        tuple.y = y;
        return tuple;
    }


    public static void main(String[] args) {
        Day3Java day1Java = new Day3Java();
        //int solve1 = day1Java.solve1(10);
//        int solve1 = day1Java.solve1(361527);
//        System.out.println(solve1);
        int solve2 = day1Java.solve2(361527);
        System.out.println(solve2);
    }

    private class Tuple {
        int x;
        int y;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Tuple tuple = (Tuple) o;

            if (x != tuple.x) return false;
            return y == tuple.y;
        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            return result;
        }
    }
}
