package se.simpor.adventofcode.year2018;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Day2 {

    public static void main(String[] args) {
        part1();

        List<List<String>> stringList = Arrays.stream(input.split("\n"))
                .map(r -> r.chars()
                        .mapToObj(i -> String.valueOf(Character.toChars(i)))
                        .collect(Collectors.toList())
                ).collect(Collectors.toList());
        System.out.println(stringList);

        int lowest = Integer.MAX_VALUE;
        List<List<String>> list = new ArrayList<>();
        for (int i = 0; i < stringList.size(); i++) {
            List<String> strings = stringList.get(i);
            for (int j = 0; j < stringList.size(); j++) {
                List<String> second = stringList.get(j);
                if (i != j) {
                    int misses = 0;
                    for (int k = 0; k < strings.size(); k++) {
                        String s1 = strings.get(k);
                        String s2 = second.get(k);
                        if (!s1.equals(s2)) misses++;
                    }

                    if (misses < lowest) {
                        lowest = misses;
                        list.clear();
                        list.add(strings);
                        list.add(second);
                    }
                }

            }
        }
        System.out.println("lowest: " + lowest);
        System.out.println("list: " + list.stream().map(l -> l.stream().collect(Collectors.joining())).collect(Collectors.toList()));

        String result = "";
        for (int i = 0; i < list.get(0).size(); i++) {
            String s1 = list.get(0).get(i);
            String s2 = list.get(1).get(i);
            if (s1.equals(s2)){
                result += s1;
            }
        }
        System.out.println("result: " + result);
    }

    private static void part1() {
        Map<Integer, Integer> collect = Arrays.stream(input.split("\n"))
                .map(r -> r.chars()
                        .sorted()
                        .mapToObj(i -> String.valueOf(Character.toChars(i)))
                        .collect(Collectors.toMap(a -> a, a -> 1, (o, v) -> o + 1))
                        .entrySet()
                        .stream()
                        .filter(e -> e.getValue() == 2 || e.getValue() == 3)
                        .collect(Collectors.toMap(Map.Entry::getValue, e -> 1, (o, n) -> o))
                )
                .flatMap(m -> m.keySet().stream())
                .collect(Collectors.toMap(k -> k, v -> 1, (o, v) -> o + 1));

        System.out.println(collect.get(2) * collect.get(3));
    }

    private static String input = "prtkqyluibmtcwqaezjmhgfndx\n" +
            "prtkqylusbsmcwvaezjmhgfndt\n" +
            "prgkqyluibsocwvamzjmhgkndx\n" +
            "prjkqyluibsocwvahzjmhgfnsx\n" +
            "prtkqylcibsocwvzezjohgfndx\n" +
            "prtkqyluiksocwziezjmhgfndx\n" +
            "prikqyluiksocwvaezjmkgfndx\n" +
            "prtkgyluibsocwvwezjehgfndx\n" +
            "prtkqyluiysocwvaezjghxfndx\n" +
            "prtkqwluibsoxwvaezjmhgfhdx\n" +
            "prtkqylgibsocwvabzjmhzfndx\n" +
            "prtknyltibnocwvaezjmhgfndx\n" +
            "prdkqyluibrocwvaezjmhgnndx\n" +
            "prtwqyluibsoctvcezjmhgfndx\n" +
            "mrtkqyluibgocwvakzjmhgfndx\n" +
            "prtkqaouibsocwvaezjmhwfndx\n" +
            "prtkqyluihjocwvaezjmhgfpdx\n" +
            "prtkqyluikfxcwvaezjmhgfndx\n" +
            "prtkqybuixsocwvaczjmhgfndx\n" +
            "pvtkayluibsocwxaezjmhgfndx\n" +
            "grtkqgluibsocdvaezjmhgfndx\n" +
            "prlkqyluibsochvaezjmhgzndx\n" +
            "prtkqylxibsocmvaezjmhgfkdx\n" +
            "prtkqyluibsqctvaezjmpgfndx\n" +
            "putkqyluibsocqvaezjmhgfndw\n" +
            "prtjqyluibsiclvaezjmhgfndx\n" +
            "prtkqylvpvsocwvaezjmhgfndx\n" +
            "prnkqyluibsocwvaezjmhefsdx\n" +
            "prtktyluibsocwvaezjkhgrndx\n" +
            "prtkqyluibcovwvaezjthgfndx\n" +
            "prtkqcluibiocwvaezjmhggndx\n" +
            "prtkqyluihsocwveezjmhgfydx\n" +
            "prtklyluibsocwqaszjmhgfndx\n" +
            "prtkqyluibsocwvaezjmfznndx\n" +
            "prtkjyluijsocwvaeejmhgfndx\n" +
            "prtkqtluibsonwvaexjmhgfndx\n" +
            "prtkqyluinsocwbaezjmjgfndx\n" +
            "prtkqyluibslckvaezjmhgyndx\n" +
            "prtkqyluibsodwlpezjmhgfndx\n" +
            "prtkquluibsfcwvaezjhhgfndx\n" +
            "prtkqyluhbsocweaezsmhgfndx\n" +
            "prrkqyluinsocxvaezjmhgfndx\n" +
            "prtkqyluibsoswvaezjmhgyqdx\n" +
            "prtkqbluibdocwvlezjmhgfndx\n" +
            "prtkqyfuibsocpvaezjmhgfnwx\n" +
            "prtkqlluibsqjwvaezjmhgfndx\n" +
            "prtkqyluibrocwvaehjmjgfndx\n" +
            "prtkqyluibsoowvaezgmhgendx\n" +
            "wrtjqyluibsocwvaezfmhgfndx\n" +
            "prtvqyluhbsocwvaezjmtgfndx\n" +
            "prtkqyllibspcwvaezjmkgfndx\n" +
            "pqtzqyeuibsocwvaezjmhgfndx\n" +
            "prtkqyluibsolpvaezjmegfndx\n" +
            "przkayguibsocwvaezjmhgfndx\n" +
            "prtkqyluidsocwvaezjmyufndx\n" +
            "prtuqyluibsocwvaezjmfgfnkx\n" +
            "prtkqwluibsrcwvaezjchgfndx\n" +
            "prtkqyluibsotwhaozjmhgfndx\n" +
            "erwkqylhibsocwvaezjmhgfndx\n" +
            "prtkqyluibsocwvgezjmkgfedx\n" +
            "prskqyluiesocwvaezjmggfndx\n" +
            "prtkqylmitsocwvaezjmhgfnox\n" +
            "prtkqyluinnocwvaezjmhgfkdx\n" +
            "prtktyluibsokwvaezjmhgfcdx\n" +
            "prtkqyluibsomwvakvjmhgfndx\n" +
            "prtkqyltibloawvaezjmhgfndx\n" +
            "prtkqyluibxocwvaezgmhgqndx\n" +
            "prtkqyluibskcmvaezjmhgfngx\n" +
            "artkqylubbsotwvaezjmhgfndx\n" +
            "prtkqyluibzocwvhezjmhgfnbx\n" +
            "prskqkluibsocwvaezjmhgfjdx\n" +
            "prtkqyluibwocwvaezjkhglndx\n" +
            "prukqyluissocwvzezjmhgfndx\n" +
            "puhkqyluibsocwvaezjmhgfsdx\n" +
            "qrtkqyluibsocwvaeujmhgfndd\n" +
            "prtkqyluibsoctvaezjmagfnda\n" +
            "prtkquluibsocwkaezjmhgfqdx\n" +
            "prtkqyluubswcwvaezjmhvfndx\n" +
            "prfkqyluibsocwvaemrmhgfndx\n" +
            "pmtkqyluibpocwvaezjmhggndx\n" +
            "prtkqvluibiocwvaezjqhgfndx\n" +
            "prtkgypuibsocwvaezcmhgfndx\n" +
            "prtpqyquibsovwvaezjmhgfndx\n" +
            "prtwqyluiasocwvaexjmhgfndx\n" +
            "mrtzqyluibbocwvaezjmhgfndx\n" +
            "prtkqyluibsocwmaegwmhgfndx\n" +
            "prtkqyluibvncwvaqzjmhgfndx\n" +
            "prtkqyluiusocwvaezjmhmfbgx\n" +
            "prtkqyljibvocwvaezjehgfndx\n" +
            "prtkqyloibsopavaezjmhgfndx\n" +
            "prckqyakibsocwvaezjmhgfndx\n" +
            "prtkqyluibsdcwvaezjmngfddx\n" +
            "prekqylupbsocwvaezemhgfndx\n" +
            "hrtkqyluibhocwvaezjmhgfnde\n" +
            "prmkqyluibsocwvaezzfhgfndx\n" +
            "prtkqyluiccfcwvaezjmhgfndx\n" +
            "pdtkqyluxbsocwvaezjmhgendx\n" +
            "prokqyluibsocwvuezjmsgfndx\n" +
            "prtkqyluibsacwvaezjyhgfndv\n" +
            "prtkqmluibsocavaezjmhgfndc\n" +
            "prtkqyluibsocwvmezjmhgtnqx\n" +
            "prtkqytuibiocyvaezjmhgfndx\n" +
            "pktkqyiuibsocwvwezjmhgfndx\n" +
            "grtrqyluibsocwvaezjmhgfbdx\n" +
            "prtkqylsibjocwvaezjmhgfnyx\n" +
            "prtkqyhutbsocwvaexjmhgfndx\n" +
            "prtknyluibsocmvaezumhgfndx\n" +
            "prtkwyluibsocwvahzjmhgpndx\n" +
            "prtkqywuibsolhvaezjmhgfndx\n" +
            "prtkcyluibsoccvaezjthgfndx\n" +
            "prtkqyrdibsocwvaezjbhgfndx\n" +
            "prtkqyhuqbsocwvaezjmhgfxdx\n" +
            "pytkqyluibsocwvagzjmhgfndv\n" +
            "prtkqyliibsocwvaexwmhgfndx\n" +
            "prtkqyluibshcwvaeljphgfndx\n" +
            "prtkqyluibsocwvaerjzhbfndx\n" +
            "prtkqyduibsocwvaezvmhgfnzx\n" +
            "drtkqylhibsocwvaezjmhmfndx\n" +
            "prtkqyluibsocwvaezamfvfndx\n" +
            "brtkqyluqbsocwvaezjmhgpndx\n" +
            "prtkqyiuibsocwvuezjmhgfngx\n" +
            "urtkqyluibsocqvaeljmhgfndx\n" +
            "prtkqyluikaocwvaezjmhgfjdx\n" +
            "prqkqzouibsocwvaezjmhgfndx\n" +
            "prtkqyluibsocxvaezjmhgfnxv\n" +
            "prlkqyluibsoxwvaeijmhgfndx\n" +
            "prthuyluibsocwvaezjmhgfnhx\n" +
            "potkqyluizsocwvaezjmhifndx\n" +
            "fstkqyduibsocwvaezjmhgfndx\n" +
            "prtkqxluibsocwvaezjmhgffdm\n" +
            "prtkqylpibsozwvaezmmhgfndx\n" +
            "prxkqylbibsocwvaezjphgfndx\n" +
            "srtkqyluibsicnvaezjmhgfndx\n" +
            "prtktyluibsocwvaezjvhgfnax\n" +
            "pctkqyluxbsocwvaezwmhgfndx\n" +
            "prtkqylusbsoclvaezsmhgfndx\n" +
            "pwtkqyluibsocrvaezjmggfndx\n" +
            "prtkqyluibswcwraezjmhgfndd\n" +
            "prtkqyluibtocwiaezjmhgfnax\n" +
            "prtuqyluibsocwvajzjmngfndx\n" +
            "pwtkqyluibsocwvaerjmogfndx\n" +
            "petkqexuibsocwvaezjmhgfndx\n" +
            "pztkqyluibsocwvaerqmhgfndx\n" +
            "prtkqyluobsocwvaezjmapfndx\n" +
            "prtkqyluiinocwvaeljmhgfndx\n" +
            "prtkqyluibsoowvxezjmhgfnnx\n" +
            "lrtkqyluibsocwvfezjmhgfndc\n" +
            "prtkqyluibokcwvahzjmhgfndx\n" +
            "prtkqmlufbsocwvaegjmhgfndx\n" +
            "prtkqylribsocwvanzjmhgfnda\n" +
            "prtkqyluibspxwvaezkmhgfndx\n" +
            "prtiqyluibsbcwvaezjmhgfntx\n" +
            "prikqzluinsocwvaezjmhgfndx\n" +
            "prtkqnldibsocwvaezjmhxfndx\n" +
            "prtkqyluixsocsvaezjmhwfndx\n" +
            "hrtkqyluibsocwvaezjhhgfodx\n" +
            "prtkqyluibsrcwvaezjmhpfwdx\n" +
            "prtkqyluibsocwyaezjmhgffdk\n" +
            "prtkqyluidsocwvalmjmhgfndx\n" +
            "prukquluabsocwvaezjmhgfndx\n" +
            "prckqyluinsmcwvaezjmhgfndx\n" +
            "prbkqymuibsocwvaezjmhgfndc\n" +
            "prtkfylaibsocwvaezjmkgfndx\n" +
            "zrtkqyluibsocwvrbzjmhgfndx\n" +
            "crtkqyluibsocwvaejjmkgfndx\n" +
            "prttqyluibsocyvaezymhgfndx\n" +
            "prtkqylugbsocwvaezjxhgfmdx\n" +
            "prtkqyluibsocwdlezjmhgfnbx\n" +
            "prtkqjluibsocwvaozjhhgfndx\n" +
            "prtcjyluibsocwbaezjmhgfndx\n" +
            "rrtkqyluiblocwvaezjmhgundx\n" +
            "prtkkyluibsocwfaezjmhgfnyx\n" +
            "prtkqyuuibsocwvaezjmhgfogx\n" +
            "prtkyyluvbsocwvaezjmhgfnox\n" +
            "prpkqyluibyocwvaezjmhggndx\n" +
            "pdtkqyluibdocwvaezjmhgfndy\n" +
            "prtklysuibsocwvaezjmhgfnwx\n" +
            "prtkqyluabsouwvaekjmhgfndx\n" +
            "phtkqyluibsocwvaezjmhgfnxt\n" +
            "prtkqyxuibsocwvaezjmhpfnqx\n" +
            "prtkqyluibsodwsaezdmhgfndx\n" +
            "prtkbyluibsohwvaezjmhgfndr\n" +
            "xrtkqylhibsocwvtezjmhgfndx\n" +
            "prtkqyluvysocwvaezbmhgfndx\n" +
            "prtkqieuibsocwvaeojmhgfndx\n" +
            "pctkqyluibsocwvanzjmhgfnux\n" +
            "vrtkqyluibsozwvaezjmhgandx\n" +
            "prtkqyluiusocwvaezjmhmfngx\n" +
            "prbkqyluibsockvaxzjmhgfndx\n" +
            "prtkqyluibsonwvaczjmhgfndi\n" +
            "prtkqyluiblocwvaezjmhgfnau\n" +
            "prtkqyluibsocwvafzuchgfndx\n" +
            "prdkqyluiysocwvaezjmhgfnax\n" +
            "prnkqyouibsocwvaezjmhgfndq\n" +
            "mrtkqgluibsocwvpezjmhgfndx\n" +
            "pvtkqyluibsocwvaczjmhgnndx\n" +
            "trtkqwluibsohwvaezjmhgfndx\n" +
            "prmkqyluibsofwvaezjmhgfrdx\n" +
            "prtyqyluibpdcwvaezjmhgfndx\n" +
            "ertkqylulbsocwvaezjmhgfnax\n" +
            "prtkqyluibsacwvaeijmhgfndf\n" +
            "prtkqyluibyocwvapzjmhgpndx\n" +
            "potkqyluibgocwvaezjmhzfndx\n" +
            "prtkqyluibsocwyaezxmhgfnpx\n" +
            "prtkqkjuibsncwvaezjmhgfndx\n" +
            "prtqqyluibsocwlaezjmhgkndx\n" +
            "prtkxyluibnocwvaezjmhgkndx\n" +
            "prtkqyluiosocwvapzjmxgfndx\n" +
            "prtkqylumbsocwvyezimhgfndx\n" +
            "prukqyluibsocwvyezjmhgindx\n" +
            "prtkqylbibstcwvaezjxhgfndx\n" +
            "pctkqyuuibsocwvaezjuhgfndx\n" +
            "vrtkqyluibsocwvaezjmhgfnll\n" +
            "urtkqyluibsopwvaezjphgfndx\n" +
            "prtkceluibsocwvaepjmhgfndx\n" +
            "prwkxyluibsocwvaezjmhgfnzx\n" +
            "prtkqyluitsocwvaezqzhgfndx\n" +
            "prtkqkauibsorwvaezjmhgfndx\n" +
            "prtkqyluibsocwvaezfmftfndx\n" +
            "prtkiybuibsocwvaezjkhgfndx\n" +
            "prtkzyluibsocwgaezjmvgfndx\n" +
            "prtkqyluibsocwvaezjmhgqnxg\n" +
            "prtkqyluimsocwvauzjwhgfndx\n" +
            "prtkqyluibsacwgaezjmhgfndd\n" +
            "pwtkuyluibsccwvaezjmhgfndx\n" +
            "prtkqyluibsoawvaezjmvgfnlx\n" +
            "prtkqyluabsocwwaezjmhgftdx\n" +
            "patkqylnibsocwvaezjmhgfnox\n" +
            "prtkqyluibsocwlaxzkmhgfndx\n" +
            "pbtkqpluibsfcwvaezjmhgfndx\n" +
            "prtkqyluibsoywsaezjmhgxndx\n" +
            "prtkqyluibfocwvaezjyhgfhdx\n" +
            "pltbqylcibsocwvaezjmhgfndx\n" +
            "prtkdyluiisocwvvezjmhgfndx\n" +
            "prtkqkxuibsokwvaezjmhgfndx\n" +
            "prtkqyluibsoawvaezzmhgfndm\n" +
            "petkqyluibsgcwvaezjmhgfndu\n" +
            "prtkqyluibsoyxvaezjmlgfndx\n" +
            "prtkqyluibxocwvaezgmhnfndx\n" +
            "prtkikluibsocwvwezjmhgfndx\n" +
            "prbkqyluibsocwvaezjhhgfnux\n" +
            "prtkqylufbsxcwvaezjmhgfnfx\n" +
            "prtkqyluibsdcdvaezjmhgxndx\n" +
            "potkiyluibsocwvaezjmhkfndx\n" +
            "prtkqyluiosocsvhezjmhgfndx\n" +
            "prtkqyluibsocqbaezomhgfndx\n" +
            "prtihyluibsocwvaeujmhgfndx\n" +
            "prtuquruibsocwvaezjmhgfndx\n" +
            "prtkqyloibsocwvaeztmhifndx\n" +
            "ertuqyluibsocwvaeajmhgfndx";
}
