package year2019

/*
At the first Go / No Go poll, every Elf is Go until the Fuel Counter-Upper.
They haven't determined the amount of fuel required yet.

Fuel required to launch a given module is based on its mass.
Specifically, to find the fuel required for a module, take its mass, divide by three, round down, and subtract 2.

For example:

For a mass of 12, divide by 3 and round down to get 4, then subtract 2 to get 2.
For a mass of 14, dividing by 3 and rounding down still yields 4, so the fuel required is also 2.
For a mass of 1969, the fuel required is 654.
For a mass of 100756, the fuel required is 33583.
*/


fun main() {
    println("12 = 2, fuel(12) = ${fuel(12)}")
    println("14 = 2, fuel(14) = ${fuel(14)}")
    println("1969 = 654, fuel(1969) = ${fuel(1969)}")
    println("100756 = 33583, fuel(100756) = ${fuel(100756)}")

    val part1Answer = part1.split("\n")
            .map { i -> fuel(i.toLong()) }
            .sum()

    println("Part1 = $part1Answer")

    println("14 = 2, fuel(14) = ${fuelTotal(14)}")
    println("1969 = 966, fuel(1969) = ${fuelTotal(1969)}")
    println("100756 = 50346, fuel(100756) = ${fuelTotal(100756)}")

    val part2Answer = part1.split("\n")
            .map { i -> fuelTotal(i.toLong()) }
            .sum()

    println("Part2 = $part2Answer")
}

fun fuel(mass: Long): Long {
    return (mass / 3) - 2
}

fun fuelTotal(mass: Long): Long {
    fun fuelRec(mass: Long, total: Long): Long {
        val fuel = fuel(mass)
        return if (fuel <= 0) total
        else fuelRec(fuel, total + fuel)
    }
    return fuelRec(mass, 0)
}

val part1 = """
147383
111288
130868
140148
79840
63305
98475
66403
68753
136306
94135
51317
136151
71724
68795
68526
130515
73606
56828
57778
86134
105030
123367
97633
85043
110888
110785
90662
128865
70997
90658
79944
141089
67543
78358
143579
146971
78795
94097
82473
73216
50919
100248
112751
86227
117399
123833
148570
141464
123266
94346
53871
51180
112900
119863
106694
129841
75990
63509
50135
140081
138387
112697
57023
114256
81429
95573
57056
52277
75137
53364
125823
113227
93993
129808
114025
101677
127114
65823
65834
57955
102314
60656
89982
61068
72089
71745
72460
142318
91951
111759
61177
143739
92202
70168
80164
77867
64235
141137
102636
""".trimIndent()