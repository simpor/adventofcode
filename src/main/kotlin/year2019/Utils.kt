package year2019

data class InputWithInt(val answer: Int, val input: String)
data class InputWithLong(val answer: Long, val input: String)
data class InputWithString(val answer: String, val input: String)

data class Point(val x: Int, val y: Int)

data class PointValue(val point: Point, val value: Int)

fun checkAnswer(inputWithInt: InputWithInt, result: Int) {
    if (inputWithInt.answer != result) {
        println("Should have gotten ${inputWithInt.answer} but got $result")
    } else {
        println("Correct answer $result")
    }
}

fun checkAnswer(inputWithLong: InputWithLong, result: Long) {
    if (inputWithLong.answer != result) {
        println("Should have gotten ${inputWithLong.answer} but got $result")
    } else {
        println("Correct answer $result")
    }
}

fun checkAnswer(inputWithString: InputWithString, result: String) {
    if (inputWithString.answer != result) {
        println("Should have gotten ${inputWithString.answer} but got $result")
    } else {
        println("Correct answer $result")
    }
}


fun uniqueNumber(input: List<String> = listOf("1", "2", "3", "4", "0")): List<String> {
    val returnList = mutableListOf<String>()

    val allCombos = allCombos(input, 5)

    allCombos.forEach {
        val str = it
        var add = true
        str.forEachIndexed { i1, c1 ->
            str.forEachIndexed { i2, c2 ->
                if (i1 != i2 && c1 == c2) {
                    add = false
                }
            }
        }
        if (add) returnList.add(str)
    }
    return returnList
}

fun allCombos(set: List<String>, k: Int): List<String> {
    val n = set.size
    val list = mutableListOf<String>()
    allCombosRec(set, "", n, k, list);
    return list
}

fun allCombosRec(set: List<String>,
                 prefix: String,
                 n: Int, k: Int, list: MutableList<String>) {
    if (k == 0) {
        list.add(prefix)
        return
    }
    for (i in 0 until n) {
        val newPrefix = prefix + set[i]
        allCombosRec(set, newPrefix, n, k - 1, list)
    }
}

// Driver Code
fun main() {
    val set1 = listOf("a", "b")
    val k = 3
    println("First test " + allCombos(set1, k))

    val set2 = listOf("a", "b", "c", "d")
    val k2 = 1
    println("Second test " + allCombos(set2, k2))

    val set3 = listOf("0", "1", "2", "3", "4")
    val k3 = 5
    println("Third test: " + allCombos(set3, k3))


}