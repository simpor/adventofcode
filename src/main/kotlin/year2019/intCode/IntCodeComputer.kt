package year2019.intCode

fun printer(string: String, props: Props) {
    //println(string)
    props.debugPrints.add(string)
}

fun String.longCodes(): MutableMap<Long, Long> {
    return this.split(",")
            .map { it.toLong() }
            .mapIndexed { index, i -> Pair(index.toLong(), i) }
            .associate { t -> t }
            .toMutableMap()
}

fun Char.longCode(): Long {
    return this.toString().toLong()
}

data class Props(val instructions: MutableMap<Long, Long>,
                 val storeInstructions: MutableList<Long>,
                 val executedInstructions: MutableList<String> = mutableListOf(),
                 val debugPrints: MutableList<String> = mutableListOf(),
                 var index: Long = 0,
                 var endInstruction: Boolean = false,
                 var relative: Long = 0,
                 var modes: List<Long> = listOf(0, 0, 0),
                 val feedbackMode: Boolean = false,
                 var forceExit: Boolean = false,
                 val print: MutableList<Long> = mutableListOf()) {
    fun p1(): Long = this.modes[0]
    fun p2(): Long = this.modes[1]
    fun p3(): Long = this.modes[2]

    fun v1(): Long = getValue(p1(), index + 1)
    fun v2(): Long = getValue(p2(), index + 2)
    fun v3(): Long = getValue(p3(), index + 3)

    fun i1(): Long = getCode(index + 1)
    fun i2(): Long = getCode(index + 2)
    fun i3(): Long = getCode(index + 3)

    fun getCode(i: Long) = instructions.getOrDefault(i, 0)
    fun toInputList() = instructions.map { t -> t.value.toString() }.joinToString(",")

    private fun getValue(mode: Long, index: Long): Long {
        val value = getCode(index)
        return when (mode) {
            0L -> getCode(value)
            1L -> value
            2L -> getCode(value + relative)
            else -> throw Error("Unsupported value: $mode")
        }
    }
}

public fun runProps(props: Props): Props {
    while (props.index < props.instructions.size) {
        val instruction = props.getCode(props.index)
        props.modes = listOf(0, 0, 0)
        props.index = interpretCode(instruction, props)
        if (props.forceExit || props.endInstruction) break
    }
    return props
}

fun parameterMode(props: Props): Long {
    val index = props.index
    val parameters = props.getCode(index).toString()
    val parametersWithZero = when (parameters.length) {
        1, 2 -> throw Exception("$parameters should be longer at index: $index")
        3 -> "00$parameters"
        4 -> "0$parameters"
        else -> parameters
    }

    val firstMode = parametersWithZero[2].longCode()
    val secondMode = parametersWithZero[1].longCode()
    val thirdMode = parametersWithZero[0].longCode()

    props.modes = listOf(firstMode, secondMode, thirdMode)
    val instruction = parametersWithZero.substring(3).toLong()
    return interpretCode(instruction, props, false)
}

private fun interpretCode(instruction: Long, props: Props,
                          checkParameter: Boolean = true): Long {
    printer("instruction: $instruction ", props)
    return when (instruction) {
        1L -> addInstruction(props)
        2L -> multiplyInstruction(props)
        3L -> storeInstruction(props)
        4L -> printInstruction(props)
        5L -> jumpIfTrueInstruction(props)
        6L -> jumpIfFalseInstruction(props)
        7L -> lessThanInstruction(props)
        8L -> equalsInstruction(props)
        9L -> adjustRelativeBase(props)
        99L -> props.apply { endInstruction = true }.index
        else -> if (checkParameter) parameterMode(props) else throw Exception("Unknown parameter: $instruction")
    }
}

fun adjustRelativeBase(props: Props): Long {
    val index = props.index
    printer("adjustRelativeBase: i=$index, [${props.i1()}]", props)
    val num1 = props.v1()
    props.relative += num1
    return index + 2
}

fun addInstruction(props: Props): Long {
    val index = props.index
    val instructions = props.instructions
    printer("add: i=$index, [${props.i1()},${props.i2()}, ${props.i3()}]", props)
    val num1 = props.v1()
    val num2 = props.v2()
    val destination = props.getCode(index + 3)
    instructions[destination] = num1 + num2
    return index + 4
}

fun multiplyInstruction(props: Props): Long {
    val instructions = props.instructions
    val index = props.index
    printer("mul: i=$index, [${props.i1()},${props.i2()}, ${props.i3()}]", props)
    val num1 = props.v1()
    val num2 = props.v2()
    val destination = props.getCode(index + 3)
    instructions[destination] = num1 * num2
    return index + 4
}

fun storeInstruction(props: Props): Long {
    val instructions = props.instructions
    val index = props.index

    printer("store: i=$index, [${props.i1()}]", props)
//        val destination = getValue(props.p1(), index + 1, props)
    val destination = if (props.p1() != 2L) props.i1() else props.getCode(props.i1() + props.relative)
    if (props.feedbackMode && props.storeInstructions.isEmpty()) {
        props.forceExit = true
        return props.index
    }
    val toStore = props.storeInstructions.removeAt(0)
    if (!props.feedbackMode) {
        props.storeInstructions.add(toStore)
    }
    instructions[destination] = toStore
    return index + 2
}

fun printInstruction(props: Props): Long {
    val index = props.index

    printer("print: i=$index, getCode[${props.i1()}]", props)
    val toPrint = props.v1()
    props.print.add(toPrint)
    printer("PrintInstruction: $toPrint", props)
    return 2 + index
}

fun jumpIfTrueInstruction(props: Props): Long {
    val index = props.index
    printer("jumpTrue: i=$index, [${props.i1()},${props.i2()}]", props)
    val num1 = props.v1()
    if (num1 != 0L) return props.v2()
    return index + 3
}

fun jumpIfFalseInstruction(props: Props): Long {
    val index = props.index
    printer("jumpFalse: i=$index, [${props.i1()},${props.i2()}]", props)
    val num1 = props.v1()
    if (num1 == 0L) return props.v2()
    return index + 3
}

fun lessThanInstruction(props: Props): Long {
    val instructions = props.instructions
    val index = props.index

    printer("less: i=$index, [${props.i1()},${props.i2()}, ${props.i3()}]", props)
    val num1 = props.v1()
    val num2 = props.v2()
    val destination = props.getCode(index + 3)
    instructions[destination] = if (num1 < num2) 1L else 0
    return index + 4
}

fun equalsInstruction(props: Props): Long {
    val instructions = props.instructions
    val index = props.index

    printer("equals: i=$index, [${props.i1()},${props.i2()}, ${props.i3()}]", props)
    val num1 = props.v1()
    val num2 = props.v2()
    val destination = props.getCode(index + 3)
    instructions[destination] = if (num1 == num2) 1L else 0

    return index + 4
}


