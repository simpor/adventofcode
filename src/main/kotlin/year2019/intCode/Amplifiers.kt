package year2019.intCode

import year2019.uniqueNumber


fun checkAmplifier(instructions: Map<Long, Long>): Long {
    val map = mutableMapOf<String, Long>()
    val uniqueNumber = uniqueNumber()
    uniqueNumber.forEach { amplifierSetting ->
        val output = runAmplifier(instructions, amplifierSetting)
        map[amplifierSetting] = output

    }
    val max = map.values.max()!!
    return max
}

fun runAmplifier(instructions: Map<Long, Long>, amplifierSetting: String): Long {
    val props1 = Props(instructions.toMutableMap(), mutableListOf(amplifierSetting[0].longCode(), 0))
    printer(amplifierSetting, props1)
    runProps(props1)
    val props2 = Props(instructions.toMutableMap(), mutableListOf(amplifierSetting[1].longCode(), props1.print.last()))
    runProps(props2)
    val props3 = Props(instructions.toMutableMap(), mutableListOf(amplifierSetting[2].longCode(), props2.print.last()))
    runProps(props3)
    val props4 = Props(instructions.toMutableMap(), mutableListOf(amplifierSetting[3].longCode(), props3.print.last()))
    runProps(props4)
    val props5 = Props(instructions.toMutableMap(), mutableListOf(amplifierSetting[4].longCode(), props4.print.last()))
    runProps(props5)
    printer("$amplifierSetting -> ${props5.print[0]}", props5)
    return props5.print[0]
}

fun feedBackAmplifiers(instructions: Map<Long, Long>): Long {
    val map = mutableMapOf<String, Long>()
    val uniqueNumber = uniqueNumber(listOf("5", "6", "7", "8", "9"))
    uniqueNumber.forEach { amplifierSetting ->
        val output = runFeedbackAmplifier(instructions, amplifierSetting)
        map[amplifierSetting] = output

    }
    val max = map.values.max()!!
    return max
}

fun runFeedbackAmplifier(instructions: Map<Long, Long>, amplifierSetting: String): Long {
    val props1 = Props(instructions.toMutableMap(), mutableListOf(amplifierSetting[0].longCode(), 0), feedbackMode = true)
    val props2 = Props(instructions.toMutableMap(), mutableListOf(amplifierSetting[1].longCode()), feedbackMode = true)
    val props3 = Props(instructions.toMutableMap(), mutableListOf(amplifierSetting[2].longCode()), feedbackMode = true)
    val props4 = Props(instructions.toMutableMap(), mutableListOf(amplifierSetting[3].longCode()), feedbackMode = true)
    val props5 = Props(instructions.toMutableMap(), mutableListOf(amplifierSetting[4].longCode()), feedbackMode = true)
    while (true) {
        printer(amplifierSetting, props1)
        runProps(props1)
        props1.storeInstructions.clear()
        props1.forceExit = false
        props2.storeInstructions.add(props1.print.last())
        runProps(props2)
        props2.storeInstructions.clear()
        props2.forceExit = false
        props3.storeInstructions.add(props2.print.last())
        runProps(props3)
        props3.storeInstructions.clear()
        props3.forceExit = false
        props4.storeInstructions.add(props3.print.last())
        runProps(props4)
        props4.storeInstructions.clear()
        props4.forceExit = false
        props5.storeInstructions.add(props4.print.last())
        runProps(props5)
        if (!props5.forceExit) break
        props5.storeInstructions.clear()
        props5.forceExit = false
        props1.storeInstructions.add(props5.print.last())
        printer("$amplifierSetting -> ${props5.print[0]}", props5)
    }

    return props5.print.last()
}
