package year2019.intCode

fun boost(instructions: MutableMap<Long, Long>): Props {
    val props = Props(instructions, mutableListOf(1))
    runProps(props)

    return props
}
