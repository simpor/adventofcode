package year2019.day09

import year2019.InputWithLong
import year2019.InputWithString
import year2019.checkAnswer
import year2019.uniqueNumber

fun printer(string: String, props: LongCodeProps) {
    //println(string)
    props.debugPrints.add(string)
}

val day07Test1 = InputWithLong(43210, "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0")
val day07Test2 = InputWithLong(54321, "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0")
val day07Test3 = InputWithLong(65210, "3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0")
val day07Test4 = InputWithLong(139629729, "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5")
val day07Test5 = InputWithLong(18216, "3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10")
val day09Test1 = InputWithString("109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99,109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99", "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99")
val day09Test2 = InputWithString("1219070632396864", "1102,34915192,34915192,7,4,7,99,0")
val day09Test3 = InputWithString("1125899906842624", "104,1125899906842624,99")
val input05 = """
3,225,1,225,6,6,1100,1,238,225,104,0,1101,72,36,225,1101,87,26,225,2,144,13,224,101,-1872,224,224,4,224,102,8,223,223,1001,224,2,224,1,223,224,223,1102,66,61,225,1102,25,49,224,101,-1225,224,224,4,224,1002,223,8,223,1001,224,5,224,1,223,224,223,1101,35,77,224,101,-112,224,224,4,224,102,8,223,223,1001,224,2,224,1,223,224,223,1002,195,30,224,1001,224,-2550,224,4,224,1002,223,8,223,1001,224,1,224,1,224,223,223,1102,30,44,225,1102,24,21,225,1,170,117,224,101,-46,224,224,4,224,1002,223,8,223,101,5,224,224,1,224,223,223,1102,63,26,225,102,74,114,224,1001,224,-3256,224,4,224,102,8,223,223,1001,224,3,224,1,224,223,223,1101,58,22,225,101,13,17,224,101,-100,224,224,4,224,1002,223,8,223,101,6,224,224,1,224,223,223,1101,85,18,225,1001,44,7,224,101,-68,224,224,4,224,102,8,223,223,1001,224,5,224,1,223,224,223,4,223,99,0,0,0,677,0,0,0,0,0,0,0,0,0,0,0,1105,0,99999,1105,227,247,1105,1,99999,1005,227,99999,1005,0,256,1105,1,99999,1106,227,99999,1106,0,265,1105,1,99999,1006,0,99999,1006,227,274,1105,1,99999,1105,1,280,1105,1,99999,1,225,225,225,1101,294,0,0,105,1,0,1105,1,99999,1106,0,300,1105,1,99999,1,225,225,225,1101,314,0,0,106,0,0,1105,1,99999,7,677,226,224,102,2,223,223,1005,224,329,101,1,223,223,8,677,226,224,1002,223,2,223,1005,224,344,1001,223,1,223,1107,677,677,224,102,2,223,223,1005,224,359,1001,223,1,223,1107,226,677,224,102,2,223,223,1005,224,374,101,1,223,223,7,226,677,224,102,2,223,223,1005,224,389,101,1,223,223,8,226,677,224,1002,223,2,223,1005,224,404,101,1,223,223,1008,226,677,224,1002,223,2,223,1005,224,419,1001,223,1,223,107,677,677,224,102,2,223,223,1005,224,434,101,1,223,223,1108,677,226,224,1002,223,2,223,1006,224,449,101,1,223,223,1108,677,677,224,102,2,223,223,1006,224,464,101,1,223,223,1007,677,226,224,102,2,223,223,1006,224,479,101,1,223,223,1008,226,226,224,102,2,223,223,1006,224,494,101,1,223,223,108,226,226,224,1002,223,2,223,1006,224,509,101,1,223,223,107,226,226,224,102,2,223,223,1006,224,524,101,1,223,223,1107,677,226,224,102,2,223,223,1005,224,539,1001,223,1,223,108,226,677,224,1002,223,2,223,1005,224,554,101,1,223,223,1007,226,226,224,102,2,223,223,1005,224,569,101,1,223,223,8,226,226,224,102,2,223,223,1006,224,584,101,1,223,223,1008,677,677,224,1002,223,2,223,1005,224,599,1001,223,1,223,107,226,677,224,1002,223,2,223,1005,224,614,1001,223,1,223,1108,226,677,224,102,2,223,223,1006,224,629,101,1,223,223,7,677,677,224,1002,223,2,223,1005,224,644,1001,223,1,223,108,677,677,224,102,2,223,223,1005,224,659,101,1,223,223,1007,677,677,224,102,2,223,223,1006,224,674,101,1,223,223,4,223,99,226
""".trimIndent()
val input07 = """
3,8,1001,8,10,8,105,1,0,0,21,34,51,76,101,126,207,288,369,450,99999,3,9,102,4,9,9,1001,9,2,9,4,9,99,3,9,1001,9,2,9,1002,9,3,9,101,3,9,9,4,9,99,3,9,102,5,9,9,1001,9,2,9,102,2,9,9,101,3,9,9,1002,9,2,9,4,9,99,3,9,101,5,9,9,102,5,9,9,1001,9,2,9,102,3,9,9,1001,9,3,9,4,9,99,3,9,101,2,9,9,1002,9,5,9,1001,9,5,9,1002,9,4,9,101,5,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,99,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,99
""".trimIndent()
val input09 = """
1102,34463338,34463338,63,1007,63,34463338,63,1005,63,53,1101,3,0,1000,109,988,209,12,9,1000,209,6,209,3,203,0,1008,1000,1,63,1005,63,65,1008,1000,2,63,1005,63,904,1008,1000,0,63,1005,63,58,4,25,104,0,99,4,0,104,0,99,4,17,104,0,99,0,0,1102,1,550,1027,1101,0,0,1020,1101,30,0,1004,1101,0,22,1014,1102,1,36,1009,1101,37,0,1007,1102,25,1,1010,1102,1,33,1012,1102,282,1,1029,1102,1,488,1025,1101,0,31,1019,1101,0,21,1008,1101,0,35,1015,1101,664,0,1023,1102,26,1,1001,1101,28,0,1016,1102,29,1,1005,1102,1,24,1002,1101,20,0,1018,1101,27,0,1013,1101,38,0,1017,1102,1,1,1021,1102,1,557,1026,1102,1,39,1000,1101,23,0,1006,1101,493,0,1024,1102,1,291,1028,1101,671,0,1022,1101,0,34,1003,1101,0,32,1011,109,10,21108,40,40,8,1005,1018,199,4,187,1105,1,203,1001,64,1,64,1002,64,2,64,109,-14,2108,30,8,63,1005,63,225,4,209,1001,64,1,64,1105,1,225,1002,64,2,64,109,3,2102,1,4,63,1008,63,34,63,1005,63,251,4,231,1001,64,1,64,1106,0,251,1002,64,2,64,109,12,2107,22,-5,63,1005,63,269,4,257,1105,1,273,1001,64,1,64,1002,64,2,64,109,20,2106,0,-3,4,279,1001,64,1,64,1106,0,291,1002,64,2,64,109,-16,21108,41,40,-3,1005,1012,311,1001,64,1,64,1105,1,313,4,297,1002,64,2,64,109,-13,2101,0,2,63,1008,63,30,63,1005,63,335,4,319,1105,1,339,1001,64,1,64,1002,64,2,64,109,-3,2102,1,4,63,1008,63,35,63,1005,63,359,1106,0,365,4,345,1001,64,1,64,1002,64,2,64,109,15,1205,6,377,1105,1,383,4,371,1001,64,1,64,1002,64,2,64,109,5,21102,42,1,-2,1008,1017,39,63,1005,63,403,1106,0,409,4,389,1001,64,1,64,1002,64,2,64,109,-17,21107,43,44,10,1005,1012,431,4,415,1001,64,1,64,1106,0,431,1002,64,2,64,109,14,21107,44,43,-4,1005,1012,451,1001,64,1,64,1106,0,453,4,437,1002,64,2,64,109,1,21102,45,1,-3,1008,1014,45,63,1005,63,479,4,459,1001,64,1,64,1105,1,479,1002,64,2,64,109,7,2105,1,0,4,485,1106,0,497,1001,64,1,64,1002,64,2,64,109,5,1206,-8,513,1001,64,1,64,1106,0,515,4,503,1002,64,2,64,109,-33,2101,0,7,63,1008,63,32,63,1005,63,535,1106,0,541,4,521,1001,64,1,64,1002,64,2,64,109,23,2106,0,8,1001,64,1,64,1106,0,559,4,547,1002,64,2,64,109,-1,21101,46,0,-5,1008,1013,46,63,1005,63,585,4,565,1001,64,1,64,1105,1,585,1002,64,2,64,109,-4,21101,47,0,2,1008,1016,44,63,1005,63,605,1105,1,611,4,591,1001,64,1,64,1002,64,2,64,109,-18,1207,4,38,63,1005,63,627,1106,0,633,4,617,1001,64,1,64,1002,64,2,64,109,5,2107,22,7,63,1005,63,649,1106,0,655,4,639,1001,64,1,64,1002,64,2,64,109,12,2105,1,10,1001,64,1,64,1106,0,673,4,661,1002,64,2,64,109,-10,1208,6,33,63,1005,63,693,1001,64,1,64,1106,0,695,4,679,1002,64,2,64,109,-7,2108,35,7,63,1005,63,715,1001,64,1,64,1106,0,717,4,701,1002,64,2,64,109,6,1208,5,37,63,1005,63,735,4,723,1106,0,739,1001,64,1,64,1002,64,2,64,109,-4,1202,5,1,63,1008,63,34,63,1005,63,765,4,745,1001,64,1,64,1105,1,765,1002,64,2,64,109,29,1206,-7,783,4,771,1001,64,1,64,1105,1,783,1002,64,2,64,109,-28,1201,6,0,63,1008,63,29,63,1005,63,809,4,789,1001,64,1,64,1106,0,809,1002,64,2,64,109,5,1202,2,1,63,1008,63,20,63,1005,63,829,1106,0,835,4,815,1001,64,1,64,1002,64,2,64,109,-1,1201,6,0,63,1008,63,35,63,1005,63,859,1001,64,1,64,1105,1,861,4,841,1002,64,2,64,109,2,1207,-3,25,63,1005,63,879,4,867,1105,1,883,1001,64,1,64,1002,64,2,64,109,13,1205,3,901,4,889,1001,64,1,64,1106,0,901,4,64,99,21101,0,27,1,21101,915,0,0,1106,0,922,21201,1,22987,1,204,1,99,109,3,1207,-2,3,63,1005,63,964,21201,-2,-1,1,21101,0,942,0,1106,0,922,22101,0,1,-1,21201,-2,-3,1,21101,0,957,0,1106,0,922,22201,1,-1,-2,1105,1,968,21202,-2,1,-2,109,-3,2105,1,0
""".trimIndent()

val addTest = InputWithLong(2, "1,0,0,0,99")
val multiplyTest01 = InputWithLong(2, "2,3,0,3,99")
val multiplyTest02 = InputWithLong(2, "2,4,4,5,99,0")
val intCodeTest04 = InputWithLong(30, "1,1,1,4,99,5,6,0,99")
val day5Part1Test = InputWithLong(5577461, input05)
val day5Part2Test = InputWithLong(7161591, input05)
val day7Part1Test = InputWithLong(422858, input07)
val day7Part2Test = InputWithLong(14897241, input07)


fun String.longCodes(): MutableMap<Long, Long> {
    return this.split(",")
            .map { it.toLong() }
            .mapIndexed { index, i -> Pair(index.toLong(), i) }
            .associate { t -> t }
            .toMutableMap()
}

fun Char.intCode(): Int {
    return this.toString().toInt()
}

fun Char.longCode(): Long {
    return this.toString().toLong()
}

fun main() {
    checkAnswer(addTest, runIntCodes(LongCodeProps(addTest.input.longCodes(), mutableListOf(0L))).getCode(0L))
    checkAnswer(multiplyTest01, runIntCodes(LongCodeProps(multiplyTest01.input.longCodes(), mutableListOf(0))).getCode(0))
    checkAnswer(multiplyTest02, runIntCodes(LongCodeProps(multiplyTest02.input.longCodes(), mutableListOf(0))).getCode(0))
    checkAnswer(intCodeTest04, runIntCodes(LongCodeProps(intCodeTest04.input.longCodes(), mutableListOf(0))).getCode(0))
    checkAnswer(day5Part1Test, runIntCodes(LongCodeProps(day5Part1Test.input.longCodes(), mutableListOf(1))).print.last { it != 0L })
    checkAnswer(day5Part2Test, runIntCodes(LongCodeProps(day5Part2Test.input.longCodes(), mutableListOf(5))).print[0])

    checkAnswer(day07Test1, runAmplifier(day07Test1.input.longCodes(), "43210"))
    checkAnswer(day07Test2, runAmplifier(day07Test2.input.longCodes(), "01234"))
    checkAnswer(day07Test3, runAmplifier(day07Test3.input.longCodes(), "10432"))

    checkAnswer(day07Test1, checkAmplifier(day07Test1.input.longCodes()))
    checkAnswer(day07Test2, checkAmplifier(day07Test2.input.longCodes()))
    checkAnswer(day07Test3, checkAmplifier(day07Test3.input.longCodes()))
    checkAnswer(day7Part1Test, checkAmplifier(day7Part1Test.input.longCodes()))

    checkAnswer(day07Test4, runFeedbackAmplifier(day07Test4.input.longCodes(), "98765"))
    checkAnswer(day07Test5, runFeedbackAmplifier(day07Test5.input.longCodes(), "98765"))
    checkAnswer(day7Part2Test, feedBackAmplifiers(day7Part2Test.input.longCodes()))

    checkAnswer(day09Test1, boost(day09Test1.input.longCodes()).instructions.map { t -> t.value.toString() }.joinToString(","))
    checkAnswer(day09Test2, boost(day09Test2.input.longCodes()).print.getOrElse(0) { -99999 }.toString())
    checkAnswer(day09Test3, boost(day09Test3.input.longCodes()).print.getOrElse(0) { -99999 }.toString())

    var instructions = input09.longCodes()
    val boost = boost(instructions)
    println("Boost: $boost")
    println("Boost: ${boost.print}")
}

data class LongCodeProps(val instructions: MutableMap<Long, Long>,
                         val storeInstructions: MutableList<Long>,
                         val executedInstructions: MutableList<String> = mutableListOf(),
                         val debugPrints: MutableList<String> = mutableListOf(),
                         var index: Long = 0,
                         var relative: Long = 0,
                         var modes: List<Long> = listOf(0, 0, 0),
                         val feedbackMode: Boolean = false,
                         var forceExit: Boolean = false,
                         val print: MutableList<Long> = mutableListOf()) {
    fun p1(): Long = this.modes[0]
    fun p2(): Long = this.modes[1]
    fun p3(): Long = this.modes[2]

    fun getCode(i: Long) = instructions.getOrDefault(i, 0)
}

private fun boost(instructions: MutableMap<Long, Long>): LongCodeProps {

    val props = LongCodeProps(instructions, mutableListOf(1))
    runIntCodes(props)

    return props
}

private fun checkAmplifier(instructions: Map<Long, Long>): Long {
    val map = mutableMapOf<String, Long>()
    val uniqueNumber = uniqueNumber()
    uniqueNumber.forEach { amplifierSetting ->
        val output = runAmplifier(instructions, amplifierSetting)
        map[amplifierSetting] = output

    }
    val max = map.values.max()!!
    return max
}

private fun runAmplifier(instructions: Map<Long, Long>, amplifierSetting: String): Long {
    val props1 = LongCodeProps(instructions.toMutableMap(), mutableListOf(amplifierSetting[0].longCode(), 0))
    printer(amplifierSetting, props1)
    runIntCodes(props1)
    val props2 = LongCodeProps(instructions.toMutableMap(), mutableListOf(amplifierSetting[1].longCode(), props1.print.last()))
    runIntCodes(props2)
    val props3 = LongCodeProps(instructions.toMutableMap(), mutableListOf(amplifierSetting[2].longCode(), props2.print.last()))
    runIntCodes(props3)
    val props4 = LongCodeProps(instructions.toMutableMap(), mutableListOf(amplifierSetting[3].longCode(), props3.print.last()))
    runIntCodes(props4)
    val props5 = LongCodeProps(instructions.toMutableMap(), mutableListOf(amplifierSetting[4].longCode(), props4.print.last()))
    runIntCodes(props5)
    printer("$amplifierSetting -> ${props5.print[0]}", props5)
    return props5.print[0]
}

private fun feedBackAmplifiers(instructions: Map<Long, Long>): Long {
    val map = mutableMapOf<String, Long>()
    val uniqueNumber = uniqueNumber(listOf("5", "6", "7", "8", "9"))
    uniqueNumber.forEach { amplifierSetting ->
        val output = runFeedbackAmplifier(instructions, amplifierSetting)
        map[amplifierSetting] = output

    }
    val max = map.values.max()!!
    return max
}

private fun runFeedbackAmplifier(instructions: Map<Long, Long>, amplifierSetting: String): Long {
    val props1 = LongCodeProps(instructions.toMutableMap(), mutableListOf(amplifierSetting[0].longCode(), 0), feedbackMode = true)
    val props2 = LongCodeProps(instructions.toMutableMap(), mutableListOf(amplifierSetting[1].longCode()), feedbackMode = true)
    val props3 = LongCodeProps(instructions.toMutableMap(), mutableListOf(amplifierSetting[2].longCode()), feedbackMode = true)
    val props4 = LongCodeProps(instructions.toMutableMap(), mutableListOf(amplifierSetting[3].longCode()), feedbackMode = true)
    val props5 = LongCodeProps(instructions.toMutableMap(), mutableListOf(amplifierSetting[4].longCode()), feedbackMode = true)
    while (true) {
        printer(amplifierSetting, props1)
        runIntCodes(props1)
        props1.storeInstructions.clear()
        props1.forceExit = false
        props2.storeInstructions.add(props1.print.last())
        runIntCodes(props2)
        props2.storeInstructions.clear()
        props2.forceExit = false
        props3.storeInstructions.add(props2.print.last())
        runIntCodes(props3)
        props3.storeInstructions.clear()
        props3.forceExit = false
        props4.storeInstructions.add(props3.print.last())
        runIntCodes(props4)
        props4.storeInstructions.clear()
        props4.forceExit = false
        props5.storeInstructions.add(props4.print.last())
        runIntCodes(props5)
        if (!props5.forceExit) break
        props5.storeInstructions.clear()
        props5.forceExit = false
        props1.storeInstructions.add(props5.print.last())
        printer("$amplifierSetting -> ${props5.print[0]}", props5)
    }

    return props5.print.last()
}

private fun runIntCodes(props: LongCodeProps): LongCodeProps {
    while (props.index < props.instructions.size) {
        val instruction = props.getCode(props.index)
        props.modes = listOf(0, 0, 0)
        props.index = interpretCode(instruction, props)
        if (props.forceExit) {
            break
        }
    }
    return props
}

fun parameterMode(props: LongCodeProps): Long {
    val index = props.index
    try {
        val parameters = props.instructions[index].toString()
        val parametersWithZero = when (parameters.length) {
            1 -> throw Exception("$parameters should be longer at index: $index")
            2 -> "000$parameters"
            3 -> "00$parameters"
            4 -> "0$parameters"
            else -> parameters
        }

        val firstMode = parametersWithZero[2].longCode()
        val secondMode = parametersWithZero[1].longCode()
        val thirdMode = parametersWithZero[0].longCode()

        props.modes = listOf(firstMode, secondMode, thirdMode)
        val instruction = parametersWithZero.substring(3).toLong()
        return interpretCode(instruction, props, false)
    } catch (e: Exception) {
        throw Exception("Problem with: ${props.instructions[index]}", e)
    }
}

private fun interpretCode(instruction: Long, props: LongCodeProps,
                          checkParameter: Boolean = true): Long {
    printer("instruction: $instruction ", props)
    try {
        return when (instruction) {
            1L -> addInstruction(props)
            2L -> multiplyInstruction(props)
            3L -> storeInstruction(props)
            4L -> printInstruction(props)
            5L -> jumpIfTrueInstruction(props)
            6L -> jumpIfFalseInstruction(props)
            7L -> lessThanInstruction(props)
            8L -> equalsInstruction(props)
            9L -> adjustRelativeBase(props)
            99L -> Long.MAX_VALUE
            else -> if (checkParameter) parameterMode(props) else throw Exception("Unknown parameter: $instruction")
        }
    } catch (e: Exception) {
        printer("instruction: $instruction, props: $props", props)
        throw e
    }
}

fun adjustRelativeBase(props: LongCodeProps): Long {
    val index = props.index
    val instructions = props.instructions
    printer("adjustRelativeBase: i=$index, [${instructions[index + 1]}]", props)
    try {
        val num1 = getValue(props.p1(), index + 1, props)
        props.relative += num1
    } catch (e: Exception) {
        throw e
    }
    return index + 4
}

fun addInstruction(props: LongCodeProps): Long {
    val index = props.index
    val instructions = props.instructions
    printer("add: i=$index, [${instructions[index + 1]},${instructions[index + 2]}, ${instructions[index + 3]}]", props)
    try {
        val num1 = getValue(props.p1(), index + 1, props)
        val num2 = getValue(props.p2(), index + 2, props)
        val destination = props.getCode(index + 3)
        instructions[destination] = num1 + num2
    } catch (e: Exception) {
        throw e
    }
    return index + 4
}

fun multiplyInstruction(props: LongCodeProps): Long {
    val index = props.index

    val instructions = props.instructions
    printer("mul: i=$index, [${instructions[index + 1]},${instructions[index + 2]}, ${instructions[index + 3]}]", props)
    try {
        val num1 = getValue(props.p1(), index + 1, props)
        val num2 = getValue(props.p2(), index + 2, props)
        val destination = props.getCode(index + 3)
        instructions[destination] = num1 * num2
    } catch (e: Exception) {
        throw e
    }
    return index + 4
}

fun storeInstruction(props: LongCodeProps): Long {
    val instructions = props.instructions
    val index = props.index

    printer("store: i=$index, [${instructions[index + 1]}]", props)
    try {
        val destination = props.getCode(index + 1)
        if (props.feedbackMode && props.storeInstructions.isEmpty()) {
            props.forceExit = true
            return props.index
        }
        val test = props.storeInstructions.removeAt(0)
        if (!props.feedbackMode) {
            props.storeInstructions.add(test)
        }
        instructions[destination] = test
    } catch (e: Exception) {
        throw e
    }
    return index + 2
}

fun printInstruction(props: LongCodeProps): Long {
    val instructions = props.instructions
    val index = props.index

    printer("print: i=$index, [${instructions[index + 1]}]", props)
    try {
        val toPrint = getValue(props.p1(), index + 1, props)
        props.print.add(toPrint)
        printer("PrintInstruction: $toPrint", props)
    } catch (e: Exception) {
        throw e
    }
    return 2 + index
}

fun jumpIfTrueInstruction(props: LongCodeProps): Long {
    val instructions = props.instructions
    val index = props.index

    printer("jumpTrue: i=$index, [${instructions[index + 1]},${instructions[index + 2]}]", props)
    try {
        val num1 = getValue(props.p1(), index + 1, props)
        if (num1 != 0L) {
            return getValue(props.p2(), index + 2, props)
        }
        return index + 3
    } catch (e: Exception) {
        throw e
    }
}

fun jumpIfFalseInstruction(props: LongCodeProps): Long {
    val instructions = props.instructions
    val index = props.index

    printer("jumpFalse: i=$index, [${instructions[index + 1]},${instructions[index + 2]}]", props)
    try {
        val num1 = getValue(props.p1(), index + 1, props)
        if (num1 == 0L) {
            return getValue(props.p2(), index + 2, props)
        }
        return index + 3

    } catch (e: Exception) {
        throw e
    }
}

fun lessThanInstruction(props: LongCodeProps): Long {
    val instructions = props.instructions
    val index = props.index

    printer("less: i=$index, [${instructions[index + 1]},${instructions[index + 2]}, ${instructions[index + 3]}]", props)
    try {
        val num1 = getValue(props.p1(), index + 1, props)
        val num2 = getValue(props.p2(), index + 2, props)
        val destination = props.getCode(index + 3)
        if (num1 < num2) {
            instructions[destination] = 1
        } else {
            instructions[destination] = 0
        }
        return index + 4

    } catch (e: Exception) {
        throw e
    }
}

fun equalsInstruction(props: LongCodeProps): Long {
    val instructions = props.instructions
    val index = props.index

    printer("equals: i=$index, [${instructions[index + 1]},${instructions[index + 2]}, ${instructions[index + 3]}]", props)
    try {
        val num1 = getValue(props.p1(), index + 1, props)
        val num2 = getValue(props.p2(), index + 2, props)
        val destination = props.getCode(index + 3)
        if (num1 == num2) {
            instructions[destination] = 1
        } else {
            instructions[destination] = 0
        }
        return index + 4
    } catch (e: Exception) {
        throw e
    }
}

fun getValue(value: Long, index: Long, props: LongCodeProps): Long {
    try {
        val i = props.getCode(index)
        return when (value) {
            0L -> props.getCode(i)
            1L -> i
            2L -> i + 0
            else -> throw Error("Unsupported value: $value")
        }
    } catch (e: Exception) {
        throw e
    }
}
