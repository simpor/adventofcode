package year2019.day07

import year2019.InputWithInt
import year2019.checkAnswer
import year2019.uniqueNumber

fun printer(string: String, props: IntCodeProps) {
    //println(string)
    props.debugPrints.add(string)
}

val day07Test1 = InputWithInt(43210, "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0")
val day07Test2 = InputWithInt(54321, "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0")
val day07Test3 = InputWithInt(65210, "3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0")
val day07Test4 = InputWithInt(139629729, "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5")
val day07Test5 = InputWithInt(18216, "3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10")
val input05 = """
3,225,1,225,6,6,1100,1,238,225,104,0,1101,72,36,225,1101,87,26,225,2,144,13,224,101,-1872,224,224,4,224,102,8,223,223,1001,224,2,224,1,223,224,223,1102,66,61,225,1102,25,49,224,101,-1225,224,224,4,224,1002,223,8,223,1001,224,5,224,1,223,224,223,1101,35,77,224,101,-112,224,224,4,224,102,8,223,223,1001,224,2,224,1,223,224,223,1002,195,30,224,1001,224,-2550,224,4,224,1002,223,8,223,1001,224,1,224,1,224,223,223,1102,30,44,225,1102,24,21,225,1,170,117,224,101,-46,224,224,4,224,1002,223,8,223,101,5,224,224,1,224,223,223,1102,63,26,225,102,74,114,224,1001,224,-3256,224,4,224,102,8,223,223,1001,224,3,224,1,224,223,223,1101,58,22,225,101,13,17,224,101,-100,224,224,4,224,1002,223,8,223,101,6,224,224,1,224,223,223,1101,85,18,225,1001,44,7,224,101,-68,224,224,4,224,102,8,223,223,1001,224,5,224,1,223,224,223,4,223,99,0,0,0,677,0,0,0,0,0,0,0,0,0,0,0,1105,0,99999,1105,227,247,1105,1,99999,1005,227,99999,1005,0,256,1105,1,99999,1106,227,99999,1106,0,265,1105,1,99999,1006,0,99999,1006,227,274,1105,1,99999,1105,1,280,1105,1,99999,1,225,225,225,1101,294,0,0,105,1,0,1105,1,99999,1106,0,300,1105,1,99999,1,225,225,225,1101,314,0,0,106,0,0,1105,1,99999,7,677,226,224,102,2,223,223,1005,224,329,101,1,223,223,8,677,226,224,1002,223,2,223,1005,224,344,1001,223,1,223,1107,677,677,224,102,2,223,223,1005,224,359,1001,223,1,223,1107,226,677,224,102,2,223,223,1005,224,374,101,1,223,223,7,226,677,224,102,2,223,223,1005,224,389,101,1,223,223,8,226,677,224,1002,223,2,223,1005,224,404,101,1,223,223,1008,226,677,224,1002,223,2,223,1005,224,419,1001,223,1,223,107,677,677,224,102,2,223,223,1005,224,434,101,1,223,223,1108,677,226,224,1002,223,2,223,1006,224,449,101,1,223,223,1108,677,677,224,102,2,223,223,1006,224,464,101,1,223,223,1007,677,226,224,102,2,223,223,1006,224,479,101,1,223,223,1008,226,226,224,102,2,223,223,1006,224,494,101,1,223,223,108,226,226,224,1002,223,2,223,1006,224,509,101,1,223,223,107,226,226,224,102,2,223,223,1006,224,524,101,1,223,223,1107,677,226,224,102,2,223,223,1005,224,539,1001,223,1,223,108,226,677,224,1002,223,2,223,1005,224,554,101,1,223,223,1007,226,226,224,102,2,223,223,1005,224,569,101,1,223,223,8,226,226,224,102,2,223,223,1006,224,584,101,1,223,223,1008,677,677,224,1002,223,2,223,1005,224,599,1001,223,1,223,107,226,677,224,1002,223,2,223,1005,224,614,1001,223,1,223,1108,226,677,224,102,2,223,223,1006,224,629,101,1,223,223,7,677,677,224,1002,223,2,223,1005,224,644,1001,223,1,223,108,677,677,224,102,2,223,223,1005,224,659,101,1,223,223,1007,677,677,224,102,2,223,223,1006,224,674,101,1,223,223,4,223,99,226
""".trimIndent()
val input07 = """
3,8,1001,8,10,8,105,1,0,0,21,34,51,76,101,126,207,288,369,450,99999,3,9,102,4,9,9,1001,9,2,9,4,9,99,3,9,1001,9,2,9,1002,9,3,9,101,3,9,9,4,9,99,3,9,102,5,9,9,1001,9,2,9,102,2,9,9,101,3,9,9,1002,9,2,9,4,9,99,3,9,101,5,9,9,102,5,9,9,1001,9,2,9,102,3,9,9,1001,9,3,9,4,9,99,3,9,101,2,9,9,1002,9,5,9,1001,9,5,9,1002,9,4,9,101,5,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,99,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,99
""".trimIndent()


val addTest = InputWithInt(2, "1,0,0,0,99")
val multiplyTest01 = InputWithInt(2, "2,3,0,3,99")
val multiplyTest02 = InputWithInt(2, "2,4,4,5,99,0")
val intCodeTest04 = InputWithInt(30, "1,1,1,4,99,5,6,0,99")
val day5Part1Test = InputWithInt(5577461, input05)
val day5Part2Test = InputWithInt(7161591, input05)
val day7Part1Test = InputWithInt(422858, input07)
val day7Part2Test = InputWithInt(14897241, input07)

fun String.intCodes(): MutableList<Int> {
    return this.split(",").map { it.toInt() }.toMutableList()
}

fun Char.intCode(): Int {
    return this.toString().toInt()
}

fun main() {
    checkAnswer(addTest, runIntCodes(IntCodeProps(addTest.input.intCodes(), mutableListOf(0))).instructions[0])
    checkAnswer(multiplyTest01, runIntCodes(IntCodeProps(multiplyTest01.input.intCodes(), mutableListOf(0))).instructions[0])
    checkAnswer(multiplyTest02, runIntCodes(IntCodeProps(multiplyTest02.input.intCodes(), mutableListOf(0))).instructions[0])
    checkAnswer(intCodeTest04, runIntCodes(IntCodeProps(intCodeTest04.input.intCodes(), mutableListOf(0))).instructions[0])
    checkAnswer(day5Part1Test, runIntCodes(IntCodeProps(day5Part1Test.input.intCodes(), mutableListOf(1))).print.last { it != 0 })
    checkAnswer(day5Part2Test, runIntCodes(IntCodeProps(day5Part2Test.input.intCodes(), mutableListOf(5))).print[0])

    checkAnswer(day07Test1, runAmplifier(day07Test1.input.intCodes(), "43210"))
    checkAnswer(day07Test2, runAmplifier(day07Test2.input.intCodes(), "01234"))
    checkAnswer(day07Test3, runAmplifier(day07Test3.input.intCodes(), "10432"))

    checkAnswer(day07Test1, checkAmplifier(day07Test1.input.intCodes()))
    checkAnswer(day07Test2, checkAmplifier(day07Test2.input.intCodes()))
    checkAnswer(day07Test3, checkAmplifier(day07Test3.input.intCodes()))
    checkAnswer(day7Part1Test, checkAmplifier(day7Part1Test.input.intCodes()))

    checkAnswer(day07Test4, runFeedbackAmplifier(day07Test4.input.intCodes(), "98765"))
    checkAnswer(day07Test5, runFeedbackAmplifier(day07Test5.input.intCodes(), "98765"))
    checkAnswer(day7Part2Test, feedBackAmplifiers(day7Part2Test.input.intCodes()))

    var instructions = input07.intCodes()
    val thruster = feedBackAmplifiers(instructions)
    println("Biggest: $thruster")
}

data class IntCodeProps(val instructions: MutableList<Int>,
                        val storeInstructions: MutableList<Int>,
                        val executedInstructions: MutableList<String> = mutableListOf(),
                        val debugPrints: MutableList<String> = mutableListOf(),
                        var index: Int = 0,
                        var modes: List<Int> = listOf(0, 0, 0),
                        val feedbackMode: Boolean = false,
                        var forceExit: Boolean = false,
                        val print: MutableList<Int> = mutableListOf()) {
    fun p1() : Int = this.modes[0]
    fun p2() : Int = this.modes[1]
    fun p3() : Int = this.modes[2]
}

private fun checkAmplifier(instructions: List<Int>): Int {
    val map = mutableMapOf<String, Int>()
    val uniqueNumber = uniqueNumber()
    uniqueNumber.forEach { amplifierSetting ->
        val output = runAmplifier(instructions, amplifierSetting)
        map[amplifierSetting] = output

    }
    val max = map.values.max()!!
    return max
}

private fun runAmplifier(instructions: List<Int>, amplifierSetting: String): Int {
    val props1 = IntCodeProps(instructions.toMutableList(), mutableListOf(amplifierSetting[0].intCode(), 0))
    printer(amplifierSetting, props1)
    runIntCodes(props1)
    val props2 = IntCodeProps(instructions.toMutableList(), mutableListOf(amplifierSetting[1].intCode(), props1.print.last()))
    runIntCodes(props2)
    val props3 = IntCodeProps(instructions.toMutableList(), mutableListOf(amplifierSetting[2].intCode(), props2.print.last()))
    runIntCodes(props3)
    val props4 = IntCodeProps(instructions.toMutableList(), mutableListOf(amplifierSetting[3].intCode(), props3.print.last()))
    runIntCodes(props4)
    val props5 = IntCodeProps(instructions.toMutableList(), mutableListOf(amplifierSetting[4].intCode(), props4.print.last()))
    runIntCodes(props5)
    printer("$amplifierSetting -> ${props5.print[0]}", props5)
    return props5.print[0]
}

private fun feedBackAmplifiers(instructions: List<Int>): Int {
    val map = mutableMapOf<String, Int>()
    val uniqueNumber = uniqueNumber(listOf("5", "6", "7", "8", "9"))
    uniqueNumber.forEach { amplifierSetting ->
        val output = runFeedbackAmplifier(instructions, amplifierSetting)
        map[amplifierSetting] = output

    }
    val max = map.values.max()!!
    return max
}

private fun runFeedbackAmplifier(instructions: List<Int>, amplifierSetting: String): Int {
    val props1 = IntCodeProps(instructions.toMutableList(), mutableListOf(amplifierSetting[0].intCode(), 0), feedbackMode = true)
    val props2 = IntCodeProps(instructions.toMutableList(), mutableListOf(amplifierSetting[1].intCode()), feedbackMode = true)
    val props3 = IntCodeProps(instructions.toMutableList(), mutableListOf(amplifierSetting[2].intCode()), feedbackMode = true)
    val props4 = IntCodeProps(instructions.toMutableList(), mutableListOf(amplifierSetting[3].intCode()), feedbackMode = true)
    val props5 = IntCodeProps(instructions.toMutableList(), mutableListOf(amplifierSetting[4].intCode()), feedbackMode = true)
    while (true) {
        printer(amplifierSetting, props1)
        runIntCodes(props1)
        props1.storeInstructions.clear()
        props1.forceExit = false
        props2.storeInstructions.add(props1.print.last())
        runIntCodes(props2)
        props2.storeInstructions.clear()
        props2.forceExit = false
        props3.storeInstructions.add(props2.print.last())
        runIntCodes(props3)
        props3.storeInstructions.clear()
        props3.forceExit = false
        props4.storeInstructions.add(props3.print.last())
        runIntCodes(props4)
        props4.storeInstructions.clear()
        props4.forceExit = false
        props5.storeInstructions.add(props4.print.last())
        runIntCodes(props5)
        if (!props5.forceExit) break
        props5.storeInstructions.clear()
        props5.forceExit = false
        props1.storeInstructions.add(props5.print.last())
        printer("$amplifierSetting -> ${props5.print[0]}", props5)
    }

    return props5.print.last()
}

private fun runIntCodes(props: IntCodeProps): IntCodeProps {
    while (props.index < props.instructions.size) {
        val instruction = props.instructions[props.index]
        props.modes = listOf(0, 0, 0)
        props.index = interpretCode(instruction, props)
        if (props.forceExit) {
            break
        }
    }
    return props
}

fun parameterMode(props: IntCodeProps): Int {
    val index = props.index
    try {
        val parameters = props.instructions[index].toString()
        val parametersWithZero = when (parameters.length) {
            1 -> throw Exception("$parameters should be longer at index: $index")
            2 -> "000$parameters"
            3 -> "00$parameters"
            4 -> "0$parameters"
            else -> parameters
        }

        val firstMode = parametersWithZero[2].intCode()
        val secondMode = parametersWithZero[1].intCode()
        val thirdMode = parametersWithZero[0].intCode()

        props.modes = listOf(firstMode, secondMode, thirdMode)
        val instruction = parametersWithZero.substring(3).toInt()
        return interpretCode(instruction, props, false)
    } catch (e: Exception) {
        throw Exception("Problem with: ${props.instructions[index]}", e)
    }
}

private fun interpretCode(instruction: Int, props: IntCodeProps,
                          checkParameter: Boolean = true): Int {
    printer("instruction: $instruction ", props)
    try {
        return when (instruction) {
            1 -> addInstruction(props)
            2 -> multiplyInstruction(props)
            3 -> storeInstruction(props)
            4 -> printInstruction(props)
            5 -> jumpIfTrueInstruction(props)
            6 -> jumpIfFalseInstruction(props)
            7 -> lessThanInstruction(props)
            8 -> equalsInstruction(props)
            99 -> Int.MAX_VALUE
            else -> if (checkParameter) parameterMode(props) else throw Exception("Unknown parameter: $instruction")
        }
    } catch (e: Exception) {
        printer("instruction: $instruction, props: $props", props)
        throw e
    }
}

fun addInstruction(props: IntCodeProps): Int {
    val index = props.index
    val instructions = props.instructions
    printer("add: i=$index, [${instructions[index + 1]},${instructions[index + 2]}, ${instructions[index + 3]}]", props)
    try {
        val num1 = getValue(props.p1(), index + 1, instructions)
        val num2 = getValue(props.p2(), index + 2, instructions)
        val destination = instructions[index + 3]
        instructions[destination] = num1 + num2
    } catch (e: Exception) {
        throw e
    }
    return index + 4
}

fun multiplyInstruction(props: IntCodeProps): Int {
    val index = props.index

    val instructions = props.instructions
    printer("mul: i=$index, [${instructions[index + 1]},${instructions[index + 2]}, ${instructions[index + 3]}]", props)
    try {
        val num1 = getValue(props.p1(), index + 1, instructions)
        val num2 = getValue(props.p2(), index + 2, instructions)
        val destination = instructions[index + 3]
        instructions[destination] = num1 * num2
    } catch (e: Exception) {
        throw e
    }
    return index + 4
}

fun storeInstruction(props: IntCodeProps): Int {
    val instructions = props.instructions
    val index = props.index

    printer("store: i=$index, [${instructions[index + 1]}]", props)
    try {
        val destination = instructions[index + 1]
        if (props.feedbackMode && props.storeInstructions.isEmpty()) {
            props.forceExit = true
            return props.index
        }
        val test = props.storeInstructions.removeAt(0)
        if (!props.feedbackMode) {
            props.storeInstructions.add(test)
        }
        instructions[destination] = test
    } catch (e: Exception) {
        throw e
    }
    return index + 2
}

fun printInstruction(props: IntCodeProps): Int {
    val instructions = props.instructions
    val index = props.index

    printer("print: i=$index, [${instructions[index + 1]}]", props)
    try {
        val toPrint = instructions[instructions[index + 1]]
        props.print.add(toPrint)
        printer("PrintInstruction: $toPrint", props)
    } catch (e: Exception) {
        throw e
    }
    return 2 + index
}

fun jumpIfTrueInstruction(props: IntCodeProps): Int {
    val instructions = props.instructions
    val index = props.index

    printer("jumpTrue: i=$index, [${instructions[index + 1]},${instructions[index + 2]}]", props)
    try {
        val num1 = getValue(props.p1(), index + 1, instructions)
        if (num1 != 0) {
            return getValue(props.p2(), index + 2, instructions)
        }
        return index + 3
    } catch (e: Exception) {
        throw e
    }
}

fun jumpIfFalseInstruction(props: IntCodeProps): Int {
    val instructions = props.instructions
    val index = props.index

    printer("jumpFalse: i=$index, [${instructions[index + 1]},${instructions[index + 2]}]", props)
    try {
        val num1 = getValue(props.p1(), index + 1, instructions)
        if (num1 == 0) {
            return getValue(props.p2(), index + 2, instructions)
        }
        return index + 3

    } catch (e: Exception) {
        throw e
    }
}

fun lessThanInstruction(props: IntCodeProps): Int {
    val instructions = props.instructions
    val index = props.index

    printer("less: i=$index, [${instructions[index + 1]},${instructions[index + 2]}, ${instructions[index + 3]}]", props)
    try {
        val num1 = getValue(props.p1(), index + 1, instructions)
        val num2 = getValue(props.p2(), index + 2, instructions)
        val destination = instructions[index + 3]
        if (num1 < num2) {
            instructions[destination] = 1
        } else {
            instructions[destination] = 0
        }
        return index + 4

    } catch (e: Exception) {
        throw e
    }
}

fun equalsInstruction(props: IntCodeProps): Int {
    val instructions = props.instructions
    val index = props.index

    printer("equals: i=$index, [${instructions[index + 1]},${instructions[index + 2]}, ${instructions[index + 3]}]", props)
    try {
        val num1 = getValue(props.p1(), index + 1, instructions)
        val num2 = getValue(props.p2(), index + 2, instructions)
        val destination = instructions[index + 3]
        if (num1 == num2) {
            instructions[destination] = 1
        } else {
            instructions[destination] = 0
        }
        return index + 4
    } catch (e: Exception) {
        throw e
    }
}

fun getValue(value: Int, index: Int, instructions: List<Int>): Int {
    try {
        val i = instructions[index]
        return when (value) {
            0 -> instructions[i]
            1 -> i
            else -> throw Error("Unsupported value: $value")
        }
    } catch (e: Exception) {
        throw e
    }
}
