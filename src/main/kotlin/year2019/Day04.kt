package year2019

fun main() {
    var list = mutableListOf<String>()
    // 125730-579381
    for (number in 125730..579381) {
        if (validPassword(number.toString())) list.add(number.toString())
    }
    println(list)
    println(list.size)
    println("PART 2")

    println("Text 112233 = ${validPasswordPart2("112233")}")
    println("Text 123444 = ${validPasswordPart2("123444")}")
    println("Text 111122 = ${validPasswordPart2("111122")}")
    println("Text 336666 = ${validPasswordPart2("336666")}")
    var list2 = mutableListOf<String>()

    for (number in 125730..579381) {
        if (validPasswordPart2(number.toString())) list2.add(number.toString())
    }
    println(list2)
    println(list2.size)

    println("diff")
    println(list.minus(list2))
}

/**
 * It is a six-digit number.
 * The value is within the range given in your puzzle input.
 * Two adjacent digits are the same (like 22 in 122345).
 * Going from left to right, the digits never decrease;
 * they only ever increase or stay the same (like 111123 or 135679).
 */
fun validPassword(password: String): Boolean {
    if (password.length != 6) return false
    var adjacent = false
    for (i in 1..5) {
        if (password[i] == password[i - 1]) adjacent = true
        if (password[i] < password[i - 1]) return false
    }
    return adjacent
}

/**
 * An Elf just remembered one more important detail: the two adjacent matching digits are not part of a larger group of matching digits.
 *
 * Given this additional criterion, but still ignoring the range rule, the following are now true:
 *
 * 112233 meets these criteria because the digits never decrease and all repeated digits are exactly two digits long.
 * 123444 no longer meets the criteria (the repeated 44 is part of a larger group of 444).
 * 111122 meets the criteria (even though 1 is repeated more than twice, it still contains a double 22).
 */
fun validPasswordPart2(password: String): Boolean {
    if (password.length != 6) return false
    val map = mutableMapOf<Int, Int>()
    map[password[0].toInt()] = 1
    for (i in 1..5) {
        if (password[i] < password[i - 1]) return false

        val num = password[i].toInt()
        map[num] = map.getOrDefault(num, 0) + 1;
    }

    val size = map.filterValues { it == 2 }.size
    return size > 0
}

