package year2019

/*
--- Day 5: Sunny with a Chance of Asteroids ---
You're starting to sweat as the ship makes its way toward Mercury. The Elves suggest that you get the air
conditioner working by upgrading your ship computer to support the Thermal Environment Supervision Terminal.

The Thermal Environment Supervision Terminal (TEST) starts by running a diagnostic program (your puzzle input).
The TEST diagnostic program will run on your existing Intcode computer after a few modifications:

First, you'll need to add two new instructions:

Opcode 3 takes a single integer as input and saves it to the address given by its only parameter.
For example, the instruction 3,50 would take an input value and store it at address 50.
Opcode 4 outputs the value of its only parameter. For example, the instruction 4,50 would output the value at address 50.
Programs that use these instructions will come with documentation that explains what should be connected to the input and output.
The program 3,0,4,0,99 outputs whatever it gets as input, then halts.

Second, you'll need to add support for parameter modes:

Each parameter of an instruction is handled based on its parameter mode. Right now, your ship computer already
understands parameter mode 0, position mode, which causes the parameter to be interpreted as a position - if
the parameter is 50, its value is the value stored at address 50 in memory. Until now, all parameters have been in position mode.

Now, your ship computer will also need to handle parameters in mode 1, immediate mode. In immediate mode, a parameter
is interpreted as a value - if the parameter is 50, its value is simply 50.

Parameter modes are stored in the same value as the instruction's opcode. The opcode is a two-digit number
based only on the ones and tens digit of the value, that is, the opcode is the rightmost two digits of the first
value in an instruction.
Parameter modes are single digits, one per parameter, read right-to-left from the opcode:
the first parameter's mode is in the hundreds digit,
the second parameter's mode is in the thousands digit,
the third parameter's mode is in the ten-thousands digit, and so on. Any missing modes are 0.

For example, consider the program 1002,4,3,4,33.

The first instruction, 1002,4,3,4, is a multiply instruction -
the rightmost two digits of the first value, 02, indicate opcode 2, multiplication.
Then, going right to left, the parameter modes are 0 (hundreds digit), 1 (thousands digit), and 0 (ten-thousands digit, not present and therefore zero):

ABCDE
1002

DE - two-digit opcode,      02 == opcode 2
C - mode of 1st parameter,  0 == position mode
B - mode of 2nd parameter,  1 == immediate mode
A - mode of 3rd parameter,  0 == position mode,
omitted due to being a leading zero
This instruction multiplies its first two parameters. The first parameter,
4 in position mode, works like it did before - its value is the value stored at address 4 (33). The second parameter,
3 in immediate mode, simply has value 3. The result of this operation, 33 * 3 = 99, is written according to the third parameter,
4 in position mode, which also works like it did before - 99 is written to address 4.

Parameters that an instruction writes to will never be in immediate mode.

Finally, some notes:

It is important to remember that the instruction pointer should increase by the number of values in the instruction
after the instruction finishes. Because of the new instructions, this amount is no longer always 4.
Integers can be negative: 1101,100,-1,4,0 is a valid program (find 100 + -1, store the result in position 4).
The TEST diagnostic program will start by requesting from the user the ID of the system to test by running an
input instruction - provide it 1, the ID for the ship's air conditioner unit.

It will then perform a series of diagnostic tests confirming that various parts of the Intcode computer,
like parameter modes, function correctly. For each test, it will run an output instruction indicating how far
the result of the test was from the expected value, where 0 means the test was successful. Non-zero outputs mean
that a function is not working correctly; check the instructions that were run before the output instruction to
see which one failed.

Finally, the program will output a diagnostic code and immediately halt. This final output isn't an error;
 an output followed immediately by a halt means the program finished.
 If all outputs were zero except the diagnostic code, the diagnostic program ran successfully.

After providing 1 to the only input instruction and passing all the tests, what diagnostic code does the program produce?
*/


val intCodeTest01 = InputWithInt(2, "1,0,0,0,99")
val intCodeTest02 = InputWithInt(2, "2,3,0,3,99")
val intCodeTest03 = InputWithInt(2, "2,4,4,5,99,0")
val intCodeTest04 = InputWithInt(30, "1,1,1,4,99,5,6,0,99")

fun printer(string: String) {
    println(string)
}

fun main() {

//    checkAnswer(intCodeTest01, runIntCodes(intCodeTest01.input.split(",").toMutableList()).get(0).toInt())
//    checkAnswer(intCodeTest02, runIntCodes(intCodeTest02.input.split(",").toMutableList()).get(0).toInt())
//    checkAnswer(intCodeTest03, runIntCodes(intCodeTest03.input.split(",").toMutableList()).get(0).toInt())
//    checkAnswer(intCodeTest04, runIntCodes(intCodeTest04.input.split(",").toMutableList()).get(0).toInt())

    var instructions = input05.split(",").toMutableList()
    runIntCodes(instructions)

}

fun runIntCodes(instructions: MutableList<String>): MutableList<String> {
    var index = 0

    while (index < instructions.size) {
        val instruction = instructions[index].toInt()
        index = interpretCode(instruction, index, instructions, 0, 0, 0)
    }
    return instructions
}

fun parameterMode(index: Int, instructions: MutableList<String>): Int {

    try {
        var parameters = instructions[index]
        parameters = when (parameters.length) {
            1 -> throw Exception("$parameters should be longer at index: $index")
            2 -> "000$parameters"
            3 -> "00$parameters"
            4 -> "0$parameters"
            else -> parameters
        }

        val firstMode = parameters[2].toString().toInt()
        val secondMode = parameters[1].toString().toInt()
        val thirdMode = parameters[0].toString().toInt()

        val instruction = parameters.substring(3).toInt()
        val newIndex = interpretCode(instruction, index, instructions, firstMode, secondMode, thirdMode, false)
        return newIndex
    } catch (e: Exception) {
        throw Exception("Problem with: ${instructions[index]}", e)
    }
}

private fun interpretCode(instruction: Int, index: Int, instructions: MutableList<String>,
                          firstMode: Int, secondMode: Int, thirdMode: Int, checkParameter: Boolean = true): Int {
    printer("instruction: $instruction ")
    return when (instruction) {
        1 -> addInstruction(index, instructions, firstMode, secondMode, thirdMode)
        2 -> multiplyInstruction(index, instructions, firstMode, secondMode, thirdMode)
        3 -> storeInstruction(index, instructions)
        4 -> printInstruction(index, instructions)
        5 -> jumpIfTrueInstruction(index, instructions, firstMode, secondMode)
        6 -> jumpIfFalseInstruction(index, instructions, firstMode, secondMode)
        7 -> lessThanInstruction(index, instructions, firstMode, secondMode)
        8 -> equalsInstruction(index, instructions, firstMode, secondMode)
        99 -> instructions.size
        else -> if (checkParameter) parameterMode(index, instructions) else throw Exception("Unknown parameter: $instruction")
    }
}

fun addInstruction(index: Int, instructions: MutableList<String>, first: Int = 0, second: Int = 0, third: Int = 0): Int {
    var modifiedIndex = false
    printer("add: i=$index, [${instructions[index + 1]},${instructions[index + 2]}, ${instructions[index + 3]}]")
    try {
        val num1 = getValue(first, instructions[index + 1], instructions)
        val num2 = getValue(second, instructions[index + 2], instructions)
        val destination = getValue(1, instructions[index + 3], instructions)
        if (destination == index) modifiedIndex = true
        instructions[destination] = (num1 + num2).toString()
    } catch (e: Exception) {
        throw e
    }
    return if (modifiedIndex) instructions[index].toInt() else index + 4

}

fun multiplyInstruction(index: Int, instructions: MutableList<String>, first: Int = 0, second: Int = 0, third: Int = 0): Int {
    var modifiedIndex = false
    printer("mul: i=$index, [${instructions[index + 1]},${instructions[index + 2]}, ${instructions[index + 3]}]")
    try {
        val num1 = getValue(first, instructions[index + 1], instructions)
        val num2 = getValue(second, instructions[index + 2], instructions)
        val destination = getValue(1, instructions[index + 3], instructions)
        if (destination == index) modifiedIndex = true
        instructions[destination] = (num1 * num2).toString()
    } catch (e: Exception) {
        throw e
    }
    return if (modifiedIndex) instructions[index].toInt() else index + 4

}

fun storeInstruction(index: Int, instructions: MutableList<String>): Int {
    printer("store: i=$index, [${instructions[index + 1]}]")
    var modifiedIndex = false
    try {
        val destination = instructions[index + 1].toInt()
        if (destination == index) modifiedIndex = true
        instructions[destination] = "5"
    } catch (e: Exception) {
        throw e
    }
    return if (modifiedIndex) instructions[index].toInt() else index + 2

}

fun printInstruction(index: Int, instructions: MutableList<String>): Int {
    printer("print: i=$index, [${instructions[index + 1]}]")
    try {
        val toPrint = instructions[instructions[index + 1].toInt()]
        println("PrintInstruction: $toPrint")
    } catch (e: Exception) {
        throw e
    }
    return 2 + index
}

fun jumpIfTrueInstruction(index: Int, instructions: MutableList<String>, first: Int = 0, second: Int = 0): Int {
    printer("jumpTrue: i=$index, [${instructions[index + 1]},${instructions[index + 2]}]")
    var modifiedIndex = false
    try {
        val num1 = getValue(first, instructions[index + 1], instructions)
        if (num1 != 0) {
            instructions[index] = getValue(second, instructions[index + 2], instructions).toString()
            modifiedIndex = true
        }
        return if (modifiedIndex) instructions[index].toInt() else index + 3
    } catch (e: Exception) {
        throw e
    }
}

fun jumpIfFalseInstruction(index: Int, instructions: MutableList<String>, first: Int = 0, second: Int = 0): Int {
    printer("jumpFalse: i=$index, [${instructions[index + 1]},${instructions[index + 2]}]")
    var modifiedIndex = false
    try {
        val num1 = getValue(first, instructions[index + 1], instructions)
        if (num1 == 0) {
            instructions[index] = getValue(second, instructions[index + 2], instructions).toString()
            modifiedIndex = true
        }
        return if (modifiedIndex) instructions[index].toInt() else index + 3

    } catch (e: Exception) {
        throw e
    }
}

fun lessThanInstruction(index: Int, instructions: MutableList<String>, first: Int = 0, second: Int = 0): Int {
    printer("less: i=$index, [${instructions[index + 1]},${instructions[index + 2]}, ${instructions[index + 3]}]")
    var modifiedIndex = false
    try {
        val num1 = getValue(first, instructions[index + 1], instructions)
        val num2 = getValue(second, instructions[index + 2], instructions)
        val destination = getValue(1, instructions[index + 3], instructions)
        if (destination == index) modifiedIndex = true
        if (num1 < num2) {
            instructions[destination] = "1"
        } else {
            instructions[destination] = "0"
        }
        return if (modifiedIndex) instructions[index].toInt() else index + 4

    } catch (e: Exception) {
        throw e
    }
}

fun equalsInstruction(index: Int, instructions: MutableList<String>, first: Int = 0, second: Int = 0): Int {
    printer("equals: i=$index, [${instructions[index + 1]},${instructions[index + 2]}, ${instructions[index + 3]}]")
    var modifiedIndex = false
    try {
        val num1 = getValue(first, instructions[index + 1], instructions)
        val num2 = getValue(second, instructions[index + 2], instructions)
        val destination = getValue(1, instructions[index + 3], instructions)
        if (destination == index) modifiedIndex = true
        if (num1 == num2) {
            instructions[destination] = "1"
        } else {
            instructions[destination] = "0"
        }
        return if (modifiedIndex) instructions[index].toInt() else index + 4
    } catch (e: Exception) {
        throw e
    }
}

fun getValue(value: Int, instruction: String, instructions: List<String>): Int {
    try {
        return when (value) {
            0 -> instructions[instruction.toInt()].toInt()
            1 -> instruction.toInt()
            else -> throw Error("Unsupported value: $value")
        }
    } catch (e: Exception) {
        throw e
    }
}


val input05 = """
3,225,1,225,6,6,1100,1,238,225,104,0,1101,72,36,225,1101,87,26,225,2,144,13,224,101,-1872,224,224,4,224,102,8,223,223,1001,224,2,224,1,223,224,223,1102,66,61,225,1102,25,49,224,101,-1225,224,224,4,224,1002,223,8,223,1001,224,5,224,1,223,224,223,1101,35,77,224,101,-112,224,224,4,224,102,8,223,223,1001,224,2,224,1,223,224,223,1002,195,30,224,1001,224,-2550,224,4,224,1002,223,8,223,1001,224,1,224,1,224,223,223,1102,30,44,225,1102,24,21,225,1,170,117,224,101,-46,224,224,4,224,1002,223,8,223,101,5,224,224,1,224,223,223,1102,63,26,225,102,74,114,224,1001,224,-3256,224,4,224,102,8,223,223,1001,224,3,224,1,224,223,223,1101,58,22,225,101,13,17,224,101,-100,224,224,4,224,1002,223,8,223,101,6,224,224,1,224,223,223,1101,85,18,225,1001,44,7,224,101,-68,224,224,4,224,102,8,223,223,1001,224,5,224,1,223,224,223,4,223,99,0,0,0,677,0,0,0,0,0,0,0,0,0,0,0,1105,0,99999,1105,227,247,1105,1,99999,1005,227,99999,1005,0,256,1105,1,99999,1106,227,99999,1106,0,265,1105,1,99999,1006,0,99999,1006,227,274,1105,1,99999,1105,1,280,1105,1,99999,1,225,225,225,1101,294,0,0,105,1,0,1105,1,99999,1106,0,300,1105,1,99999,1,225,225,225,1101,314,0,0,106,0,0,1105,1,99999,7,677,226,224,102,2,223,223,1005,224,329,101,1,223,223,8,677,226,224,1002,223,2,223,1005,224,344,1001,223,1,223,1107,677,677,224,102,2,223,223,1005,224,359,1001,223,1,223,1107,226,677,224,102,2,223,223,1005,224,374,101,1,223,223,7,226,677,224,102,2,223,223,1005,224,389,101,1,223,223,8,226,677,224,1002,223,2,223,1005,224,404,101,1,223,223,1008,226,677,224,1002,223,2,223,1005,224,419,1001,223,1,223,107,677,677,224,102,2,223,223,1005,224,434,101,1,223,223,1108,677,226,224,1002,223,2,223,1006,224,449,101,1,223,223,1108,677,677,224,102,2,223,223,1006,224,464,101,1,223,223,1007,677,226,224,102,2,223,223,1006,224,479,101,1,223,223,1008,226,226,224,102,2,223,223,1006,224,494,101,1,223,223,108,226,226,224,1002,223,2,223,1006,224,509,101,1,223,223,107,226,226,224,102,2,223,223,1006,224,524,101,1,223,223,1107,677,226,224,102,2,223,223,1005,224,539,1001,223,1,223,108,226,677,224,1002,223,2,223,1005,224,554,101,1,223,223,1007,226,226,224,102,2,223,223,1005,224,569,101,1,223,223,8,226,226,224,102,2,223,223,1006,224,584,101,1,223,223,1008,677,677,224,1002,223,2,223,1005,224,599,1001,223,1,223,107,226,677,224,1002,223,2,223,1005,224,614,1001,223,1,223,1108,226,677,224,102,2,223,223,1006,224,629,101,1,223,223,7,677,677,224,1002,223,2,223,1005,224,644,1001,223,1,223,108,677,677,224,102,2,223,223,1005,224,659,101,1,223,223,1007,677,677,224,102,2,223,223,1006,224,674,101,1,223,223,4,223,99,226
""".trimIndent()

