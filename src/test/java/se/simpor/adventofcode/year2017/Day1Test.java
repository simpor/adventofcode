package se.simpor.adventofcode.year2017;

import org.junit.jupiter.api.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;

public class Day1Test {

    @Test
    public void test1(){
        String problem = "1122";
        int answer = 3;
        int actual = new Day1Java().solve1(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
    @Test
    public void test2(){
        String problem = "1111";
        int answer = 4;
        int actual = new Day1Java().solve1(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
    @Test
    public void test3(){
        String problem = "1234";
        int answer = 0;
        int actual = new Day1Java().solve1(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
    @Test
    public void test4(){
        String problem = "91212129";
        int answer = 9;
        int actual = new Day1Java().solve1(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
    @Test
    public void test5(){
        String problem = "1212";
        int answer = 6;
        int actual = new Day1Java().solve2(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
    @Test
    public void test6(){
        String problem = "1221";
        int answer = 0;
        int actual = new Day1Java().solve2(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
    @Test
    public void test7(){
        String problem = "123425";
        int answer = 4;
        int actual = new Day1Java().solve2(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
    @Test
    public void test8(){
        String problem = "123123";
        int answer = 12;
        int actual = new Day1Java().solve2(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
    @Test
    public void test9(){
        String problem = "12131415";
        int answer = 4;
        int actual = new Day1Java().solve2(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
}
