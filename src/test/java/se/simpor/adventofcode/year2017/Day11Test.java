package se.simpor.adventofcode.year2017;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class Day11Test {

    @Test
    public void test1(){
        String problem = "ne,ne,ne";
        int answer = 3;
        int actual = new Day11Java().solve1(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
    @Test
    public void test2(){
        String problem = "ne,ne,sw,sw";
        int answer = 0;
        int actual = new Day11Java().solve1(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
    @Test
    public void test3(){
        String problem = "ne,ne,s,s";
        int answer = 2;
        int actual = new Day11Java().solve1(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
    @Test
    public void test4(){
        String problem = "se,sw,se,sw,sw";
        int answer = 5;
        int actual = new Day11Java().solve1(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
}
