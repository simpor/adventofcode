package se.simpor.adventofcode.year2017;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class Day9Test {

    @Test
    public void test1(){
        String problem = "{}";
        int answer = 1;
        int actual = new Day9Java().solve1(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
    @Test
    public void test2(){
        String problem = "{{{}}}";
        int answer = 6;
        int actual = new Day9Java().solve1(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
    @Test
    public void test3(){
        String problem = "{{},{}}";
        int answer = 5;
        int actual = new Day9Java().solve1(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
    @Test
    public void test5(){
        String problem = "{{{},{},{{}}}}";
        int answer = 16;
        int actual = new Day9Java().solve1(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
    @Test
    public void test6(){
        String problem = "{<a>,<a>,<a>,<a>}";
        int answer = 1;
        int actual = new Day9Java().solve1(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
    @Test
    public void test7(){
        String problem = "{{<ab>},{<ab>},{<ab>},{<ab>}}";
        int answer = 9;
        int actual = new Day9Java().solve1(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
    @Test
    public void test8(){
        String problem = "{{<!!>},{<!!>},{<!!>},{<!!>}}";
        int answer = 9;
        int actual = new Day9Java().solve1(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
    @Test
    public void test9(){
        String problem = "{{<a!>},{<a!>},{<a!>},{<ab>}}";
        int answer = 3;
        int actual = new Day9Java().solve1(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
}
