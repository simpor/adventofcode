package se.simpor.adventofcode.year2017;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class Day3Test {

    @Test
    public void test1(){
        int problem = 1;
        int answer = 0;
        int actual = new Day3Java().solve1(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }

    @Test
    public void test2(){
        int problem = 12;
        int answer = 3;
        int actual = new Day3Java().solve1(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }

    @Test
    public void test3(){
        int problem = 23;
        int answer = 2;
        int actual = new Day3Java().solve1(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }

    @Test
    public void test4(){
        int problem = 1024;
        int answer = 31;
        int actual = new Day3Java().solve1(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
}
