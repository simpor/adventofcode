package se.simpor.adventofcode.year2017;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

public class Day20Test {

    Day21Java.PixelPattern pixelPattern1;
    Day21Java.PixelPattern pixelPattern2;
    Day21Java.PixelPattern pixelPattern3;
    Day21Java.PixelPattern pixelPattern4;
    Day21Java.PixelPattern pixelPattern5;


    @BeforeEach
    public void before() {
        pixelPattern1 = new Day21Java.PixelPattern();
        pixelPattern1.size = 3;
        pixelPattern1.rows.add(".#.");
        pixelPattern1.rows.add("..#");
        pixelPattern1.rows.add("###");
        pixelPattern2 = new Day21Java.PixelPattern();
        pixelPattern2.size = 3;
        pixelPattern2.rows.add(".#.");
        pixelPattern2.rows.add("#..");
        pixelPattern2.rows.add("###");
        pixelPattern3 = new Day21Java.PixelPattern();
        pixelPattern3.size = 3;
        pixelPattern3.rows.add("#..");
        pixelPattern3.rows.add("#.#");
        pixelPattern3.rows.add("##.");
        pixelPattern4 = new Day21Java.PixelPattern();
        pixelPattern4.size = 3;
        pixelPattern4.rows.add("###");
        pixelPattern4.rows.add("..#");
        pixelPattern4.rows.add(".#.");
        pixelPattern5 = new Day21Java.PixelPattern();
        pixelPattern5.size = 3;
        pixelPattern5.rows.add("###");
        pixelPattern5.rows.add("#..");
        pixelPattern5.rows.add(".#.");

    }

    @Test
    public void mapping() {
        Map<Day21Java.PixelPattern, Day21Java.PixelPattern> rules = new HashMap<>();

        rules.put(pixelPattern1, pixelPattern2);
        rules.put(pixelPattern2, pixelPattern3);
        rules.put(pixelPattern3, pixelPattern4);
        rules.put(pixelPattern4, pixelPattern5);
        rules.put(pixelPattern5, pixelPattern1);

        assertThat(rules.get(pixelPattern1), is(pixelPattern2));
        assertThat(rules.get(pixelPattern2), is(pixelPattern3));
        assertThat(rules.get(pixelPattern3), is(pixelPattern4));
        assertThat(rules.get(pixelPattern4), is(pixelPattern5));
        assertThat(rules.get(pixelPattern5), is(pixelPattern1));
        assertThat(rules.get(pixelPattern2), not(pixelPattern1));
    }

    @Test
    public void test_rotate_1() {
        assertThat(pixelPattern1.rotate().equal(pixelPattern3), is(true));
    }

    @Test
    public void test_rotate_1_1() {
        assertThat(pixelPattern3.rotate().equal(pixelPattern5), is(true));
    }

    @Test
    public void test_rotate_2() {
        assertThat(pixelPattern1.rotate().rotate().equal(pixelPattern5), is(true));
    }


    @Test
    public void test_1_1() {
        assertThat(pixelPattern1.match(pixelPattern1), is(true));
    }

    @Test
    public void test_2_2() {
        assertThat(pixelPattern2.match(pixelPattern2), is(true));
    }

    @Test
    public void test_3_3() {
        assertThat(pixelPattern3.match(pixelPattern3), is(true));
    }

    @Test
    public void test_4_4() {
        assertThat(pixelPattern4.match(pixelPattern4), is(true));
    }

    @Test
    public void test_1_2() {
        assertThat(pixelPattern1.match(pixelPattern2), is(true));
    }

    @Test
    public void test_1_3() {
        assertThat(pixelPattern1.match(pixelPattern3), is(true));
    }

    @Test
    public void test_1_4() {
        assertThat(pixelPattern1.match(pixelPattern4), is(true));
    }

    @Test
    public void test_2_1() {
        assertThat(pixelPattern2.match(pixelPattern1), is(true));
    }

    @Test
    public void test_2_3() {
        assertThat(pixelPattern2.match(pixelPattern3), is(true));
    }

    @Test
    public void test_2_4() {
        assertThat(pixelPattern2.match(pixelPattern4), is(true));
    }

    @Test
    public void test_3_1() {
        assertThat(pixelPattern3.match(pixelPattern1), is(true));
    }

    @Test
    public void test_3_2() {
        assertThat(pixelPattern3.match(pixelPattern2), is(true));
    }

    @Test
    public void test_3_4() {
        assertThat(pixelPattern3.match(pixelPattern4), is(true));
    }

    @Test
    public void test_4_1() {
        assertThat(pixelPattern4.match(pixelPattern1), is(true));
    }

    @Test
    public void test_4_2() {
        assertThat(pixelPattern4.match(pixelPattern2), is(true));
    }

    @Test
    public void test_4_3() {
        assertThat(pixelPattern4.match(pixelPattern3), is(true));
    }


}
