package se.simpor.adventofcode.year2017;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class Day10Test {

    @Test
    public void test1(){
        String problem = "";
        String answer = "a2582a3a0e66e6e86e3812dcb672a272";
        String actual = new Day10Java().solve2(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
    @Test
    public void test2(){
        String problem = "AoC 2017";
        String answer = "33efeb34ea91902bb2f59c9920caa6cd";
        String actual = new Day10Java().solve2(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
    @Test
    public void test3(){
        String problem = "1,2,3";
        String answer = "3efbe78a8d82f29979031a4aa0b16a9d";
        String actual = new Day10Java().solve2(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
    @Test
    public void test4(){
        String problem = "1,2,4";
        String answer = "63960835bcdc130f0b66d7ff4f6a5a8e";
        String actual = new Day10Java().solve2(problem);
        System.out.println(String.format("%s -> %s", problem, actual));
        assertThat(actual, is(answer));
    }
}
