package year2019

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import year2019.intCode.*

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class IntCodeTests {
    @Nested
    inner class Day02 {
        private val taskInput = "1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,6,1,19,2,19,9,23,1,23,5,27,2,6,27,31,1,31,5,35,1,35,5,39,2,39,6,43,2,43,10,47,1,47,6,51,1,51,6,55,2,55,6,59,1,10,59,63,1,5,63,67,2,10,67,71,1,6,71,75,1,5,75,79,1,10,79,83,2,83,10,87,1,87,9,91,1,91,10,95,2,6,95,99,1,5,99,103,1,103,13,107,1,107,10,111,2,9,111,115,1,115,6,119,2,13,119,123,1,123,6,127,1,5,127,131,2,6,131,135,2,6,135,139,1,139,5,143,1,143,10,147,1,147,2,151,1,151,13,0,99,2,0,14,0"

        @Test
        fun test1() {
            val input = "1,0,0,0,99"
            val answer = "2,0,0,0,99"
            val props = Props(input.longCodes(), mutableListOf(0))
            runProps(props)
            assertThat(answer).isEqualTo(props.toInputList())
        }

        @Test
        fun test2() {
            val input = "2,3,0,3,99"
            val answer = "2,3,0,6,99"
            val props = Props(input.longCodes(), mutableListOf(0))
            runProps(props)
            assertThat(answer).isEqualTo(props.toInputList())
        }

        @Test
        fun test3() {
            val input = "2,4,4,5,99,0"
            val answer = "2,4,4,5,99,9801"
            val props = Props(input.longCodes(), mutableListOf(0))
            runProps(props)
            assertThat(answer).isEqualTo(props.toInputList())
        }

        @Test
        fun test4() {
            val input = "1,1,1,4,99,5,6,0,99"
            val answer = "30,1,1,4,2,5,6,0,99"
            val props = Props(input.longCodes(), mutableListOf(0))
            runProps(props)
            assertThat(answer).isEqualTo(props.toInputList())
        }

        @Test
        fun task1() {
            val answer = 4576384L
            val props = Props(taskInput.longCodes(), mutableListOf(0))
            props.instructions[1] = 12
            props.instructions[2] = 2
            runProps(props)
            assertThat(answer).isEqualTo(props.instructions[0])
        }

        @Test
        fun task2() {
            val answer = 5398L

            for (noun in 0L..99) {
                for (verb in 0L..99) {
                    val props = Props(taskInput.longCodes(), mutableListOf(0))
                    props.instructions[1] = noun
                    props.instructions[2] = verb
                    runProps(props)
                    if (props.instructions[0] == 19690720L) {
                        assertThat(answer).isEqualTo(100 * noun + verb)
                        return
                    }
                }
            }
            throw Exception("No solution found")
        }
    }

    @Nested
    inner class Day05 {

        private val taskInput = "3,225,1,225,6,6,1100,1,238,225,104,0,1101,72,36,225,1101,87,26,225,2,144,13,224,101,-1872,224,224,4,224,102,8,223,223,1001,224,2,224,1,223,224,223,1102,66,61,225,1102,25,49,224,101,-1225,224,224,4,224,1002,223,8,223,1001,224,5,224,1,223,224,223,1101,35,77,224,101,-112,224,224,4,224,102,8,223,223,1001,224,2,224,1,223,224,223,1002,195,30,224,1001,224,-2550,224,4,224,1002,223,8,223,1001,224,1,224,1,224,223,223,1102,30,44,225,1102,24,21,225,1,170,117,224,101,-46,224,224,4,224,1002,223,8,223,101,5,224,224,1,224,223,223,1102,63,26,225,102,74,114,224,1001,224,-3256,224,4,224,102,8,223,223,1001,224,3,224,1,224,223,223,1101,58,22,225,101,13,17,224,101,-100,224,224,4,224,1002,223,8,223,101,6,224,224,1,224,223,223,1101,85,18,225,1001,44,7,224,101,-68,224,224,4,224,102,8,223,223,1001,224,5,224,1,223,224,223,4,223,99,0,0,0,677,0,0,0,0,0,0,0,0,0,0,0,1105,0,99999,1105,227,247,1105,1,99999,1005,227,99999,1005,0,256,1105,1,99999,1106,227,99999,1106,0,265,1105,1,99999,1006,0,99999,1006,227,274,1105,1,99999,1105,1,280,1105,1,99999,1,225,225,225,1101,294,0,0,105,1,0,1105,1,99999,1106,0,300,1105,1,99999,1,225,225,225,1101,314,0,0,106,0,0,1105,1,99999,7,677,226,224,102,2,223,223,1005,224,329,101,1,223,223,8,677,226,224,1002,223,2,223,1005,224,344,1001,223,1,223,1107,677,677,224,102,2,223,223,1005,224,359,1001,223,1,223,1107,226,677,224,102,2,223,223,1005,224,374,101,1,223,223,7,226,677,224,102,2,223,223,1005,224,389,101,1,223,223,8,226,677,224,1002,223,2,223,1005,224,404,101,1,223,223,1008,226,677,224,1002,223,2,223,1005,224,419,1001,223,1,223,107,677,677,224,102,2,223,223,1005,224,434,101,1,223,223,1108,677,226,224,1002,223,2,223,1006,224,449,101,1,223,223,1108,677,677,224,102,2,223,223,1006,224,464,101,1,223,223,1007,677,226,224,102,2,223,223,1006,224,479,101,1,223,223,1008,226,226,224,102,2,223,223,1006,224,494,101,1,223,223,108,226,226,224,1002,223,2,223,1006,224,509,101,1,223,223,107,226,226,224,102,2,223,223,1006,224,524,101,1,223,223,1107,677,226,224,102,2,223,223,1005,224,539,1001,223,1,223,108,226,677,224,1002,223,2,223,1005,224,554,101,1,223,223,1007,226,226,224,102,2,223,223,1005,224,569,101,1,223,223,8,226,226,224,102,2,223,223,1006,224,584,101,1,223,223,1008,677,677,224,1002,223,2,223,1005,224,599,1001,223,1,223,107,226,677,224,1002,223,2,223,1005,224,614,1001,223,1,223,1108,226,677,224,102,2,223,223,1006,224,629,101,1,223,223,7,677,677,224,1002,223,2,223,1005,224,644,1001,223,1,223,108,677,677,224,102,2,223,223,1005,224,659,101,1,223,223,1007,677,677,224,102,2,223,223,1006,224,674,101,1,223,223,4,223,99,226"

        @Nested
        inner class Modes {
            @Test
            fun test1() {
                val input = "3,0,4,0,99"
                val props = Props(input.longCodes(), mutableListOf(789))
                runProps(props)
                assertThat(props.print.size).isEqualTo(1)
                assertThat(props.print[0]).isEqualTo(789)
            }

            @Test
            fun test2() {
                val input = "1002,4,3,4,33"
                val answer = "1002,4,3,4,99"
                val props = Props(input.longCodes(), mutableListOf(0))
                runProps(props)
                assertThat(answer).isEqualTo(props.toInputList())
            }

            @Test
            fun test3() {
                val input = "1102,4,3,3,99"
                val answer = "1102,4,3,12,99"
                val props = Props(input.longCodes(), mutableListOf(0))
                runProps(props)
                assertThat(answer).isEqualTo(props.toInputList())
            }

            @Test
            fun test4() {
                val input = "2,0,2,5,99,0"
                val answer = "2,0,2,5,99,4"
                val props = Props(input.longCodes(), mutableListOf(0))
                runProps(props)
                assertThat(answer).isEqualTo(props.toInputList())
            }
        }

        @Nested
        inner class EqualsTo {
            @Test
            fun test1() {
                val input = "3,9,8,9,10,9,4,9,99,-1,8"
                val answer = 1L
                val props = Props(input.longCodes(), mutableListOf(8))
                runProps(props)
                assertThat(props.print.first()).isEqualTo(answer)
            }

            @Test
            fun test2() {
                val input = "3,9,8,9,10,9,4,9,99,-1,8"
                val answer = 0L
                val props = Props(input.longCodes(), mutableListOf(7))
                runProps(props)
                assertThat(props.print.first()).isEqualTo(answer)
            }

            @Test
            fun test3() {
                val input = "3,3,1108,-1,8,3,4,3,99"
                val answer = 1L
                val props = Props(input.longCodes(), mutableListOf(8))
                runProps(props)
                assertThat(props.print.first()).isEqualTo(answer)
            }

            @Test
            fun test4() {
                val input = "3,3,1108,-1,8,3,4,3,99"
                val answer = 0L
                val props = Props(input.longCodes(), mutableListOf(7))
                runProps(props)
                assertThat(props.print.first()).isEqualTo(answer)
            }
        }

        @Nested
        inner class LessThan {
            @Test
            fun test1() {
                val input = "3,9,7,9,10,9,4,9,99,-1,8"
                val answer = 1L
                val props = Props(input.longCodes(), mutableListOf(2))
                runProps(props)
                assertThat(props.print.first()).isEqualTo(answer)
            }

            @Test
            fun test2() {
                val input = "3,9,7,9,10,9,4,9,99,-1,8"
                val answer = 0L
                val props = Props(input.longCodes(), mutableListOf(9))
                runProps(props)
                assertThat(props.print.first()).isEqualTo(answer)
            }

            @Test
            fun test3() {
                val input = "3,3,1107,-1,8,3,4,3,99"
                val answer = 1L
                val props = Props(input.longCodes(), mutableListOf(2))
                runProps(props)
                assertThat(props.print.first()).isEqualTo(answer)
            }

            @Test
            fun test4() {
                val input = "3,3,1107,-1,8,3,4,3,99"
                val answer = 0L
                val props = Props(input.longCodes(), mutableListOf(9))
                runProps(props)
                assertThat(props.print.first()).isEqualTo(answer)
            }
        }

        @Nested
        inner class Jump {
            @Test
            fun test1() {
                val input = "3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9"
                val answer = 0L
                val props = Props(input.longCodes(), mutableListOf(0))
                runProps(props)
                assertThat(props.print.first()).isEqualTo(answer)
            }

            @Test
            fun test2() {
                val input = "3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9"
                val answer = 1L
                val props = Props(input.longCodes(), mutableListOf(2))
                runProps(props)
                assertThat(props.print.first()).isEqualTo(answer)
            }
        }

        @Nested
        inner class LargerTest {
            // The above example program uses an input instruction to ask for a single number.
            // The program will then
            // output 999 if the input value is below 8,
            // output 1000 if the input value is equal to 8, or
            // output 1001 if the input value is greater than 8.
            @Test
            fun largerTest1() {
                val input = "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99"
                val answer = 999L
                val props = Props(input.longCodes(), mutableListOf(2))
                runProps(props)
                assertThat(props.print.first()).isEqualTo(answer)
            }

            @Test
            fun largerTest2() {
                val input = "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99"
                val answer = 1000L
                val props = Props(input.longCodes(), mutableListOf(8))
                runProps(props)
                assertThat(props.print.first()).isEqualTo(answer)
            }

            @Test
            fun largerTest3() {
                val input = "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99"
                val answer = 1001L
                val props = Props(input.longCodes(), mutableListOf(9))
                runProps(props)
                assertThat(props.print.first()).isEqualTo(answer)
            }
        }

        @Test
        fun task1() {
            val answer = 5577461L
            val props = Props(taskInput.longCodes(), mutableListOf(1))
            runProps(props)
            assertThat(props.print).isNotEmpty
            assertThat(props.print.toList().subList(0, props.print.size - 1)).containsOnly(0L)
            assertThat(props.print.last()).isEqualTo(answer)
        }

        @Test
        fun task2() {
            val answer = 7161591L
            val props = Props(taskInput.longCodes(), mutableListOf(5))
            runProps(props)
            assertThat(props.print).isNotEmpty
            assertThat(props.print.size).isEqualTo(1)
            assertThat(props.print.last()).isEqualTo(answer)
        }
    }

    @Nested
    inner class Day07 {
        private val taskInput = "3,8,1001,8,10,8,105,1,0,0,21,34,51,76,101,126,207,288,369,450,99999,3,9,102,4,9,9,1001,9,2,9,4,9,99,3,9,1001,9,2,9,1002,9,3,9,101,3,9,9,4,9,99,3,9,102,5,9,9,1001,9,2,9,102,2,9,9,101,3,9,9,1002,9,2,9,4,9,99,3,9,101,5,9,9,102,5,9,9,1001,9,2,9,102,3,9,9,1001,9,3,9,4,9,99,3,9,101,2,9,9,1002,9,5,9,1001,9,5,9,1002,9,4,9,101,5,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,99,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,99"

        @Nested
        inner class Amplifier {
            @Test
            fun test1() {
                val input = "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0"
                val output = 43210L
                val amp = runAmplifier(input.longCodes(), "43210")
                assertThat(amp).isEqualTo(output)
            }

            @Test
            fun test2() {
                val input = "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0"
                val output = 54321L
                val amp = runAmplifier(input.longCodes(), "01234")
                assertThat(amp).isEqualTo(output)
            }

            @Test
            fun test3() {
                val input = "3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0"
                val output = 65210L
                val amp = runAmplifier(input.longCodes(), "10432")
                assertThat(amp).isEqualTo(output)
            }
        }

        @Nested
        inner class FeedBackAmplifier {
            @Test
            fun test1() {
                val input = "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5"
                val output = 139629729L
                val amp = runFeedbackAmplifier(input.longCodes(), "98765")
                assertThat(amp).isEqualTo(output)
            }

            @Test
            fun test2() {
                val input = "3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10"
                val output = 18216L
                val amp = runFeedbackAmplifier(input.longCodes(), "97856")
                assertThat(amp).isEqualTo(output)
            }
        }

        @Test
        fun task1() {
            val output = 422858L
            val amp = checkAmplifier(taskInput.longCodes())
            assertThat(amp).isEqualTo(output)
        }

        @Test
        fun task2() {
            val output = 14897241L
            val amp = feedBackAmplifiers(taskInput.longCodes())
            assertThat(amp).isEqualTo(output)
        }

    }

    @Nested
    inner class Day09 {
        private val taskInput = "1102,34463338,34463338,63,1007,63,34463338,63,1005,63,53,1101,3,0,1000,109,988,209,12,9,1000,209,6,209,3,203,0,1008,1000,1,63,1005,63,65,1008,1000,2,63,1005,63,904,1008,1000,0,63,1005,63,58,4,25,104,0,99,4,0,104,0,99,4,17,104,0,99,0,0,1102,1,550,1027,1101,0,0,1020,1101,30,0,1004,1101,0,22,1014,1102,1,36,1009,1101,37,0,1007,1102,25,1,1010,1102,1,33,1012,1102,282,1,1029,1102,1,488,1025,1101,0,31,1019,1101,0,21,1008,1101,0,35,1015,1101,664,0,1023,1102,26,1,1001,1101,28,0,1016,1102,29,1,1005,1102,1,24,1002,1101,20,0,1018,1101,27,0,1013,1101,38,0,1017,1102,1,1,1021,1102,1,557,1026,1102,1,39,1000,1101,23,0,1006,1101,493,0,1024,1102,1,291,1028,1101,671,0,1022,1101,0,34,1003,1101,0,32,1011,109,10,21108,40,40,8,1005,1018,199,4,187,1105,1,203,1001,64,1,64,1002,64,2,64,109,-14,2108,30,8,63,1005,63,225,4,209,1001,64,1,64,1105,1,225,1002,64,2,64,109,3,2102,1,4,63,1008,63,34,63,1005,63,251,4,231,1001,64,1,64,1106,0,251,1002,64,2,64,109,12,2107,22,-5,63,1005,63,269,4,257,1105,1,273,1001,64,1,64,1002,64,2,64,109,20,2106,0,-3,4,279,1001,64,1,64,1106,0,291,1002,64,2,64,109,-16,21108,41,40,-3,1005,1012,311,1001,64,1,64,1105,1,313,4,297,1002,64,2,64,109,-13,2101,0,2,63,1008,63,30,63,1005,63,335,4,319,1105,1,339,1001,64,1,64,1002,64,2,64,109,-3,2102,1,4,63,1008,63,35,63,1005,63,359,1106,0,365,4,345,1001,64,1,64,1002,64,2,64,109,15,1205,6,377,1105,1,383,4,371,1001,64,1,64,1002,64,2,64,109,5,21102,42,1,-2,1008,1017,39,63,1005,63,403,1106,0,409,4,389,1001,64,1,64,1002,64,2,64,109,-17,21107,43,44,10,1005,1012,431,4,415,1001,64,1,64,1106,0,431,1002,64,2,64,109,14,21107,44,43,-4,1005,1012,451,1001,64,1,64,1106,0,453,4,437,1002,64,2,64,109,1,21102,45,1,-3,1008,1014,45,63,1005,63,479,4,459,1001,64,1,64,1105,1,479,1002,64,2,64,109,7,2105,1,0,4,485,1106,0,497,1001,64,1,64,1002,64,2,64,109,5,1206,-8,513,1001,64,1,64,1106,0,515,4,503,1002,64,2,64,109,-33,2101,0,7,63,1008,63,32,63,1005,63,535,1106,0,541,4,521,1001,64,1,64,1002,64,2,64,109,23,2106,0,8,1001,64,1,64,1106,0,559,4,547,1002,64,2,64,109,-1,21101,46,0,-5,1008,1013,46,63,1005,63,585,4,565,1001,64,1,64,1105,1,585,1002,64,2,64,109,-4,21101,47,0,2,1008,1016,44,63,1005,63,605,1105,1,611,4,591,1001,64,1,64,1002,64,2,64,109,-18,1207,4,38,63,1005,63,627,1106,0,633,4,617,1001,64,1,64,1002,64,2,64,109,5,2107,22,7,63,1005,63,649,1106,0,655,4,639,1001,64,1,64,1002,64,2,64,109,12,2105,1,10,1001,64,1,64,1106,0,673,4,661,1002,64,2,64,109,-10,1208,6,33,63,1005,63,693,1001,64,1,64,1106,0,695,4,679,1002,64,2,64,109,-7,2108,35,7,63,1005,63,715,1001,64,1,64,1106,0,717,4,701,1002,64,2,64,109,6,1208,5,37,63,1005,63,735,4,723,1106,0,739,1001,64,1,64,1002,64,2,64,109,-4,1202,5,1,63,1008,63,34,63,1005,63,765,4,745,1001,64,1,64,1105,1,765,1002,64,2,64,109,29,1206,-7,783,4,771,1001,64,1,64,1105,1,783,1002,64,2,64,109,-28,1201,6,0,63,1008,63,29,63,1005,63,809,4,789,1001,64,1,64,1106,0,809,1002,64,2,64,109,5,1202,2,1,63,1008,63,20,63,1005,63,829,1106,0,835,4,815,1001,64,1,64,1002,64,2,64,109,-1,1201,6,0,63,1008,63,35,63,1005,63,859,1001,64,1,64,1105,1,861,4,841,1002,64,2,64,109,2,1207,-3,25,63,1005,63,879,4,867,1105,1,883,1001,64,1,64,1002,64,2,64,109,13,1205,3,901,4,889,1001,64,1,64,1106,0,901,4,64,99,21101,0,27,1,21101,915,0,0,1106,0,922,21201,1,22987,1,204,1,99,109,3,1207,-2,3,63,1005,63,964,21201,-2,-1,1,21101,0,942,0,1106,0,922,22101,0,1,-1,21201,-2,-3,1,21101,0,957,0,1106,0,922,22201,1,-1,-2,1105,1,968,21202,-2,1,-2,109,-3,2105,1,0"
        @Test
        fun test1() {
            val input = "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99"
            val answer = listOf(109L, 1L, 204L, -1L, 1001L, 100L, 1L, 100L, 1008L, 100L, 16L, 101L, 1006L, 101L, 0L, 99L)
            val props = Props(input.longCodes(), mutableListOf(1))
            runProps(props)
            assertThat(props.print).isNotEmpty
            assertThat(props.print).isEqualTo(answer)
        }

        @Test
        fun test2() {
            val input = "1102,34915192,34915192,7,4,7,99,0"
            val answer = 34915192L * 34915192L
            val props = Props(input.longCodes(), mutableListOf(1))
            runProps(props)
            assertThat(props.print).isNotEmpty
            assertThat(props.print.last()).isEqualTo(answer)
        }

        @Test
        fun test3() {
            val input = "104,1125899906842624,99"
            val answer = 1125899906842624L
            val props = Props(input.longCodes(), mutableListOf(1))
            runProps(props)
            assertThat(props.print).isNotEmpty
            assertThat(props.print.last()).isEqualTo(answer)
        }

        @Nested
        inner class Relative {

            @Test
            fun test00() {
                val input = "1,1,2,5,99"
                val answer = "1,1,2,5,99,3"
                val props = Props(input.longCodes(), mutableListOf(0), relative = 1)
                runProps(props)
                assertThat(answer).isEqualTo(props.toInputList())
            }

            @Test
            fun test0() {
                val input = "201,1,2,5,99"
                val answer = "201,1,2,5,99,4"
                val props = Props(input.longCodes(), mutableListOf(0), relative = 1)
                runProps(props)
                assertThat(answer).isEqualTo(props.toInputList())
            }

            @Test
            fun test1() {
                val input = "2001,1,2,5,99"
                val answer = "2001,1,2,5,99,6"
                val props = Props(input.longCodes(), mutableListOf(0), relative = 1)
                runProps(props)
                assertThat(answer).isEqualTo(props.toInputList())
            }

            @Test
            fun test2() {
                val input = "2201,1,2,5,99,5"
                val answer = "2201,1,2,5,99,7"
                val props = Props(input.longCodes(), mutableListOf(0), relative = 1)
                runProps(props)
                assertThat(answer).isEqualTo(props.toInputList())
            }

            @Test
            fun test3() {
                val input = "109,19,99"
                val answer = 2019L
                val props = Props(input.longCodes(), mutableListOf(1), relative = 2000)
                runProps(props)
                assertThat(props.relative).isEqualTo(answer)
            }

            @Test
            fun test4() {
                val input = "109,2,204,2,99"
                val answer = 99L
                val props = Props(input.longCodes(), mutableListOf(1), relative = 0)
                runProps(props)
                assertThat(props.print.last()).isEqualTo(answer)
            }
        }

        @Test
        fun task1() {
            val answer = 422858L
            val props = Props(taskInput.longCodes(), mutableListOf(1))
            runProps(props)
            assertThat(props.print).isNotEmpty
            assertThat(props.print).isEqualTo(listOf(answer))
        }
    }
}
